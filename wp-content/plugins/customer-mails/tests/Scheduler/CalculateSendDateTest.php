<?php
namespace Tests\Greenhill\CustomerMails\Scheduler;

use Greenhill\CustomerMails\Service\CalculateSendDate;
use PHPUnit\Framework\TestCase;

class CalculateSendDateTest extends TestCase
{
    /** @test */
    public function itRemindsCleanersTheDayBeforeCheckin()
    {
        $reservationDay = '2018-01-08 00:00:00';
        $expectedReminderDay = '2018-01-07 06:00:00';

        $this->assertSame(
            $expectedReminderDay,
            CalculateSendDate::forCleanerReminderEmail($reservationDay)
        );
    }

    /** @test */
    public function itReturnsSendDateAWeekPriorToReservationDate()
    {
        $reservationDay = '2018-01-08 00:00:00';
        $expectedResult = '2018-01-01 06:00:00';

        $this->assertSame(
            $expectedResult,
            CalculateSendDate::forReminderEmail($reservationDay)
        );
    }

    /**
     * @test
     * @dataProvider dates
     * @param string $reservationDay
     * @param string $expectedResult
     */
    public function itReturnsNowAsSendDateIfReservationLessThanAWeekInTheFuture($reservationDay, $expectedResult)
    {
        $this->assertSame($expectedResult, CalculateSendDate::forReminderEmail($reservationDay));
    }

    public function dates() {
        return [
            '1 day from now' => [
                'reservation' => (new \DateTimeImmutable())->modify('+1 days')->format('Y-m-d H:i:s'),
                'expected' => (new \DateTimeImmutable())->modify('+30 minutes')->format('Y-m-d H:i:s'),
            ],
            '2 days from now' => [
                'reservation' => (new \DateTimeImmutable())->modify('+2 days')->format('Y-m-d H:i:s'),
                'expected' => (new \DateTimeImmutable())->modify('+30 minutes')->format('Y-m-d H:i:s'),
            ],
            '3 days from now' => [
                'reservation' => (new \DateTimeImmutable())->modify('+3 days')->format('Y-m-d H:i:s'),
                'expected' => (new \DateTimeImmutable())->modify('+30 minutes')->format('Y-m-d H:i:s'),
            ],
            '4 days from now' => [
                'reservation' => (new \DateTimeImmutable())->modify('+4 days')->format('Y-m-d H:i:s'),
                'expected' => (new \DateTimeImmutable())->modify('+30 minutes')->format('Y-m-d H:i:s'),
            ],
            '5 days from now' => [
                'reservation' => (new \DateTimeImmutable())->modify('+5 days')->format('Y-m-d H:i:s'),
                'expected' => (new \DateTimeImmutable())->modify('+30 minutes')->format('Y-m-d H:i:s'),
            ],
            '6 days from now' => [
                'reservation' => (new \DateTimeImmutable())->modify('+6 days')->format('Y-m-d H:i:s'),
                'expected' => (new \DateTimeImmutable())->modify('+30 minutes')->format('Y-m-d H:i:s'),
            ],
            '7 days from now' => [
                'reservation' => (new \DateTimeImmutable())->modify('+7 days')->format('Y-m-d H:i:s'),
                'expected' => (new \DateTimeImmutable())->modify('+30 minutes')->format('Y-m-d H:i:s'),
            ],
            '8 days from now' => [
                'reservation' => (new \DateTimeImmutable())->modify('+8 days')->format('Y-m-d'),
                'expected' => (new \DateTimeImmutable())->modify('+30 minutes')->format('Y-m-d H:i:s'),
            ],
            '9 days from now' => [
                'reservation' => (new \DateTimeImmutable())->modify('+9 days')->format('Y-m-d H:i:s'),
                'expected' => (new \DateTimeImmutable())->modify('+2 days')->modify('+6 hours')->format('Y-m-d H:i:s'),
            ],
            '10 days from now' =>  [
                'reservation' => (new \DateTimeImmutable())->modify('+10 days')->format('Y-m-d H:i:s'),
                'expected' => (new \DateTimeImmutable())->modify('+3 days')->modify('+6 hours')->format('Y-m-d H:i:s'),
            ],
        ];
    }
}
