# Customer Mails Plugin

This plugin provides an automated mailing service for booking reminders,
surveys, and cleaner reminder emails.  It hooks into the booking process and
schedules emails appropriately.


## Console Script

It provides a console script allowing manual rescheduling. This can be used if a
cleaner changes or a cabin needs to have all its booking emails rescheduled for
some reason.


### Usage

- log in to one of the cabinscape servers
- `cd /var/www/html/wp-content/plugins/customer-mails`
- `php bin/console.php emails:reschedule <MANDRILL_API_KEY> <CABIN_ID>`
- You can run this command in a dry run mode without sending the mails by adding
the dry run flag:
```
php bin/console.php emails:reschedule --dry-run MANDRILL_API_KEY CABIN_ID
```
- Leave the dry run flag off to schedule the emails.

```
php bin/console.php emails:reschedule MANDRILL_API_KEY CABIN_ID
```

- You can optionally add the booking id to the command to only reschedule for a
    single booking.
```
php bin/console.php emails:reschedule MANDRILL_API_KEY CABIN_ID BOOKING_ID
```

