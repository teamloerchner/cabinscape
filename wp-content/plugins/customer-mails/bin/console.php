#!/usr/bin/env php
<?php

require_once __DIR__ . "/../vendor/autoload.php";
require_once __DIR__ . "/../../../../wp-load.php";

use Greenhill\CustomerMails\Commands\ReplayScheduledEmailsCommand;

const APP_NAME="Email Scheduler";
const APP_VERSION="0.0.1";
$app = new \Symfony\Component\Console\Application(APP_NAME, APP_VERSION);

$app->add(new ReplayScheduledEmailsCommand());

try {
    $app->run();
} catch (\Throwable $e) {
    error_log($e->getMessage());
    exit(1);
}
