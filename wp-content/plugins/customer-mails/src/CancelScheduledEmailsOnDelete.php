<?php
namespace Greenhill\CustomerMails;

use Greenhill\CustomerMails\Repository\CustomerMails;
use Monolog\Logger;

class CancelScheduledEmailsOnDelete
{
    const DELETED_HOOK = 'nd_booking_reservation_deleted_from_db';

    /**
     * @var \Mandrill
     */
    private $mandrill;

    /**
     * @var CustomerMails
     */
    private $customerMails;

    /**
     * @var Logger
     */
    private $logger;

    private function __construct(\Mandrill $mandrill, CustomerMails $customerMails, Logger $logger)
    {
        $this->mandrill = $mandrill;
        $this->customerMails = $customerMails;
        $this->logger = $logger;

        add_action(self::DELETED_HOOK, [$this, 'perform'], 10, 1);
    }

    public static function registerDeleteBookingHook(\Mandrill $mandrill, CustomerMails $customerMails, Logger $logger)
    {
        return new self($mandrill, $customerMails, $logger);
    }

    public function perform($bookingId)
    {
        $emails = $this->customerMails->findEmailsByBookingId($bookingId);

        foreach (['survey', 'reminder', 'cleaner'] as $emailType) {
            try {
                $this->mandrill->messages->cancelScheduled($emails[$emailType]);
            } catch (\Exception $e) {
                $this->logger->error("error deleting $emailType email", ['exception' => $e, 'emails' => $emails]);
            }
        }

        $this->customerMails->delete($bookingId);
    }
}
