<?php
namespace Greenhill\CustomerMails;

use Greenhill\CustomerMails\Domain\Booking;
use Greenhill\CustomerMails\Emails\CleanerReminderEmail;
use Greenhill\CustomerMails\Emails\ReminderEmail;
use Greenhill\CustomerMails\Emails\SurveyEmail;
use Greenhill\CustomerMails\Repository\CustomerMails;
use Monolog\Logger;

class ScheduleEmailsOnBooking
{
    const ORDER_ADDED_HOOK = 'nd_booking_reservation_added_in_db';

    /**
     * @var ReminderEmail
     */
    private $scheduleReminderEmail;

    /**
     * @var SurveyEmail
     */
    private $scheduleSurveyEmail;

    /**
     * @var CleanerReminderEmail
     */
    private $scheduleCleanerReminderEmail;

    /**
     * @var CustomerMails
     */
    private $customerMails;

    private function __construct(\Mandrill $mandrill, Logger $log, $registerHook = true){
        $this->customerMails =  new CustomerMails();
        $this->scheduleReminderEmail = new ReminderEmail($mandrill, $log);
        $this->scheduleSurveyEmail = new SurveyEmail($mandrill, $log);
        $this->scheduleCleanerReminderEmail = new CleanerReminderEmail($mandrill, $log);

        if ($registerHook === true) {
            add_action(self::ORDER_ADDED_HOOK, [$this, 'perform'], 10, 24);
        }
    }

    public static function registerBookingHook(\Mandrill $mandrill, Logger $log)
    {
        return new self($mandrill, $log);
    }

    public static function getInstance(\Mandrill $mandrill, Logger $log)
    {
        return new self($mandrill, $log, false);
    }

    public function perform(...$bookingData)
    {
        $booking = Booking::fromBookingData($bookingData);
        $cleanerResult = $this->scheduleCleanerReminderEmail->execute($booking);
        $reminderResult = $this->scheduleReminderEmail->execute($booking);
        $surveyResult = $this->scheduleSurveyEmail->execute($booking);

        $dateFrom = (new \DateTimeImmutable($booking->getReservationStart()))->format('Y/m/d');
        $dateTo = (new \DateTimeImmutable($booking->getReservationEnd()))->format('Y/m/d');
        $bookingId = $this->customerMails->getBookingId($booking->getCabinId(), $dateFrom, $dateTo, $booking->getEmail());

        $this->customerMails->addBookingEmails(
            $bookingId,
            $surveyResult[0]['_id'],
            $reminderResult[0]['_id'],
            $cleanerResult[0]['_id']
        );
    }
}
