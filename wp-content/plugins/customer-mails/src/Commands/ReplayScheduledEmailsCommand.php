<?php declare(strict_types=1);
namespace Greenhill\CustomerMails\Commands;

use Greenhill\CustomerMails\Domain\Booking;
use Greenhill\CustomerMails\Repository\CustomerMails;
use Greenhill\CustomerMails\UpdateScheduledEmailsOnChange;
use Monolog\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ReplayScheduledEmailsCommand extends Command
{
    const MANDRILL_KEY = 'mandrill_api_key';
    const BOOKING_ID = 'booking_id';
    const CABIN_ID = 'cabin_id';

    const DRY_RUN = 'dry-run';
    const API_CALL_RATE_MS = 500000;

    protected function configure()
    {
        $this
            ->setName("emails:reschedule")
            ->setDescription("Reschedules failed emails")
            ->setHelp("This command will reschedule failed emails")
            ->addArgument(
                self::MANDRILL_KEY,
                InputArgument::REQUIRED,
                'Mandrill API Key'
            )
            ->addArgument(
                self::CABIN_ID,
                InputArgument::REQUIRED,
                'run the command for a cabin'
            )
            ->addArgument(
                self::BOOKING_ID,
                InputArgument::OPTIONAL,
                'run the command for a single booking id'
            )
            ->addOption(
                self::DRY_RUN,
                null,
                InputOption::VALUE_NONE,
                'Does not schedule email or write to database'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mandrill = new \Mandrill($input->getArgument(self::MANDRILL_KEY));
        $logger = new Logger('replay-scheduled-mails');
        $logger->pushHandler(new \Monolog\Handler\StreamHandler(__DIR__ . '/../../log/mails.log'));
        $customerMails = new CustomerMails();
        $command = UpdateScheduledEmailsOnChange::getInstance($mandrill, $customerMails, $logger);
        $results = $customerMails->getBookingsByCabinAndBooking($input->getArgument(self::CABIN_ID), $input->getArgument(self::BOOKING_ID));
        if (count($results) === 0) {
            $output->writeln("<error>no bookings found. exiting ...</error>");
            exit();
        }

        $output->writeln(sprintf("<info>Found %d booking records to replay</info>", count($results)));
        $output->writeln($input->getOption(self::DRY_RUN) ? "<info>This is a dry run</info>": "");

        foreach($results as $result) {
            if (!$input->getOption(self::DRY_RUN)) {
                extract($result);
                $command->perform(
                    $id,
                    $start,
                    $end,
                    $user_first_name,
                    $user_last_name,
                    $paypal_email
                );
            }

            $output->writeln(sprintf("<info>Scheduled Email to %s with booking %s for Cabin ID %s</info>",
                $result['paypal_email'],
                $result['id'],
                $result['id_post']
            ));

            usleep(self::API_CALL_RATE_MS); // wait 0.5 seconds between processing bookings
        }

        $output->writeln("===========================");
        $output->writeln(sprintf("<info>Emails for %d bookings scheduled</info>", count($results)));
    }

}
