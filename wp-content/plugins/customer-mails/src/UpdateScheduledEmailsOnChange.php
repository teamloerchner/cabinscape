<?php
namespace Greenhill\CustomerMails;

use Greenhill\CustomerMails\Domain\Booking;
use Greenhill\CustomerMails\Emails\CleanerReminderEmail;
use Greenhill\CustomerMails\Emails\ReminderEmail;
use Greenhill\CustomerMails\Emails\SurveyEmail;
use Greenhill\CustomerMails\Repository\CustomerMails;
use Monolog\Logger;

class UpdateScheduledEmailsOnChange
{
    const UPDATED_HOOK = 'nd_booking_reservation_updated_in_db';

    /**
     * @var \Mandrill
     */
    private $mandrill;

    /**
     * @var CustomerMails
     */
    private $customerMails;

    /**
     * @var Logger
     */
    private $logger;

    private function __construct(\Mandrill $mandrill, CustomerMails $customerMails, Logger $logger, $registerHook = true)
    {
        $this->mandrill = $mandrill;
        $this->customerMails = $customerMails;
        $this->logger = $logger;
        $this->scheduleReminderEmail = new ReminderEmail($mandrill, $logger);
        $this->scheduleSurveyEmail = new SurveyEmail($mandrill, $logger);
        $this->scheduleCleanerReminderEmail = new CleanerReminderEmail($mandrill, $logger);

        if ($registerHook === true) {
            add_action(self::UPDATED_HOOK, [$this, 'perform'], 10, 6);
        }
    }

    public static function registerBookingChangeHook(\Mandrill $mandrill, CustomerMails $customerMails, Logger $logger)
    {
        return new self($mandrill, $customerMails, $logger);
    }

    public static function getInstance(\Mandrill $mandrill, CustomerMails $customerMails, Logger $logger)
    {
        return new self($mandrill, $customerMails, $logger, false);
    }

    public function perform(...$orderData)
    {
        foreach(['cleaner', 'survey', 'reminder'] as $emailType) {
            try {
                $scheduledEmails = $this->customerMails->findEmailsByBookingId($orderData[0]);
                if ($scheduledEmails !== null) {
                    $this->mandrill->messages->cancelScheduled($scheduledEmails[$emailType]);
                }
            } catch (\Exception $e) {
                $this->logger->error("Error deleting $emailType emails", ['exception' => $e->getMessage()]);
            }
        }

        $this->customerMails->delete($orderData[0]);

        try {
            $cabinID = $this->customerMails->getCabinIdByBookingId($orderData[0])->id_post;
            $result = $this->customerMails->getBookingsByCabinAndBooking($cabinID, $orderData[0]);
            $booking = Booking::fromBookingData(array_values($result[0]));
            $this->rescheduleMessages($booking);
        } catch (\Exception $e) {
            $this->logger->error('Cleaner Email Rescheduling Error', ['exception' => $e->getMessage()]);
        }
    }

    private function rescheduleMessages($booking)
    {
        $cleanerResult = $this->scheduleCleanerReminderEmail->execute($booking);
        $reminderResult = $this->scheduleReminderEmail->execute($booking);
        $surveyResult = $this->scheduleSurveyEmail->execute($booking);

        $dateFrom = (new \DateTimeImmutable($booking->getReservationStart()))->format('Y/m/d');
        $dateTo = (new \DateTimeImmutable($booking->getReservationEnd()))->format('Y/m/d');

        $this->customerMails->addBookingEmails(
            $booking->getID(),
            $surveyResult[0]['_id'],
            $reminderResult[0]['_id'],
            $cleanerResult[0]['_id']
        );
    }
}
