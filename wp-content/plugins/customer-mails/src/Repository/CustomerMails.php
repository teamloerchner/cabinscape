<?php
namespace Greenhill\CustomerMails\Repository;

use Symfony\Component\Console\Exception\InvalidArgumentException;

class CustomerMails
{
    const TABLE_NAME = 'cs_scheduled_emails';

    /** @var string */
    private $table;

    public function __construct()
    {
        global $wpdb;
        $this->table = $wpdb->prefix . self::TABLE_NAME;
    }

    public function addBookingEmails($bookingId, $surveyId, $reminderId, $cleanerReminderId)
    {
        global $wpdb;
        $wpdb->insert(
            $this->table,
            [
               'booking_id' => $bookingId,
               'survey_id' => $surveyId,
               'reminder_id' => $reminderId,
               'cleaner_reminder_id' => $cleanerReminderId
            ],
            [
                '%s',
                '%s',
                '%s',
                '%s'
            ]
        );
    }

    public function findEmailsByBookingId($bookingId)
    {
        global $wpdb;
        $results = $wpdb->get_results($wpdb->prepare("SELECT survey_id, reminder_id, cleaner_reminder_id FROM {$this->table} WHERE booking_id = %s", $bookingId));
        if (count($results) === 0) {
            return null;
        }

        return [
            'survey' => $results[0]->survey_id,
            'reminder' => $results[0]->reminder_id,
            'cleaner' => $results[0]->cleaner_reminder_id
        ];
    }

    public function getCabinIdByBookingId($bookingID)
    {
        global $wpdb;
        $t = $wpdb->prefix . 'nd_booking_booking';
        return $wpdb->get_row(sprintf('select id_post from %s where id = %s', $t, (string) $bookingID));
    }

    public function getBookingId($cabinId, $dateFrom, $dateTo, $email)
    {
        global $wpdb;
        $ndBookingsTable = $wpdb->prefix . "nd_booking_booking";
        $results = $wpdb->get_results($wpdb->prepare(
            "SELECT id FROM {$ndBookingsTable} WHERE id_post = %d AND date_from = %s AND date_to = %s and paypal_email = %s",
            $cabinId,
            $dateFrom,
            $dateTo,
            $email
        ));
        return $results[0]->id;
    }

    public function delete($bookingId)
    {
        global $wpdb;
        $wpdb->delete($this->table, ['booking_id' => $bookingId]);
    }

    public function getCleanerByCabinID(string $cabinID)
    {
        global $wpdb;
        $results = $wpdb->get_results('
            select
                   u.display_name as name,
                   u.user_email as email
            from '.$wpdb->prefix.'nd_booking_booking b
            join wp_postmeta m on m.post_id = b.id_post
            join wp_users u on u.ID = m.meta_value
            where b.id_post = '.$cabinID.'
            and m.meta_key = \'cleaner_user\';
        ');
        if (count($results) === 0) {
            return null;
        }
        return $results[0];
    }

    public function updateBookingMailsWithCleanerReminder($bookingID, $cleanerReminderID)
    {
        global $wpdb;
        $update = ['cleaner_reminder_id' => $cleanerReminderID];
        $where = ['booking_id' => $bookingID];
        $updated = $wpdb->update($this->table, $update, $where);

        if ($updated === false) {
            throw new InvalidArgumentException(sprintf(
                'no booking with id %s. failed to add cleaner reminder %s',
                $bookingID,
                $cleanerReminderID
            ));
        }
    }

    public function updateBookingMailsWithReminderAndSurvey($bookingID, $reminderID, $surveyID)
    {
        global $wpdb;
        $update = [
            'reminder_id' => $reminderID,
            'survey_id' => $surveyID
        ];
        $where = [
            'booking_id' => $bookingID
        ];

        $updated = $wpdb->update($this->table, $update, $where);

        if ($updated === false) {
            throw new InvalidArgumentException(sprintf(
                'no booking with id %s. failed to add reminder %s and survey %s',
                $bookingID,
                $reminderID,
                $surveyID
            ));
        }
    }

    /**
     * getBookingsByCabinAndBooking
     * returns bookings for the provided cabin id.
     * if no booking id is provided, bookings are selected whose checkin
     * dates are a week from now and one year from now
     *
     * @param string $cabinID
     * @param string|null $bookingID
     * @return array
     */
    public function getBookingsByCabinAndBooking($cabinID, $bookingID = null)
    {
        global $wpdb;
        $table = $wpdb->prefix . "nd_booking_booking";
        $where = $bookingID ? "AND b.id = $bookingID" : "AND b.date_from BETWEEN NOW() + INTERVAL 1 WEEK AND NOW() + INTERVAL 1 YEAR";
        $query = "
select
b.id_post,
b.title_post,
STR_TO_DATE(b.date, '%H:%i:%s %M %d %Y') as placed,
STR_TO_DATE(b.date_from, '%Y/%m/%d') as start,
STR_TO_DATE(b.date_to, '%Y/%m/%d') as end,
b.guests,
b.final_trip_price,
b.extra_services,
b.id_user,
b.user_first_name,
b.user_last_name,
b.paypal_email,
b.user_phone,
b.user_address,
b.user_city,
b.user_country,
b.user_message,
b.user_arrival,
b.user_coupon,
b.paypal_payment_status,
b.paypal_currency,
b.paypal_tx,
b.action_type,
b.id
  from $table b
where b.id_post = $cabinID
$where
";
        return $wpdb->get_results($query, "ARRAY_A");
    }
}
