<?php
namespace Greenhill\CustomerMails\Scheduler;

use Greenhill\CustomerMails\Domain\Booking;
use Greenhill\CustomerMails\Repository\CustomerMails;
use Monolog\Logger;

abstract class MandrillScheduler
{
    const CS_CUSTOMER_MAILS_OPTION = 'cs_customer_mails_option';

    /**
     * @var \Mandrill
     */
    protected $mandrill;

    /**
     * @var CustomerMails
     */
    protected $customerMails;

    /**
     * @var Logger
     */
    protected $logger;

    public function __construct(\Mandrill $mandrill, Logger $logger)
    {
        $this->mandrill = $mandrill;
        $this->logger = $logger;
        $this->customerMails = new CustomerMails();
    }

    protected function subaccount()
    {
        return get_option(self::CS_CUSTOMER_MAILS_OPTION)['mandrill-subaccount'];
    }

    protected function cleanerSubaccount()
    {
        return get_option(self::CS_CUSTOMER_MAILS_OPTION)['cleaner-subaccount'];
    }

    protected function surveyEmailId()
    {
        return get_option(self::CS_CUSTOMER_MAILS_OPTION)['survey-template'];
    }

    protected function reminderEmailId()
    {
        return get_option(self::CS_CUSTOMER_MAILS_OPTION)['reminder-template'];
    }

    protected function cleanerReminderTemplate()
    {
        return get_option(self::CS_CUSTOMER_MAILS_OPTION)['cleaner-reminder-template'];
    }

    abstract public function execute(Booking $booking);
}
