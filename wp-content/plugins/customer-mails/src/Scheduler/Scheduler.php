<?php
namespace Greenhill\CustomerMails\Scheduler;

use Greenhill\CustomerMails\Domain\Booking;

interface Scheduler
{
    /**
     * @param Booking $booking
     */
    public function execute(Booking $booking);
}
