<?php declare(strict_types=1);
namespace Greenhill\CustomerMails\Emails;

use Greenhill\CustomerMails\Domain\Booking;
use Greenhill\CustomerMails\Domain\MailContent;
use Greenhill\CustomerMails\Scheduler\MandrillScheduler;
use Greenhill\CustomerMails\Service\CalculateSendDate;

class CleanerReminderEmail extends MandrillScheduler
{
    /**
     * @param Booking $booking
     * @return array
     */
    public function execute(Booking $booking)
    {
        $cleaner = $this->customerMails->getCleanerByCabinID((string) $booking->getCabinId());
        if ($cleaner === null) {
            $this->logger->debug('no cleaner assigned to cabin', ['cabin' => $booking->getCabinId()]);
            $cleaner = new \stdClass();
            $cleaner->email = 'bookings@cabinscape.com';
            $cleaner->name = 'no cleaner';
        }
        $emailTemplateVars = MailContent::mergeVarsFromBooking($booking);
        $emailTemplateVars[] = [
            'name' => 'CLEANER_NAME',
            'content' => $cleaner->name
        ];

        $message = [
            'to' => [
                [
                    'email' => $cleaner->email,
                    'name' => $cleaner->name,
                ]
            ],
            'subaccount' => $this->cleanerSubaccount(),
            'merge_language' => 'mailchimp',
            'global_merge_vars' => $emailTemplateVars,
        ];
        $resp = $this->mandrill->messages->sendTemplate(
            $this->cleanerReminderTemplate(),
            [],
            $message,
            false,
            null,
            CalculateSendDate::forCleanerReminderEmail($booking->getReservationStart())
        );
        $this->logger->debug(
            'cleaner mail mandrill response',
            ['response' => $resp, 'message' => $message]
        );
        return $resp;
    }
}
