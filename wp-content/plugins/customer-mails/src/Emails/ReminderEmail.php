<?php
namespace Greenhill\CustomerMails\Emails;

use Greenhill\CustomerMails\Domain\Booking;
use Greenhill\CustomerMails\Domain\MailContent;
use Greenhill\CustomerMails\Service\CalculateSendDate;
use Greenhill\CustomerMails\Scheduler\MandrillScheduler;

class ReminderEmail extends MandrillScheduler
{
    public function execute(Booking $booking)
    {
        $message = [
            'to' => [
                ['email' => $booking->getEmail(),'name' => sprintf("%s %s", $booking->getFirstName(), $booking->getLastName()) ]
            ],
            'subaccount' => $this->subaccount(),
            'merge_language' => 'mailchimp',
            'global_merge_vars' => MailContent::mergeVarsFromBooking($booking),
        ];

        $resp = $this->mandrill->messages->sendTemplate(
            sprintf("%s-%s", $this->reminderEmailId(), $booking->getCabinId()),
            [],
            $message,
            false,
            null,
            CalculateSendDate::forReminderEmail($booking->getReservationStart())
        );

        $this->logger->debug(
            'reminder mail mandrill response',
            ['response' => $resp, 'message' => $message]
        );
        return $resp;
    }
}
