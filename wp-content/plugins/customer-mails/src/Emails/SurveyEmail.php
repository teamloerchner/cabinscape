<?php
namespace Greenhill\CustomerMails\Emails;

use Greenhill\CustomerMails\Service\CalculateSendDate;
use Greenhill\CustomerMails\Scheduler\MandrillScheduler;
use Greenhill\CustomerMails\Domain\MailContent;
use Greenhill\CustomerMails\Domain\Booking;

class SurveyEmail extends MandrillScheduler
{
    public function execute(Booking $booking)
    {
        $message = [
            'to' => [
                ['email' => $booking->getEmail(),'name' => $booking->getFirstName()]
            ],
            'subaccount' => $this->subaccount(),
            'merge_language' => 'mailchimp',
            'global_merge_vars' => MailContent::mergeVarsFromBooking($booking),
        ];

        $resp =  $this->mandrill->messages->sendTemplate(
            $this->surveyEmailId(),
            [],
            $message,
            false,
            null,
            CalculateSendDate::forSurveyEmail($booking->getReservationEnd())
        );

        $this->logger->debug(
            'follow-up mail mandrill response',
            ['response' => $resp, 'message' => $message]
        );
        return $resp;
    }
}
