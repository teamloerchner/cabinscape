<?php
namespace Greenhill\CustomerMails;

class SettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __invoke()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Cabinscape Customer Mails',
            'Customer Mails Settings',
            'manage_options',
            'cs_customer_mails_setting',
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'cs_customer_mails_option' );
        ?>
        <div class="wrap">
            <h1>Customer Mails Settings</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'cs_customer_mails_option_group' );
                do_settings_sections( 'cs_customer_mails_setting' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        register_setting(
            'cs_customer_mails_option_group', // Option group
            'cs_customer_mails_option', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'settings_section_mandrill', // ID
            'Cabinscape Customer Mails Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'cs_customer_mails_setting' // Page
        );

        add_settings_field(
            'mandrill_api_key', // ID
            'Mandrill API Key', // Title
            array( $this, 'api_key_callback' ), // Callback
            'cs_customer_mails_setting', // Page
            'settings_section_mandrill' // Section
        );

        add_settings_field(
            'subaccount',
            'Mandrill Subaccount',
            array( $this, 'mandrill_subaccount_callback' ),
            'cs_customer_mails_setting',
            'settings_section_mandrill'
        );

        add_settings_field(
            'cleaner-subaccount',
            'Cleaner Subaccount',
            array( $this, 'cleaner_subaccount_callback' ),
            'cs_customer_mails_setting',
            'settings_section_mandrill'
        );

        add_settings_field(
            'survey-template',
            'Survey Email Template',
            array( $this, 'survey_template_callback' ),
            'cs_customer_mails_setting',
            'settings_section_mandrill'
        );

        add_settings_field(
            'reminder-template',
            'Reminder Email Template',
            array( $this, 'reminder_template_callback' ),
            'cs_customer_mails_setting',
            'settings_section_mandrill'
        );

        add_settings_field(
            'cleaner-reminder-template',
            'Cleaner Reminder Email Template',
            array( $this, 'cleaner_reminder_template_callback' ),
            'cs_customer_mails_setting',
            'settings_section_mandrill'
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['api_key'] ) )
            $new_input['api_key'] = sanitize_text_field( $input['api_key'] );

        if( isset( $input['survey-template'] ) )
            $new_input['survey-template'] = sanitize_text_field( $input['survey-template'] );

        if( isset( $input['reminder-template'] ) )
            $new_input['reminder-template'] = sanitize_text_field( $input['reminder-template'] );

        if( isset( $input['cleaner-reminder-template'] ) )
            $new_input['cleaner-reminder-template'] = sanitize_text_field( $input['cleaner-reminder-template'] );

        if( isset( $input['mandrill-subaccount'] ) )
            $new_input['mandrill-subaccount'] = sanitize_text_field( $input['mandrill-subaccount'] );

        if( isset( $input['cleaner-subaccount'] ) )
            $new_input['cleaner-subaccount'] = sanitize_text_field( $input['cleaner-subaccount'] );

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your settings below:';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function api_key_callback()
    {
        printf(
            '<input type="text" id="api_key" name="cs_customer_mails_option[api_key]" value="%s" />',
            isset( $this->options['api_key'] ) ? esc_attr( $this->options['api_key']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function survey_template_callback()
    {
        printf(
            '<input type="text" id="survey-template" name="cs_customer_mails_option[survey-template]" value="%s" />',
            isset( $this->options['survey-template'] ) ? esc_attr( $this->options['survey-template']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function reminder_template_callback()
    {
        printf(
            '<input type="text" id="reminder-template" name="cs_customer_mails_option[reminder-template]" value="%s" />',
            isset( $this->options['reminder-template'] ) ? esc_attr( $this->options['reminder-template']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function cleaner_reminder_template_callback()
    {
        printf(
            '<input type="text" id="cleaner-reminder-template" name="cs_customer_mails_option[cleaner-reminder-template]" value="%s" />',
            isset( $this->options['cleaner-reminder-template'] ) ? esc_attr( $this->options['cleaner-reminder-template']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function mandrill_subaccount_callback()
    {
        printf(
            '<input type="text" id="mandrill-subaccount" name="cs_customer_mails_option[mandrill-subaccount]" value="%s" />',
            isset( $this->options['mandrill-subaccount'] ) ? esc_attr( $this->options['mandrill-subaccount']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function cleaner_subaccount_callback()
    {
        printf(
            '<input type="text" id="cleaner-subaccount" name="cs_customer_mails_option[cleaner-subaccount]" value="%s" />',
            isset( $this->options['cleaner-subaccount'] ) ? esc_attr( $this->options['cleaner-subaccount']) : ''
        );
    }
}
