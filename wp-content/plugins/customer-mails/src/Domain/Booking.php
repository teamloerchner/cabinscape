<?php
namespace Greenhill\CustomerMails\Domain;

class Booking
{
    /**
     * @var int
     */
    private $ID;

    /**
     * @var int
     */
    private $cabinId;

    /**
     * @var string
     */
    private $cabinTitle;

    /**
     * @var \DateTimeImmutable
     */
    private $dateBooked;

    /**
     * @var \DateTimeImmutable
     */
    private $reservationStart;

    /**
     * @var \DateTimeImmutable
     */
    private $reservationEnd;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $extraServices;

    /**
     * @param array $bookingData
     * @return Booking
     * @throws \Exception
     */
    public static function fromBookingData($bookingData)
    {
        $booking = new self();
        $booking->setCabinId($bookingData[0]);
        $booking->setCabinTitle($bookingData[1]);
        $booking->setDateBooked($bookingData[2]);
        $booking->setReservationStart($bookingData[3]);
        $booking->setReservationEnd($bookingData[4]);
        $booking->setExtraServices($bookingData[7]);
        $booking->setFirstName($bookingData[9]);
        $booking->setLastName($bookingData[10]);
        $booking->setEmail($bookingData[11]);
        $booking->setID($bookingData[23]);
        return $booking;
    }

    /**
     * @return int
     */
    public function getID()
    {
        return $this->ID;
    }

    /**
     * @return int
     */
    public function getCabinId()
    {
        return $this->cabinId;
    }

    /**
     * @return string
     */
    public function getCabinTitle()
    {
        return $this->cabinTitle;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getReservationStart()
    {
        return $this->reservationStart->format('Y-m-d');
    }

    /**
     * @return string
     */
    public function getReservationEnd()
    {
        return $this->reservationEnd->format('Y-m-d');
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getDateBooked()
    {
        return $this->dateBooked->format('H:i:s Y-m-d');
    }

    /**
     * @param mixed $ID
     */
    public function setID($ID)
    {
        $this->ID = (int) $ID;
    }

    /**
     * @param mixed $cabinId
     */
    public function setCabinId($cabinId)
    {
        $this->cabinId = (int) $cabinId;
    }

    /**
     * @param mixed $cabinTitle
     */
    public function setCabinTitle($cabinTitle)
    {
        $this->cabinTitle = $cabinTitle;
    }

    /**
     * @param mixed $dateBooked
     */
    public function setDateBooked($dateBooked)
    {
        $this->dateBooked = $dateBooked;
    }

    /**
     * @param mixed $reservationStart
     * @throws \Exception
     */
    public function setReservationStart($reservationStart)
    {
        $this->reservationStart = (new \DateTimeImmutable($reservationStart));
    }

    /**
     * @param mixed $reservationEnd
     * @throws \Exception
     */
    public function setReservationEnd($reservationEnd)
    {
        $this->reservationEnd = (new \DateTimeImmutable($reservationEnd));
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = filter_var($email, FILTER_SANITIZE_EMAIL);
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    private function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setExtraServices($extraServices)
    {
        $this->extraServices = $extraServices;
    }

    public function getExtraServices()
    {
        return $this->extraServices;
    }
}
