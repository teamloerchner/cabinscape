<?php
namespace Greenhill\CustomerMails\Domain;

class MailContent
{
    const FIRST_NAME = 'FNAME';
    const BOOKING_ID = 'BOOKING_ID';
    const ARRIVAL = 'ARRIVAL';
    const CHECKOUT = 'CHECKOUT';
    const EXTRA_SERVICES = 'SERVICE';
    const CABIN = 'cabin';
    const SERVICES_POST_TYPE = "nd_booking_cpt_2";
    const GUEST_FIRST_NAME = 'GUEST_FIRST_NAME';

    /**
     * @param Booking $booking
     * @return array
     */
    public static function mergeVarsFromBooking(Booking $booking)
    {
        $reservationStart = (new \DateTimeImmutable($booking->getReservationStart()))->format('M d, Y');
        $reservationEnd = (new \DateTimeImmutable($booking->getReservationEnd()))->format('M d, Y');

        return array_merge(
            self::parseExtraServices($booking->getExtraServices()),
            [
                ['name' => self::BOOKING_ID, 'content' => $booking->getID()],
                ['name' => self::FIRST_NAME, 'content' => $booking->getFirstName()],
                ['name' => self::GUEST_FIRST_NAME, 'content' => $booking->getFirstName()],
                ['name' => self::ARRIVAL, 'content' => $reservationStart],
                ['name' => self::CHECKOUT, 'content' => $reservationEnd],
                ['name' => self::CABIN, 'content' => $booking->getCabinTitle()],
            ]
        );
    }

    private static function parseExtraServices($extraServices)
    {
        $allServices = self::getAllServices();
        $chosenServices = explode(',', $extraServices);

        $results = array_map(function($service) use ($chosenServices) {
            $serviceID = sprintf('%s_%s', self::EXTRA_SERVICES, $service[0]);
            return ['name' => $serviceID, 'content' => in_array($service[0], $chosenServices)];
        }, $allServices);

        return $results;
    }

    private static function getAllServices()
    {
        global $wpdb;
        return $wpdb->get_results($wpdb->prepare(
            "SELECT ID FROM wp_posts WHERE post_type = %s",
            self::SERVICES_POST_TYPE),
            ARRAY_N
        );
    }
}
