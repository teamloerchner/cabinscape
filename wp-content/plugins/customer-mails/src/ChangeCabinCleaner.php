<?php
namespace Greenhill\CustomerMails;

use Greenhill\CustomerMails\Repository\CustomerMails;
use Monolog\Logger;

class ChangeCabinCleaner
{
    const UPDATED_HOOK = "updated_post_meta";
    const CLEANER_META_KEY = "cleaner_user";
    const API_CALL_RATE_MS = 500000;

    /**
     * @var \Mandrill
     */
    private $mandrill;

    /**
     * @var CustomerMails
     */
    private $customerMails;

    /**
     * @var Logger
     */
    private $logger;

    private function __construct(\Mandrill $mandrill, Logger $log, $registerHook = true) {
        $this->logger = $log;
        $this->mandrill = $mandrill;
        $this->customerMails = new CustomerMails();

        if ($registerHook === true) {
            add_action(self::UPDATED_HOOK, [$this, 'rescheduleCabinEmails'], 5, 3);
        }
    }

    public static function registerChangeCabinCleanerHook(\Mandrill $mandrill, Logger $log) {
        return new self($mandrill, $log);
    }

    public function rescheduleCabinEmails($meta_id, $post_id, $meta_key='', $meta_value='') {
        // if cleaner wasn't updated return early
        if($meta_key !== self::CLEANER_META_KEY) {
            return;
        }

        // get bookings by cabin id
        $results = $this->customerMails->getBookingsByCabinAndBooking($post_id);

        // no bookings found return early
        if ($results === null) {
            return;
        }

        $command = UpdateScheduledEmailsOnChange::getInstance($this->mandrill, $this->customerMails, $this->logger);

        // reschedule cleaner reminder email for each booking found
        foreach($results as $result) {
            extract($result);
            $command->perform(
                $id,
                $start,
                $end,
                $user_first_name,
                $user_last_name,
                $paypal_email
            );

            // wait 0.5 seconds between processing bookings
            usleep(self::API_CALL_RATE_MS);
        }
    }
}
