<?php
namespace Greenhill\CustomerMails\Service;

class CalculateSendDate
{
    const REMINDER_EMAIL_TIME_PERIOD = '-1 week';
    const REMINDER_TIME_OF_DAY = '+6 hours';
    const SURVEY_EMAIL_TIME_PERIOD = '+2 days';
    const CLEANER_REMINDER_TIME_PERIOD = '-1 day';

    public static function forReminderEmail($reservationStart)
    {
        if ((new \DateTimeImmutable($reservationStart))->diff(new \DateTimeImmutable())->days <= 7) {
            return (new \DateTimeImmutable())->modify('+30 minutes')->format('Y-m-d H:i:s');
        }

        return (new \DateTimeImmutable($reservationStart))
            ->modify(self::REMINDER_EMAIL_TIME_PERIOD)
            ->modify(self::REMINDER_TIME_OF_DAY)
            ->format('Y-m-d H:i:s');
    }

    public static function forSurveyEmail($reservationEnd)
    {
        return (new \DateTimeImmutable($reservationEnd))
            ->modify(self::SURVEY_EMAIL_TIME_PERIOD)
            ->format('Y-m-d H:i:s');
    }

    public static function forCleanerReminderEmail($reservationStart)
    {
        return (new \DateTimeImmutable($reservationStart))
            ->modify(self::CLEANER_REMINDER_TIME_PERIOD)
            ->modify(self::REMINDER_TIME_OF_DAY)
            ->format('Y-m-d H:i:s');
    }
}
