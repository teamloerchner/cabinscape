<?php
/**
 * Plugin Name:       Cabinscape Customer Emails
 * Plugin URI:        https://briancurtis.de
 * Description:       Sends scheduled customer emails for pre-booking and post-stay.
 * Version:           0.0.1
 * Author:            Brian Greenhill
 * Author URI:        https://briancurtis.de
 */
require_once __DIR__ . '/vendor/autoload.php';

add_action( 'init', 'cs_customer_mails_plugin_activate' );
register_activation_hook( __FILE__, 'cs_customer_mails_plugin_create_table' );
register_deactivation_hook( __FILE__, 'cs_customer_mails_plugin_deactivate' );

function cs_customer_mails_plugin_activate()
{
    $settingsPage = new \Greenhill\CustomerMails\SettingsPage();
    $settingsPage();

    if(!get_option('cs_customer_mails_option')) {
        return;
    }

    try {
        $mandrill = new \Mandrill(get_option('cs_customer_mails_option')['api_key']);
        $customerMails = new \Greenhill\CustomerMails\Repository\CustomerMails();
        $logger = new \Monolog\Logger('customer-mails');
        $logger->pushHandler(new \Monolog\Handler\StreamHandler(__DIR__ . '/log/mails.log'));

        \Greenhill\CustomerMails\ScheduleEmailsOnBooking::registerBookingHook($mandrill, $logger);
        \Greenhill\CustomerMails\UpdateScheduledEmailsOnChange::registerBookingChangeHook($mandrill, $customerMails, $logger);
        \Greenhill\CustomerMails\CancelScheduledEmailsOnDelete::registerDeleteBookingHook($mandrill, $customerMails, $logger);
        \Greenhill\CustomerMails\ChangeCabinCleaner::registerChangeCabinCleanerHook($mandrill, $logger);
    } catch (Mandrill_Error $e) {
        $logger->error('API Key Invalid', ['exception' => $e]);
    } catch (\Exception $e) {
        $logger->error('Something unexpected happened', ['exception' => $e]);
    }

}

function cs_customer_mails_plugin_create_table()
{
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    global $wpdb;

    $table = $wpdb->prefix . \Greenhill\CustomerMails\Repository\CustomerMails::TABLE_NAME;

    $sql = "
            CREATE TABLE IF NOT EXISTS `$table` (
              id int(11) NOT NULL AUTO_INCREMENT,
              survey_id varchar(100) NOT NULL,
              reminder_id varchar(100) NOT NULL,
              cleaner_reminder_id varchar(100) NOT NULL,
              booking_id varchar(100) NOT NULL,
              PRIMARY KEY (id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
        ";

    dbDelta($sql);
}

function cs_customer_mails_plugin_deactivate()
{
    // down database?
    // unschedule emails?
}
