(function ($) {
	function showError(message) {
		var errorWrapper = document.querySelector('#payment-form .form-message');
		errorWrapper.classList.remove('nd_booking_display_none');
		errorWrapper.classList.add('has-error');
		errorWrapper.innerHTML = message;
	}
	function hideError() {
		var errorWrapper = document.querySelector('#payment-form .form-message');
		errorWrapper.classList.add('nd_booking_display_none');
		errorWrapper.innerHTML = "";
	}
	function enableSubmitButton() {
		var form = document.getElementById("payment-form");
		var submitButton = form.querySelector("[type=submit]");
		submitButton.removeAttribute("disabled");
	}
	function handleOverlappingDates(message, roomUrl) {
		var errorMessage = message + ' <a class="form-message__room-link" href="' + roomUrl + '">Return to booking page</a>';
		showError(errorMessage);
	}
	function handlePaymentError(message) {
		showError(message);
		enableSubmitButton();
	}
	function handleCheckoutResponse(response) {
		if (response.status === 'success') {
			window.location.href = response.data.confirmation_page;
		} else if (response.status === 'fail') {
			if (response.data.dates) {
				handleOverlappingDates(response.data.dates, response.data.room_url);
			} else if (response.data.payment) {
				handlePaymentError(response.data.payment);
			}
		} else {
			var message = response.message || 'An unknown error has occurred. Please try again.';
			showError(message);
		}
	}
	function startCheckout(event) {
		var formData = new FormData(event.detail);
		formData.append('action', 'nd_booking_checkout_php_function');
		hideError();
		$.ajax({
			type: "POST",
			url: nd_booking_my_vars_checkout.nd_booking_ajaxurl_checkout,
			data: formData,
			processData: false,
			contentType: false,
			dataType: 'json',
			success: handleCheckoutResponse
		});
	}
	document.addEventListener('checkout', startCheckout);
})(jQuery);
