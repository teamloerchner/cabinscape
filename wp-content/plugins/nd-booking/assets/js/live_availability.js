function getBookingClass(index, bookingCount){
    if(!index) return "";
    switch (index){
        case 0:
            return "first-day-of-booking";
        case bookingCount:
            return "last-day-of-booking";
        default:
            return "middle-of-booking";
    }
}
function checkDate(date, previousBookings, isFrom){
    var bookingClasses = getDateClassList(date, previousBookings, true);
    var bookingClassText = (bookingClasses.length) ? bookingClasses.join(" ") : "";
    var isBooked = getBookingStatus(bookingClasses, isFrom);
    return [!isBooked, bookingClassText];
}
function checkDateRangeOverlap(dateToCheck, dateRanges, callback){
    var result = {};
    result.datesOverlap = dateRanges.some(function(dateRange){
        var testDate = {
            date_from: new Date(dateRange.date_from).getTime(),
            date_to: new Date(dateRange.date_to).getTime()
        };
        if ((dateToCheck.date_to <= testDate.date_from) || (dateToCheck.date_from >= testDate.date_to)) {
            return false;
        }else{
            if(callback && dateRange.minDateRange) callback(dateRange.minDateRange);
            return true;
        }
    }, this);
    return result;
}
function getNightsBetween(date1, date2) {
    var ONE_DAY = 1000 * 60 * 60 * 24;
    var diff = Math.abs(date1 - date2);

    return Math.round(diff / ONE_DAY);
}
function validateEndDate(date, previousBookings){
    var startDate = new Date($("#nd_booking_archive_form_date_range_from").val()).getTime();
    var endDate = new Date(date).getTime();
    var isInvalid = previousBookings.startDates.some(function(previousDate){
        return (startDate < previousDate && previousDate < endDate);
    });
    return {
        valid: !isInvalid,
        message: isInvalid ? "End date cannot be after the start of another booking date" : ""
    };
}
function validateNumberNights(options){
    var checkDate = {
        date_from: new Date(options.start_date).getTime(),
        date_to: new Date(options.end_date).getTime(),
    };
    var duration = options.minDuration;
    checkDateRangeOverlap(checkDate, options.dateRangeExceptions, function hasOverlap(minDuration){
        duration = minDuration;
    });
    checkDate.duration = getNightsBetween(checkDate.date_from, checkDate.date_to);
    var isValid = checkDate.duration >= duration;
    return {
        valid: isValid,
        message: !isValid ? "Booking during these dates cannot be shorter than " + duration + " nights" : ""
    };
}
function getDateClassList(date, previousBookings){
    var classes = [];
    var dateToCheck = new Date(date).getTime();

    // Booking Classes
    var isFirstDay = previousBookings.startDates.some(function(startDate){
        if(dateToCheck === startDate){
            classes.push("first-day-of-booking");
            return true;
        }
    });
    var isLastDay = previousBookings.endDates.some(function(endDate){
        if(dateToCheck === endDate){
            classes.push("last-day-of-booking");
            return true;
        }
    });
    if(!isFirstDay && !isLastDay){
        var isInnerDay = previousBookings.allDates.some(function(previousDate){
            if(previousDate.date_from < dateToCheck && dateToCheck < previousDate.date_to){
                classes.push("date-booked");
                return true;
            }
        });
    }else{
        classes.push("date-booked");
    }

    // Selected Classes
    var currentSelectionStart = new Date($("#nd_booking_archive_form_date_range_from").val()).getTime();
    var currentSelectionEnd = new Date($("#nd_booking_archive_form_date_range_to").val()).getTime();
    if(currentSelectionStart <= dateToCheck && dateToCheck <= currentSelectionEnd){
        classes.push("currently-selected");
    }

    return classes;
}

function getBookingStatus(bookingClasses, isFrom){
    var isBooked = false;
    if(bookingClasses.indexOf("date-booked") !== -1 && bookingClasses.indexOf("first-day-of-booking") === -1 && bookingClasses.indexOf("last-day-of-booking") === -1){
        isBooked = true;
    }else if(bookingClasses.indexOf("first-day-of-booking") !== -1 && bookingClasses.indexOf("last-day-of-booking") !== -1){
        isBooked = true;
    }else if(isFrom && bookingClasses.indexOf("first-day-of-booking") !== -1){
        isBooked = true;
    }else if(!isFrom && bookingClasses.indexOf("last-day-of-booking") !== -1){
        isBooked = true;
    }
    return isBooked;
}

var availableDates;
function getAvailability(){
    if(!availableDates){
        availableDates = jQuery.get(
            //ajax definition
            nd_booking_live_availability.nd_booking_ajaxurl_live_availability,
            {
              action : 'nd_booking_live_availability_php_function',
              nd_booking_id : nd_booking_live_availability.live_availability_booking_id
            },
            //Success callback
            function(availability) {
                return availability;
            },
            "json"
        );
    }
    return availableDates;
}

function showFormError(message){
    $("#nd_booking_single_cpt_1_calendar_btn").append("<span class='error'>" + message + "</span>");
}
function clearFormError(){
    $("#nd_booking_single_cpt_1_calendar_btn .error").remove();
}

(function($){
    $(document).ready(function(){
        $('#nd_booking_single_cpt_1_calendar').submit(function(e){
            e.preventDefault();
            clearFormError();
            var form = this;

            getAvailability().then(function(availability){
                // Get booking form data
                var values = $(form).serializeArray();
                var formData = values.reduce(function(accData, curValue){
                    accData[curValue.name] = curValue.value;
                    return accData;
                }, {});

                // Check booking date against previously booked dates
                var booking = checkDateRangeOverlap({
                    date_from: new Date(formData.nd_booking_archive_form_date_range_from).getTime(),
                    date_to: new Date(formData.nd_booking_archive_form_date_range_to).getTime()
                }, availability.bookedDates);
                if(booking.datesOverlap){
                    showFormError("The selected date range already contains a booking.");
                    return false;
                }

                // Check booking date against minimum nights
                var minimumNights = validateNumberNights({
                    start_date: formData.nd_booking_archive_form_date_range_from,
                    end_date: formData.nd_booking_archive_form_date_range_to,
                    dateRangeExceptions: availability.dateRangeExceptions,
                    minDuration: availability.minDateRange
                });
                if(!minimumNights.valid){
                    showFormError(minimumNights.message);
                    return false;
                }

                // Valid form
                form.submit();
            });
        });
    });
})(jQuery);