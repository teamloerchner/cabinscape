<?php
function nd_booking_shortcode_confirmation()
{
	if (!isset($_COOKIE['confirmation_id'])) {
		$default_message = '
			<h2>Order complete</h2>
			<p>Check your email for confirmation details.</p>';
		echo $default_message;
		return;
	}

	// Retrive saved booking
	global $wpdb;
	$booking_tablename = $wpdb->prefix . "nd_booking_booking";
	$booking_id = $_COOKIE["confirmation_id"];
	$sql = $wpdb->prepare("SELECT * FROM $booking_tablename WHERE paypal_tx = %s", $booking_id);
	$booking = $wpdb->get_row($sql, ARRAY_A);

	include 'include/thankyou/nd_booking_thankyou_left_content.php';
	include 'include/thankyou/nd_booking_thankyou_right_content.php';

	$nd_booking_shortcode_result .= '
	<div class="nd_booking_section">
		<div class="nd_booking_float_left nd_booking_width_33_percentage nd_booking_width_100_percentage_responsive nd_booking_padding_0_responsive nd_booking_padding_right_15 nd_booking_box_sizing_border_box">
			' . $nd_booking_shortcode_left_content . '
		</div>
		<div class="nd_booking_float_left nd_booking_width_66_percentage nd_booking_width_100_percentage_responsive nd_booking_padding_0_responsive nd_booking_padding_left_15 nd_booking_box_sizing_border_box">
			' . $nd_booking_shortcode_right_content . '
		</div>
	</div>';

	echo $nd_booking_shortcode_result;
}
add_shortcode('nd_booking_confirmation', 'nd_booking_shortcode_confirmation');
