<?php

function booking_calendar_function( $atts ) {
    ob_start();
	?>
    <div class="booking-calendar booking-display-only"></div>
    <script type="text/javascript">
    <!--//--><![CDATA[//><!--
        jQuery(document).ready(function($) {
            function initBookingCalendar(availability){
                var allDates = availability.bookedDates.map(function(bookingDate){
                    return {
                        date_from: new Date(bookingDate.date_from).getTime(),
                        date_to: new Date(bookingDate.date_to).getTime()
                    };
                });
                var previousBookings = {
                    startDates: allDates.map(function(bookingDate){
                        return bookingDate.date_from;
                    }),
                    endDates: allDates.map(function(bookingDate){
                        return bookingDate.date_to;
                    }),
                    allDates: allDates
                };
                var today = new Date().getTime();
                var datepickerOptions = {
                    numberOfMonths: 3,
                    minDate: 0
                };
                datepickerOptions.beforeShowDay = function(date){
                        var dateStatus = checkDate(date, previousBookings, true);
                        var dateClasses = dateStatus[1].split(' ');
                        var checkedDate = new Date(date).getTime();
                        dateStatus[0] = false;
                        if(dateClasses.indexOf('date-booked') === -1 && checkedDate >= today){
                            dateClasses.push('date-available');
                        }
                        dateStatus[1] = dateClasses.join(' ');
                        return dateStatus;
                };
                datepickerOptions.onChangeMonthYear = function(year, month, instance){
                    //Hack to prevent hilighting current date
                    setTimeout(function() {
                        $('.booking-calendar td.ui-datepicker-current-day').removeClass('ui-datepicker-current-day');
                    }, 0);
                };
                <?php if($atts['max_date']):
                echo 'datepickerOptions.maxDate = "'.$atts['max_date'].'";';
                endif; ?>
                $('.booking-calendar').datepicker(datepickerOptions).find('td.ui-datepicker-current-day').removeClass('ui-datepicker-current-day');
            }
            getAvailability().then(initBookingCalendar);
        });
    //--><!]]>
    </script>
    <?php
	return ob_get_clean();
    // return $booking_calendar_result;
	// return "foo = {$atts['foo']}";
}
add_shortcode( 'booking_calendar', 'booking_calendar_function' );
