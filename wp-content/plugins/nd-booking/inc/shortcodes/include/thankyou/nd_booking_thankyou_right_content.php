<?php

$nd_booking_shortcode_right_content = '';
$address = unfuckAddress($booking["user_address"]);

$headerNotice = 'Payment Successful';

$nd_booking_shortcode_right_content .= '

	<h1 class="nd_booking_margin_top_50_responsive">
		' . __($headerNotice  . '</br>Your Order Details', 'nd-booking') . ' :
	</h1>
	<div class="nd_booking_section nd_booking_height_30"></div>
	<p>
		Your payment has been processed! You will receive a confirmation email of your booking momentarily.</br>
		Please check your junk mail if you do not receive it soon.</br>
	</p>
	</br></br>
	<div class="nd_booking_width_100_percentage nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">
			' . __('Order Transaction', 'nd-booking') . ' :</span>
		</p>
		<p>' . $booking["paypal_tx"] . '</p>
	</div>
	<div class="nd_booking_section nd_booking_height_30"></div>
	<div class="nd_booking_width_33_percentage nd_booking_width_100_percentage_all_iphone nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">
			' . __('Name', 'nd-booking') . ' :</span>
			' . $booking["user_first_name"] . '
		</p>
	</div>
	<div class="nd_booking_width_33_percentage nd_booking_width_100_percentage_all_iphone nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">
			' . __('Surname', 'nd-booking') . ' :</span>
			' . $booking["user_last_name"] . '
		</p>
	</div>
	<div class="nd_booking_width_33_percentage nd_booking_width_100_percentage_all_iphone nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">
			' . __('Email', 'nd-booking') . ' :</span>
			' . $booking["paypal_email"] . '
		</p>
	</div>
	<div class="nd_booking_section nd_booking_height_10"></div>
	<div class="nd_booking_width_33_percentage nd_booking_width_100_percentage_all_iphone nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">
			' . __('Phone', 'nd-booking') . ' :</span>
			' . $booking["user_phone"] . '
		</p>
	</div>
	<div class="nd_booking_width_33_percentage nd_booking_width_100_percentage_all_iphone nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">
			' . __('Address', 'nd-booking') . ' :</span>
			' . $address["address"] . '
		</p>
	</div>
	<div class="nd_booking_width_33_percentage nd_booking_width_100_percentage_all_iphone nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">
			' . __('City', 'nd-booking') . ' :</span>
			' . $booking["user_city"] . '
		</p>
	</div>
	<div class="nd_booking_section nd_booking_height_10"></div>
	<div class="nd_booking_width_33_percentage nd_booking_width_100_percentage_all_iphone nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">
			' . __('Country', 'nd-booking') . ' :</span>
			' . $booking["user_country"] . '
		</p>
	</div>
	<div class="nd_booking_width_33_percentage nd_booking_width_100_percentage_all_iphone nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">
			' . __('ZIP', 'nd-booking') . ' :</span>
			' . $address["zip"] . '
		</p>
	</div>
	<div class="nd_booking_width_33_percentage nd_booking_width_100_percentage_all_iphone nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">
			' . __('Arrival', 'nd-booking') . ' :</span>
			' . $booking["user_arrival"] . '
		</p>
	</div>
	<div class="nd_booking_section nd_booking_height_30"></div>
	<div class="nd_booking_width_100_percentage nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">
			' . __('Requests', 'nd-booking') . ' :</span>
		</p>
		<p>' . $booking["user_message"] . '</p>
	</div>
	<div class="nd_booking_section nd_booking_height_30"></div>
	<div class="nd_booking_width_100_percentage nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">
			' . __('Extra Services', 'nd-booking') . ' :</span>
		</p>';

$extra_services = $booking["extra_services"];
if ($extra_services == '') {
	$nd_booking_shortcode_right_content .= '<p>' . __('You have not selected any additional services', 'nd-booking') . '</p>';
} else {
	$nd_booking_shortcode_right_content .= '<div class="nd_booking_section nd_booking_height_10"></div>';
	$nd_booking_services_array = explode(',', $extra_services);
	$nd_booking_shortcode_right_content .= array_reduce($nd_booking_services_array, function ($formatted, $service_group) {
		if (!$service_group) return $formatted;
		$service_id = substr($service_group, 0, strpos($service_group, '['));
		$service_name = get_the_title($service_id);
		$service_icon_src = get_post_meta($service_id, 'nd_booking_meta_box_cpt_2_icon', true);
		$service_icon = ($service_icon_src)
			? '<img alt="" class="nd_booking_margin_right_15 nd_booking_display_table_cell nd_booking_vertical_align_middle" width="25" src="' . $service_icon_src . '">'
			: '';
		return $formatted .= '
			<div class="nd_booking_width_33_percentage nd_booking_width_100_percentage_all_iphone nd_booking_float_left">
				<div class="nd_booking_display_table nd_booking_float_left">
					' . $service_icon . '
					<p class="nd_booking_display_table_cell nd_booking_vertical_align_middle nd_booking_line_height_20">
						' . $service_name . '
					</p>
				</div>
			</div>';
	}, "");
}

$nd_booking_shortcode_right_content .= '</div>';

$nd_booking_shortcode_right_content .= '
	<div class="nd_booking_section nd_booking_height_30 ' . nd_booking_get_coupon_enable_class() . ' "></div>
	<div class="nd_booking_width_100_percentage ' . nd_booking_get_coupon_enable_class() . ' nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">' . __('Coupon', 'nd-booking') . ' :</span></p>';

if ($booking["user_coupon"] != '') {
	$nd_booking_shortcode_right_content .= '<p>' . $booking["user_coupon"] . ' - ' . __('Value $', 'nd-booking') . ' ' . nd_booking_get_coupon_value($booking["user_coupon"]) . ' ' . __(' off', 'nd-booking') . ' </p>';
} else {
	$nd_booking_shortcode_right_content .= '<p>' . __('Not set', 'nd-booking') . '</p>';
}

$nd_booking_shortcode_right_content .= '
	</div>';

$nd_booking_shortcode_right_content .= '
	<div class="nd_booking_section nd_booking_height_30"></div>
	<div class="nd_booking_width_100_percentage nd_booking_float_left nd_booking_box_sizing_border_box ">
		<p><span class="nd_options_color_greydark nd_booking_font_weight_bolder">
			' . __('Payment Options', 'nd-booking') . ' :</span>
		</p>
		<p class="nd_booking_text_transform_capitalize">
			' . __('Stripe', 'nd-booking') . '
		</p>
	</div>
	<div class="nd_booking_section nd_booking_height_30"></div>';
