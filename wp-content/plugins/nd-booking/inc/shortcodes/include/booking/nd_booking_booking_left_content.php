<?php


$nd_booking_shortcode_left_content = '';

//START price
$nd_booking_trip_price_for_person = 0;

$date_interval = $interval = new DateInterval('P1D');
$end = clone $date_to;
$booking_period = new DatePeriod($date_from, $date_interval, $end);

foreach ($booking_period as $booking_date) {
  $nd_booking_trip_price_for_person += nd_get_price_on_date($nd_booking_form_booking_id, $booking_date);
}

$nd_booking_price_guests_enable = get_option('nd_booking_price_guests');
if ($nd_booking_price_guests_enable == 1) {
  //Check that there are more than two guests, if so add additional cost if not just add cleaning fee
  if ($nd_booking_form_booking_guests > 2) {
    $nd_booking_trip_price = ($nd_booking_trip_price_for_person + (($nd_booking_form_booking_guests - 2) * 25) * $nd_number_of_nights) + 40;
  } else {
    $nd_booking_trip_price = ($nd_booking_trip_price_for_person + 40);
  }
} else {
  $nd_booking_trip_price = ($nd_booking_trip_price_for_person + 40);
}
//END price

//image
$nd_booking_image_src = nd_booking_get_post_img_src($nd_booking_form_booking_id);
if ($nd_booking_image_src != '') {

  $nd_booking_image = '
    <div class="nd_booking_section nd_booking_position_relative">
      <img class="nd_booking_section" src="' . $nd_booking_image_src . '">
      <div class="nd_booking_bg_greydark_alpha_gradient_3 nd_booking_position_absolute nd_booking_left_0 nd_booking_height_100_percentage nd_booking_width_100_percentage nd_booking_padding_30 nd_booking_box_sizing_border_box">
        <div class="nd_booking_position_absolute nd_booking_top_20">
          <p class="nd_options_color_white nd_booking_float_left nd_booking_font_size_11 nd_booking_padding_3_5 nd_booking_bg_greydark nd_booking_letter_spacing_2 nd_booking_text_transform_uppercase">
            ' . get_the_title($nd_booking_form_booking_id) . '
          </p>
        </div>
      </div>
    </div>';
} else {
  $nd_booking_image = '';
}

$nd_booking_shortcode_left_content .= '

<div class="nd_booking_section nd_booking_box_sizing_border_box">
  ' . $nd_booking_image . '
  <!--START black section-->
  <div id="nd_booking_book_main_bg" class="nd_booking_section nd_booking_bg_greydark nd_booking_padding_30 nd_booking_padding_0_all_iphone nd_booking_box_sizing_border_box">
    <h6 class="nd_options_second_font nd_booking_margin_top_20_all_iphone nd_options_color_white nd_booking_letter_spacing_2 nd_booking_text_align_center nd_booking_font_size_12 nd_booking_font_weight_lighter">
      ' . __('YOUR RESERVATION', 'nd-booking') . '
    </h6>
    <div class="nd_booking_section nd_booking_height_30"></div>
    <div class="nd_booking_width_50_percentage nd_booking_float_left  nd_booking_padding_right_10 nd_booking_box_sizing_border_box ">
      <div id="nd_booking_book_bg_check_in" class="nd_booking_section nd_booking_bg_greydark_2 nd_booking_padding_20 nd_booking_box_sizing_border_box nd_booking_text_align_center">
        <h6 class="nd_options_color_white nd_booking_color_yellow_important nd_options_second_font nd_booking_letter_spacing_2 nd_booking_font_size_12 nd_booking_font_weight_lighter">
          ' . __('CHECK-IN', 'nd-booking') . '
        </h6>
        <div class="nd_booking_section nd_booking_height_15"></div>
        <h1 class="nd_booking_font_size_50 nd_booking_color_yellow_important">
          ' . date_format($date_from, 'd') . '
        </h1>
        <div class="nd_booking_section nd_booking_height_15"></div>
        <h6 class="nd_options_color_white nd_booking_font_size_11">
          <i>' . date_format($date_from, 'M') . ', ' . date_format($date_from, 'Y') . '</i>
        </h6>
        <div class="nd_booking_section nd_booking_height_5"></div>
        <h6 class="nd_options_second_font nd_options_color_grey nd_booking_font_size_11 nd_booking_letter_spacing_2 nd_booking_font_weight_lighter nd_booking_text_transform_uppercase">
          ' . date_format($date_from, 'l') . '
        </h6>
      </div>
    </div>
    <div class="nd_booking_width_50_percentage nd_booking_float_left  nd_booking_padding_left_10 nd_booking_box_sizing_border_box ">
      <div id="nd_booking_book_bg_check_out" class="nd_booking_section nd_booking_bg_greydark_2 nd_booking_padding_20 nd_booking_box_sizing_border_box nd_booking_text_align_center">
        <h6 class="nd_options_color_white nd_booking_color_yellow_important nd_options_second_font nd_booking_letter_spacing_2 nd_booking_font_size_12 nd_booking_font_weight_lighter">
          ' . __('CHECK-OUT', 'nd-booking') . '
        </h6>
        <div class="nd_booking_section nd_booking_height_15"></div>
        <h1 class="nd_booking_font_size_50 nd_booking_color_yellow_important">
          ' . date_format($date_to, 'd') . '
        </h1>
        <div class="nd_booking_section nd_booking_height_15"></div>
        <h6 class="nd_options_color_white nd_booking_font_size_11">
          <i>' . date_format($date_to, 'M') . ', ' . date_format($date_to, 'M') . '</i>
        </h6>
        <div class="nd_booking_section nd_booking_height_5"></div>
        <h6 class="nd_options_second_font nd_options_color_grey nd_booking_font_size_11 nd_booking_letter_spacing_2 nd_booking_font_weight_lighter nd_booking_text_transform_uppercase">
          ' . date_format($date_to, 'l') . '
        </h6>
      </div>
    </div>
    <div class="nd_booking_section nd_booking_height_20"></div>
    <div class="nd_booking_width_50_percentage nd_booking_float_left  nd_booking_padding_right_10 nd_booking_box_sizing_border_box ">
      <div id="nd_booking_book_bg_guests" class="nd_booking_section nd_booking_bg_greydark_2 nd_booking_padding_20 nd_booking_box_sizing_border_box nd_booking_text_align_center">
        <h1 class=" nd_options_color_white">' . $nd_booking_form_booking_guests . '</h1>
        <div class="nd_booking_section nd_booking_height_10"></div>
        <h6 class="nd_options_second_font nd_options_color_grey nd_booking_font_size_11 nd_booking_letter_spacing_2 nd_booking_font_weight_lighter">
          ' . __('GUESTS', 'nd-booking') . '
        </h6>
      </div>
    </div>
    <div class="nd_booking_width_50_percentage nd_booking_float_left  nd_booking_padding_left_10 nd_booking_box_sizing_border_box ">
      <div id="nd_booking_book_bg_nights" class="nd_booking_section nd_booking_bg_greydark_2 nd_booking_padding_20 nd_booking_box_sizing_border_box nd_booking_text_align_center">
        <h1 class=" nd_options_color_white">' . $nd_number_of_nights . '</h1>
        <div class="nd_booking_section nd_booking_height_10"></div>
        <h6 class="nd_options_second_font nd_options_color_grey nd_booking_font_size_11 nd_booking_letter_spacing_2 nd_booking_font_weight_lighter">
          ' . __('NIGHTS', 'nd-booking') . '
        </h6>
      </div>
    </div>
  </div>
  <div id="nd_booking_book_bg_total" class="nd_booking_section nd_booking_bg_greydark_2 nd_booking_padding_30 nd_booking_box_sizing_border_box nd_booking_text_align_center">
    <div class="nd_booking_section nd_booking_box_sizing_border_box nd_booking_text_align_center">
      <div class="nd_booking_display_inline_block ">
        <div id="nd_booking_final_trip_price_content" class="nd_booking_float_left nd_booking_text_align_right">
          <h1 id="nd_booking_final_trip_price" class="nd_options_color_white nd_booking_font_size_50">
            <span>' . $nd_booking_trip_price . '</span>
          </h1>
        </div>
        <div class="nd_booking_float_right nd_booking_text_align_left nd_booking_margin_left_10">
          <h5 class="nd_options_second_font nd_options_color_white nd_booking_margin_top_7 nd_booking_font_size_14 nd_booking_font_weight_lighter">
            ' . nd_booking_get_currency() . '
            <div class="nd_booking_section nd_booking_height_5"></div>
          </h5>
          <h3 class="nd_options_second_font nd_options_color_white nd_booking_font_size_14 nd_booking_letter_spacing_2 nd_booking_font_weight_lighter">/
            ' . __('TOTAL', 'nd-booking') . '
          </h3>
        </div>
      </div>
      </br>' . $nd_number_of_nights . ' Nights = $' . $nd_booking_trip_price_for_person . '
      </br>+ $40 Cleaning Fee
      </br>+ 13% HST will be added to your total on checkout
    </div>
  </div>
  <!--END black section-->';

$nd_booking_shortcode_left_content .= '
  <div style="padding-top:20px;" class="nd_booking_section nd_booking_box_sizing_border_box nd_booking_padding_30">';

$nd_booking_shortcode_left_content .= '
  </div>';

$nd_booking_shortcode_left_content .= '
</div>';
