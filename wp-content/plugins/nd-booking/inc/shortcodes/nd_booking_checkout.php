<?php

function nd_booking_format_phone($phone_number)
{
	// Strip out anything not a number
	$phone_digits = preg_replace("/[^0-9]/", '', $phone_number);

	// Remove leading 1 if number is 11 digits long
	if (strlen($phone_digits) == 11) $phone_digits = preg_replace("/^1/", '', $phone_digits);

	return preg_replace('/(\d{3})(\d{3})(\d{4})/', "$1-$2-$3", $phone_digits);
}
//START  nd_booking_checkout
function nd_booking_shortcode_checkout()
{
	wp_enqueue_script('nd_booking_checkout', plugins_url() . '/nd-booking/assets/js/checkout.js', array('jquery'));
	wp_localize_script('nd_booking_checkout', 'nd_booking_my_vars_checkout', array('nd_booking_ajaxurl_checkout' => admin_url('admin-ajax.php')));

	$nd_booking_shortcode_result = '';
	if (isset($_POST['nd_booking_form_booking_arrive'])) {
		$nd_booking_form_booking_arrive = $_POST['nd_booking_form_booking_arrive'];
	} else {
		$nd_booking_form_booking_arrive = '';
	}
	if (isset($_POST['nd_booking_form_checkout_arrive'])) {
		$nd_booking_form_checkout_arrive = $_POST['nd_booking_form_checkout_arrive'];
	} else {
		$nd_booking_form_checkout_arrive = '';
	}

	//ARRIVE FROM BOOKING FORM
	if ($nd_booking_form_booking_arrive == 1) {
		//get value
		$nd_booking_booking_form_final_price = $_POST['nd_booking_booking_form_final_price'];
		$nd_booking_booking_form_date_from = $_POST['nd_booking_booking_form_date_from'];
		$nd_booking_booking_form_date_to = $_POST['nd_booking_booking_form_date_to'];
		$nd_booking_booking_form_guests = $_POST['nd_booking_booking_form_guests'];
		$nd_booking_booking_form_name = $_POST['nd_booking_booking_form_name'];
		$nd_booking_booking_form_surname = $_POST['nd_booking_booking_form_surname'];
		$nd_booking_booking_form_email = $_POST['nd_booking_booking_form_email'];
		$nd_booking_booking_form_phone = nd_booking_format_phone($_POST['nd_booking_booking_form_phone']);
		$nd_booking_booking_form_address = $_POST['nd_booking_booking_form_address'];
		$nd_booking_booking_form_city = $_POST['nd_booking_booking_form_city'];
		$nd_booking_booking_form_country = $_POST['nd_booking_booking_form_country'];
		$nd_booking_booking_form_zip = $_POST['nd_booking_booking_form_zip'];
		$nd_booking_booking_form_requests = $_POST['nd_booking_booking_form_requests'];
		$nd_booking_booking_form_arrival = $_POST['nd_booking_booking_form_arrival'];
		$nd_booking_booking_form_coupon = $_POST['nd_booking_booking_form_coupon'];
		$nd_booking_booking_form_term = $_POST['nd_booking_booking_form_term'];
		$nd_booking_booking_form_post_id = $_POST['nd_booking_booking_form_post_id'];
		$nd_booking_booking_form_post_title = $_POST['nd_booking_booking_form_post_title'];
		$nd_booking_booking_form_services = $_POST['nd_booking_booking_checkbox_services_id'];

		//ids
		$nd_booking_booking_form_post_id = $_POST['nd_booking_booking_form_post_id'];
		$nd_booking_ids_array = explode('-', $nd_booking_booking_form_post_id);
		$nd_booking_booking_form_post_id = $nd_booking_ids_array[0];
		$nd_booking_id_room = $nd_booking_ids_array[1];

		//city tax
		if (get_option('nd_booking_city_tax') != '') {
			//  $nd_booking_total_city_tax = get_option('nd_booking_city_tax') * $nd_booking_booking_form_guests * nd_booking_get_number_night($nd_booking_booking_form_date_from,$nd_booking_booking_form_date_to);
			$nd_booking_booking_form_final_price = $nd_booking_booking_form_final_price * (1 + (get_option('nd_booking_city_tax') / 100));
		}

		include 'include/checkout/nd_booking_checkout_left_content.php';
		include 'include/checkout/nd_booking_checkout_right_content.php';
		include 'include/checkout/nd_booking_checkout_payment_options.php';

		$nd_booking_shortcode_result .= '
		<div class="nd_booking_section">
			BOOKING DATE IS:: ' . $nd_booking_booking_form_date_from . '
			<div class="nd_booking_float_left nd_booking_width_33_percentage nd_booking_width_100_percentage_responsive nd_booking_padding_0_responsive nd_booking_padding_right_15 nd_booking_box_sizing_border_box">
				' . $nd_booking_shortcode_left_content . '
			</div>
			<div class="nd_booking_float_left nd_booking_width_66_percentage nd_booking_width_100_percentage_responsive nd_booking_padding_0_responsive nd_booking_padding_left_15 nd_booking_box_sizing_border_box">
				' . $nd_booking_shortcode_right_content . '
			</div>
		</div>';
	} else {
		$nd_booking_shortcode_result .= '
		<div class="nd_booking_section">
			<div class="nd_booking_float_left nd_booking_width_100_percentage nd_booking_box_sizing_border_box">
				<p>' . __('Please select a room to make a reservation', 'nd-booking') . '</p>
				<div class="nd_booking_section nd_booking_height_20"></div>
				<a href="' . nd_booking_search_page() . '" class="nd_booking_bg_yellow nd_booking_padding_15_30_important nd_options_second_font_important nd_booking_border_radius_0_important nd_options_color_white nd_booking_cursor_pointer nd_booking_display_inline_block nd_booking_font_size_11 nd_booking_font_weight_bold nd_booking_letter_spacing_2">
					' . __('RETURN TO SEARCH PAGE', 'nd-booking') . '
				</a>
			</div>
		</div>';
	}
	echo $nd_booking_shortcode_result;
}
add_shortcode('nd_booking_checkout', 'nd_booking_shortcode_checkout');
//END nd_booking_checkout

function format_extra_services($service_list, $guests, $duration)
{
	//START extra services
	$services = explode(',', $service_list);

	return array_reduce($services, function ($formatted, $service_id) use ($guests, $duration) {
		if (!$service_id) return $formatted;
		$extra_services_meta = get_post_meta($service_id);
		$price = (float)$extra_services_meta['nd_booking_meta_box_cpt_2_price'][0];
		$price_multiplier_1 = ($extra_services_meta['nd_booking_meta_box_cpt_2_price_type_1'][0] == 'nd_booking_price_type_person')
			? (int)$guests
			: 1;
		$price_multiplier_2 = ($extra_services_meta['nd_booking_meta_box_cpt_2_price_type_2'][0] == 'nd_booking_price_type_day')
			? (int)$duration
			: 1;
		$total_price = $price * $price_multiplier_1 * $price_multiplier_2;
		return $formatted .= "{$service_id}[{$total_price}],";
	}, "");
}

function get_room_id($nd_booking_checkout_form_post_id)
{
	$nd_booking_ids_array = explode('-', $nd_booking_checkout_form_post_id);
	return $nd_booking_ids_array[1];
}

function get_booking_details($room_id)
{
	$guests = $_POST['nd_booking_checkout_form_guests'];
	$date_from = $_POST['nd_booking_checkout_form_date_from'];
	$date_to = $_POST['nd_booking_checkout_form_date_top'];
	$duration = nd_get_nights_from_string($date_from, $date_to);
	$booking_details = array(
		'id_post' => $room_id,
		'title_post' => get_the_title($room_id),
		'date_from' => $date_from,
		'date_to' => $date_to,
		'guests' => $guests,
		'final_trip_price' => $_POST['nd_booking_checkout_form_final_price'],
		'extra_services' => format_extra_services(
			$_POST['nd_booking_booking_form_services'],
			$guests,
			$duration
		),
		'id_user' => 0, // No support needed for logged-in users
		'user_first_name' => $_POST['nd_booking_checkout_form_name'],
		'user_last_name' => $_POST['nd_booking_checkout_form_surname'],
		'paypal_email' => $_POST['nd_booking_checkout_form_email'],
		'user_phone' => $_POST['nd_booking_checkout_form_phone'],
		'user_address' => $_POST['nd_booking_checkout_form_address'] . ' ' . $_POST['nd_booking_checkout_form_zip'],
		'user_city' => $_POST['nd_booking_checkout_form_city'],
		'user_country' => $_POST['nd_booking_checkout_form_country'],
		'user_message' => $_POST['nd_booking_checkout_form_requets'],
		'user_arrival' => $_POST['nd_booking_checkout_form_arrival'],
		'user_coupon' => $_POST['nd_booking_checkout_form_coupon'],
		'paypal_payment_status' => 'Pending Payment',
		'paypal_currency' => nd_booking_get_currency(),
		'action_type' => $_POST['nd_booking_booking_form_action_type']
	);
	return $booking_details;
}

function format_stripe_query($booking_details, $stripe_token)
{
	$booking_amount = $booking_details['final_trip_price'] * 100;
	$booking_currency = get_option('nd_booking_stripe_currency');
	$booking_description = $booking_details['title_post'] . ' - ' . $booking_details['user_first_name'] . ' ' . $$booking_details['user_last_name'] . ' - ' . $booking_details['date_from'] . ' ' . $booking_details['date_to'];
	$stripe_params = array(
		'amount' => $booking_amount,
		'currency' => $booking_currency,
		'description' => $booking_description,
		'source' => $stripe_token,
		'metadata[date_from]' => $booking_details['date_from'],
		'metadata[date_to]' => $booking_details['date_to'],
		'metadata[guests]' => $booking_details['guests'],
		'metadata[name]' => $booking_details['user_first_name'] . ' ' . $booking_details['user_last_name'],
		'metadata[email]' => $booking_details['paypal_email'],
		'metadata[phone]' => $booking_details['user_phone'],
		'metadata[address]' => $booking_details['user_address'] . ' ' . $booking_details['user_city'] . ' ' . $booking_details['user_country'],
		'metadata[requests]' => $booking_details['user_message']
	);

	return array(
		'params' => http_build_query($stripe_params),
		'count' => count($stripe_params)
	);
}

function post_payment_charges($stripe_query)
{
	$stripe_api_header[] = 'Authorization: Bearer ' . get_option('nd_booking_stripe_secret_key');
	$stripe_api_header[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
	$stripe_api_url = 'https://api.stripe.com/v1/charges';

	//open connection
	$curl_handle = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($curl_handle, CURLOPT_URL, $stripe_api_url);
	curl_setopt($curl_handle, CURLOPT_POST, $stripe_query['count']);
	curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $stripe_query['params']);
	curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $stripe_api_header);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

	//execute post
	$stripe_payment_result = curl_exec($curl_handle);
	$response = array(
		'body' => json_decode($stripe_payment_result, true),
		'status' => curl_getinfo($curl_handle, CURLINFO_HTTP_CODE)
	);

	//close connection
	curl_close($curl_handle);

	return $response;
}

function make_stripe_payment($booking_details, $stripe_token)
{
	if (get_option('nd_booking_plugin_dev_mode') == 1) {
		// dev mode won't hit the stripe API
		return array('paypal_tx' => rand(100000000, 999999999));
	}

	$stripe_query = format_stripe_query($booking_details, $stripe_token);
	$booking_response = post_payment_charges($stripe_query);

	if ($booking_response['status'] == 200 && $booking_response['body']['paid'] == true) {
		$payment_response = array(
			'status' => 'success',
			'data' => array(
				'paypal_payment_status' => 'Completed',
				'paypal_tx' => $booking_response['body']['id'],
				'date' => date('H:m:s F j Y')
			)
		);
	} else {
		$payment_response = array(
			'status' => 'fail',
			'data' => $booking_response
		);
	}
	return $payment_response;
}

function handle_dates_not_available($room_id, $booking_details)
{
	// Log failed transaction
	$booking_details['paypal_payment_status'] = 'Scooped';
	nd_booking_add_scooped_booking($booking_details);

	// Send response
	$room_url = get_permalink($room_id);
	$ajax_response = array(
		'status' => 'fail',
		'data' => array(
			'dates' => 'We’re sorry, these dates are no longer available. Someone else must have booked it in the time you were filling out your information.',
			'room_url' => $room_url
		)
	);
	echo json_encode($ajax_response);
	wp_die();
}

function store_confirmation_cookie($confirmation_id)
{
	setcookie('confirmation_id', $confirmation_id, 0, COOKIEPATH, COOKIE_DOMAIN);
}

//php function for checkout
function nd_booking_checkout_php_function()
{
	$room_id = get_room_id($_POST['nd_booking_checkout_form_post_id']);
	$date_from = $_POST['nd_booking_checkout_form_date_from'];
	$date_to = $_POST['nd_booking_checkout_form_date_top'];
	$success_url = $_POST['confirmation_page'];

	$booking_details = get_booking_details($room_id);
	// Check if room is *still* available for all of the days
	$previous_bookings = nd_booking_check_for_overlapping_dates($room_id, $date_from, $date_to);
	if ($previous_bookings > 0) {
		handle_dates_not_available($room_id, $booking_details);
	}

	// Add pending booking to DB
	$booking_id = nd_booking_add_pending_booking($booking_details);

	if ($booking_id) {
		// Make payment with Stripe
		$stripe_token = $_POST['stripeToken'];
		$stripe_payment_details = make_stripe_payment($booking_details, $stripe_token);

		if ($stripe_payment_details['status'] === 'success') {
			// Update booking to have "Complete" for payment status
			nd_booking_complete_booking($booking_id, $stripe_payment_details['data']);
			store_confirmation_cookie($stripe_payment_details['data']['paypal_tx']);
			$ajax_response = array(
				'status' => 'success',
				'data' => array(
					'confirmation_page' => $success_url,
					'transaction_code' => $stripe_payment_details['data']['paypal_tx']
				)
			);
		} else {
			nd_booking_failed_booking($booking_id);
			$ajax_response = array(
				'status' => 'fail',
				'data' => array(
					'payment' => 'I’m sorry, we were unable to process your payment. You may have entered incorrect information or your card may have been declined for a different reason. Please check with your bank or try a different card.',
					'stripe_data' => $stripe_payment_details
				)
			);
		}
	} else {
		$ajax_response = array(
			'status' => 'error',
			'message' => 'An error occurred while processing your request'
		);
	}

	echo json_encode($ajax_response);
	wp_die();
}
add_action('wp_ajax_nd_booking_checkout_php_function', 'nd_booking_checkout_php_function');
add_action('wp_ajax_nopriv_nd_booking_checkout_php_function', 'nd_booking_checkout_php_function');
