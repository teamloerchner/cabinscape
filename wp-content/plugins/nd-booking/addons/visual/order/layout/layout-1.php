<?php


$nd_booking_current_page_permalink = get_permalink(get_the_ID());

if ( $nd_booking_current_page_permalink == nd_booking_search_page() ) {


    $str .= '

      <script type="text/javascript">
          //<![CDATA[

          jQuery(document).ready(function() {


            jQuery(function ($) {

              $( "#nd_booking_search_filter_options a" ).on("click",function() {

                $( "#nd_booking_search_filter_options a" ).removeClass( "nd_booking_search_filter_options_active" );
                $(this).addClass( "nd_booking_search_filter_options_active");

                nd_booking_sorting(1);

              });

              $( "#nd_booking_search_filter_layout a" ).on("click",function() {

                $( "#nd_booking_search_filter_layout a" ).removeClass( "nd_booking_search_filter_layout_active" );
                $(this).addClass( "nd_booking_search_filter_layout_active");

                nd_booking_sorting();

              });


              $("#nd_booking_search_filter_options li").on("click",function() {
                $("#nd_booking_search_filter_options li").removeClass( "nd_booking_search_filter_options_active_parent" );
                $(this).addClass( "nd_booking_search_filter_options_active_parent");
              });

            });


          });

          //]]>
        </script>

        <style>
        .nd_booking_search_filter_options_active { color:#fff !important; }
        #nd_booking_search_filter_options li.nd_booking_search_filter_options_active_parent p { color:#fff !important; border-bottom: 2px solid #878787;}

        #nd_booking_search_filter_options li:hover .nd_booking_search_filter_options_child { display: block; }

        .nd_booking_search_filter_layout_grid { background-image:url('.plugins_url().'/nd-booking/assets/img/icons/icon-grid-grey.svg); }
        .nd_booking_search_filter_layout_grid.nd_booking_search_filter_layout_active { background-image:url('.plugins_url().'/nd-booking/assets/img/icons/icon-grid-white.svg); }

        .nd_booking_search_filter_layout_list.nd_booking_search_filter_layout_active { background-image:url('.plugins_url().'/nd-booking/assets/img/icons/icon-list-white.svg); }
        .nd_booking_search_filter_layout_list { background-image:url('.plugins_url().'/nd-booking/assets/img/icons/icon-list-grey.svg); }

        </style>

		</div>
		';


}
