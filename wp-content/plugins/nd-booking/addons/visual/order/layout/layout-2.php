<?php


$nd_booking_current_page_permalink = get_permalink(get_the_ID());

if ( $nd_booking_current_page_permalink == nd_booking_search_page() ) {


    $str .= '

      <script type="text/javascript">
          //<![CDATA[

          jQuery(document).ready(function() {


            jQuery(function ($) {

              $( "#nd_booking_search_filter_options a" ).on("click",function() {

                $( "#nd_booking_search_filter_options a" ).removeClass( "nd_booking_search_filter_options_active" );
                $(this).addClass( "nd_booking_search_filter_options_active");

                nd_booking_sorting(1);

              });

              $( "#nd_booking_search_filter_layout a" ).on("click",function() {

                $( "#nd_booking_search_filter_layout a" ).removeClass( "nd_booking_search_filter_layout_active" );
                $(this).addClass( "nd_booking_search_filter_layout_active");

                nd_booking_sorting();

              });


              $("#nd_booking_search_filter_options li").on("click",function() {
                $("#nd_booking_search_filter_options li").removeClass( "nd_booking_search_filter_options_active_parent" );
                $(this).addClass( "nd_booking_search_filter_options_active_parent");
              });

            });


          });

          //]]>
        </script>

        <style>
        .nd_booking_search_filter_options_active { color:'.$nd_booking_text_color_active.' !important; }
        #nd_booking_search_filter_options li.nd_booking_search_filter_options_active_parent p { color:'.$nd_booking_text_color_active.' !important; border-bottom: 2px solid '.$nd_booking_text_color_active.' !important;}
        #nd_booking_search_filter_options li p { border-bottom: 2px solid '.$nd_booking_bg_color.' !important;}

        #nd_booking_search_filter_options li:hover .nd_booking_search_filter_options_child { display: block; }

        .nd_booking_search_filter_layout_grid { background-size: 18px 18px; opacity:0.5; background-image:url('.$nd_booking_grid_image_src[0].'); }
        .nd_booking_search_filter_layout_grid.nd_booking_search_filter_layout_active { opacity:1 !important; background-size: 18px 18px; background-image:url('.$nd_booking_grid_image_src[0].'); }

        .nd_booking_search_filter_layout_list.nd_booking_search_filter_layout_active { opacity:1 !important; background-size: 18px 18px; background-image:url('.$nd_booking_list_image_src[0].'); }
        .nd_booking_search_filter_layout_list { background-size: 18px 18px; opacity:0.5; background-image:url('.$nd_booking_list_image_src[0].'); }

        </style>



		</div>
		';


}
