<?php
/*
Plugin Name:       ND Booking
Description:       The plugin is used to manage your booking. To get started: 1) Click the "Activate" link to the left of this description. 2) Follow the documentation for installation for use the plugin in the better way.
Version:           2.3.6
Plugin URI:        http://www.nicdark.com/
Author:            Nicdark
Author URI:        http://www.nicdark.com/
License:           GPLv2 or later
*/

///////////////////////////////////////////////////TRANSLATIONS///////////////////////////////////////////////////////////////

//translation
function nd_booking_load_textdomain()
{
  load_plugin_textdomain("nd-booking", false, dirname(plugin_basename(__FILE__)) . '/languages');
}
add_action('plugins_loaded', 'nd_booking_load_textdomain');


///////////////////////////////////////////////////DB///////////////////////////////////////////////////////////////
register_activation_hook( __FILE__, 'nd_booking_create_booking_db' );
function nd_booking_create_booking_db()
{
    global $wpdb;

    $nd_booking_table_name = $wpdb->prefix . 'nd_booking_booking';

    $nd_booking_sql = "CREATE TABLE $nd_booking_table_name (
      id int(11) NOT NULL AUTO_INCREMENT,
      id_post int(11) NOT NULL,
      title_post varchar(255) NOT NULL,
      date varchar(255) NOT NULL,
      date_from varchar(255) NOT NULL,
      date_to varchar(255) NOT NULL,
      guests int(11) NOT NULL,
      final_trip_price int(11) NOT NULL,
      extra_services varchar(255) NOT NULL,
      id_user int(11) NOT NULL,
      user_first_name varchar(255) NOT NULL,
      user_last_name varchar(255) NOT NULL,
      paypal_email varchar(255) NOT NULL,
      user_phone varchar(255) NOT NULL,
      user_address varchar(255) NOT NULL,
      user_city varchar(255) NOT NULL,
      user_country varchar(255) NOT NULL,
      user_message varchar(255) NOT NULL,
      user_arrival varchar(255) NOT NULL,
      user_coupon varchar(255) NOT NULL,
      paypal_payment_status varchar(255) NOT NULL,
      paypal_currency varchar(255) NOT NULL,
      paypal_tx varchar(255) NOT NULL,
      action_type varchar(255) NOT NULL,
      UNIQUE KEY id (id)
    );";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $nd_booking_sql );
}



///////////////////////////////////////////////////CSS STYLE///////////////////////////////////////////////////////////////

//add custom css
function nd_booking_scripts() {

  //basic css plugin
  wp_enqueue_style( 'nd_booking_style', plugins_url() . '/nd-booking/assets/css/style.css' );

  wp_enqueue_script('jquery');

}
add_action( 'wp_enqueue_scripts', 'nd_booking_scripts' );


//START add admin custom css
function nd_booking_admin_style() {

  wp_enqueue_style( 'nd_booking_admin_style', plugins_url() . '/nd-booking/assets/css/admin-style.css', array(), false, false );

}
add_action( 'admin_enqueue_scripts', 'nd_booking_admin_style' );
//END add custom css


///////////////////////////////////////////////////GET TEMPLATE ///////////////////////////////////////////////////////////////

//single Cpt 1
function nd_booking_get_cpt_1_template($nd_booking_single_cpt_1_template) {
     global $post;

     if ($post->post_type == 'nd_booking_cpt_1') {
          $nd_booking_single_cpt_1_template = dirname( __FILE__ ) . '/templates/single-cpt-1.php';
     }
     return $nd_booking_single_cpt_1_template;
}
add_filter( 'single_template', 'nd_booking_get_cpt_1_template' );

//single Cpt 4
function nd_booking_get_cpt_4_template($nd_booking_single_cpt_4_template) {
     global $post;

     if ($post->post_type == 'nd_booking_cpt_4') {
          $nd_booking_single_cpt_4_template = dirname( __FILE__ ) . '/templates/single-cpt-4.php';
     }
     return $nd_booking_single_cpt_4_template;
}
add_filter( 'single_template', 'nd_booking_get_cpt_4_template' );

//update theme options
function nd_booking_theme_setup_update(){
    update_option( 'nicdark_theme_author', 0 );
}
add_action( 'after_switch_theme' , 'nd_booking_theme_setup_update');


///////////////////////////////////////////////////CPT///////////////////////////////////////////////////////////////
foreach ( glob ( plugin_dir_path( __FILE__ ) . "inc/cpt/*.php" ) as $file ){
  include_once $file;
}


///////////////////////////////////////////////////SHORTCODES ///////////////////////////////////////////////////////////////
foreach ( glob ( plugin_dir_path( __FILE__ ) . "inc/shortcodes/*.php" ) as $file ){
  include_once $file;
}


///////////////////////////////////////////////////ADDONS ///////////////////////////////////////////////////////////////
foreach ( glob ( plugin_dir_path( __FILE__ ) . "addons/*/index.php" ) as $file ){
  include_once $file;
}


///////////////////////////////////////////////////FUNCTIONS///////////////////////////////////////////////////////////////
require_once dirname( __FILE__ ) . '/inc/functions/functions.php';


///////////////////////////////////////////////////METABOX ///////////////////////////////////////////////////////////////
foreach ( glob ( plugin_dir_path( __FILE__ ) . "inc/metabox/*.php" ) as $file ){
  include_once $file;
}


///////////////////////////////////////////////////PLUGIN SETTINGS ///////////////////////////////////////////////////////////
require_once dirname( __FILE__ ) . '/inc/admin/plugin-settings.php';


//function for get plugin version
function nd_booking_get_plugin_version(){

    $nd_booking_plugin_data = get_plugin_data( __FILE__ );
    $nd_booking_plugin_version = $nd_booking_plugin_data['Version'];

    return $nd_booking_plugin_version;

}

function get_exception_ids($booking_exceptions){
  return array_map(function ($booking_exception) {
    $booking_page = get_page_by_path($booking_exception, OBJECT, 'nd_booking_cpt_3');
    return $booking_page->ID;
  }, $booking_exceptions);
}

function filter_exception_type($booking_exception_ids, $exception_type){
  return array_filter($booking_exception_ids, function ($booking_exception_id) use ($exception_type) {
    if(!$booking_exception_id) return false;
    $booking_exception_type = get_post_meta($booking_exception_id, 'nd_booking_meta_box_cpt_3_exceptions_type', true);
    return ($booking_exception_type === $exception_type);
  });
}

function get_exception_data($booking_exception_id){
  $exception_data = [
    'date_from' => get_post_meta( $booking_exception_id, 'nd_booking_meta_box_cpt_3_date_range_from', true ),
    'date_to' => get_post_meta( $booking_exception_id, 'nd_booking_meta_box_cpt_3_date_range_to', true )
  ];
  $minDateRange = (int)get_post_meta( $booking_exception_id, 'nd_booking_meta_box_cpt_3_min_booking_days', true );
  if ($minDateRange){
    $exception_data['minDateRange'] = $minDateRange;
  }
  return $exception_data;
}

function nd_booking_get_min_booking_duration($room_id){
  return get_post_meta( $room_id, 'nd_booking_meta_box_min_booking_day', true );
}

function nd_booking_get_exceptions($room_id, $exception_type, $meta_name){
  $booking_exception_list = get_post_meta( $room_id, $meta_name, true );
  $filtered_exceptions = [];
  if ($booking_exception_list) {
    $booking_exceptions = explode(',', $booking_exception_list);
    $booking_exception_ids = get_exception_ids($booking_exceptions);
    $filtered_exception_ids = filter_exception_type($booking_exception_ids, $exception_type);
    $filtered_date_ranges = array_map('get_exception_data', $filtered_exception_ids);
    $filtered_exceptions = array_values($filtered_date_ranges);
  }
  return $filtered_exceptions;
}

/*
* AJAX
*/
function nd_booking_live_availability_php_function() {
  $nd_booking_id = $_GET['nd_booking_id'];
  global $wpdb;

  $nd_booking_table_name = $wpdb->prefix . 'nd_booking_booking';
  $today = date('Y/m/d');

  $nd_booking_dates = $wpdb->get_results( "SELECT date_from,date_to,id FROM $nd_booking_table_name WHERE id_post = $nd_booking_id AND STR_TO_DATE(date_to, '%Y/%m/%d') > STR_TO_DATE('$today', '%Y/%m/%d') AND (paypal_payment_status = 'Completed' OR paypal_payment_status = 'Pending Payment')");
  $nd_blocked_dates = nd_booking_get_exceptions($nd_booking_id, 'nd_booking_block_dates', 'nd_booking_meta_box_exceptions_block');
  $availability = [
    "bookedDates" => array_merge($nd_booking_dates, $nd_blocked_dates),
    "minDateRange"=> (int)nd_booking_get_min_booking_duration($nd_booking_id),
    "dateRangeExceptions"=> nd_booking_get_exceptions($nd_booking_id, 'nd_booking_min_booking_days', 'nd_booking_meta_box_exceptions_min_booking_days')
  ];

  echo json_encode($availability);

  //close the function to avoid wordpress errors
  die();
}
add_action( 'wp_ajax_nd_booking_live_availability_php_function', 'nd_booking_live_availability_php_function' );
add_action( 'wp_ajax_nopriv_nd_booking_live_availability_php_function', 'nd_booking_live_availability_php_function' );

