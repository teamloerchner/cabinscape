<?php
/**
 * Plugin Name: Cabinscape Landing Pages
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once( plugin_dir_path( __FILE__ ) . 'CSLandingPages.php' );
add_action( 'plugins_loaded', array( 'CSLandingPages', 'get_instance' ) );
