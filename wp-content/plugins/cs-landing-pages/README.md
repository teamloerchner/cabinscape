# Cabinscape Customer Booking Landing Page

This plugin adds a page template that customers will see when they click on a CTA in their booking reminder email. The page is intended to provide information about the cabin they have booked and remind them of their booking dates.

It also adds a shortcode for displaying individual booking properties based on the booking id passed to the request so that the page is personalised to the user.

## Shortcode

The shortcode this plugin adds can be used to display booking properties like email, first name,  or last name. Usage: `[booking prop="prop-from-db"]`. The `prop` parameter accepts the following values (default: `id`)

```
- 'id' // booking ID
- 'id_post' // cabin ID
- 'title_post' // cabin title
- 'date' // date of booking
- 'date_from' // checkin
- 'date_to' // checkout
- 'user_first_name'
- 'user_last_name'
```

### Extra Services

Extra services have their own shortcode that wraps conditional content. Usage:
```
[if_extra_service id="service-id"]
    You booked an extra service!
[else]
    You didn't book an extra service :(
[/if_extra_service]
```
