# AirBnB Calendar Sync WP Plugin

## Usage
- Create a page with title `cabin_name.ics`
- Add `cabin_id` custom field to the page
- Set value of `cabin_id` to the post ID of the cabin post (see by hovering over the cabin in the dashboard. Get the ID from the URL)

This plugin then exposes an ics endpoint for consumption by airbnb's calendar sync feature.
