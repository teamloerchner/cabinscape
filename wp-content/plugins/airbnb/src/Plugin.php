<?php
namespace Greenhill\Airbnb;

use Greenhill\Airbnb\GetCabin;

class Plugin
{
    private function __construct()
    {
        add_filter( 'template_include', [$this, 'getTemplate'], 99 );
    }

    function getTemplate( $template ) {
        $file_name = 'CustomTemplate.php';
        $allCabins = GetCabin::titles();
        if ( is_page( array_map(function($cabin){
            $normalised = str_replace(' ', '', strtolower($cabin));
            return $normalised . '.ics';
        }, $allCabins) ) ) {
            if ( locate_template( $file_name ) ) {
                $template = locate_template( $file_name );
            } else {
                $template = dirname( __FILE__ ) . '/templates/' . $file_name;
            }
        }

        return $template;
    }

    public static function register()
    {
        return new self();
    }
}
