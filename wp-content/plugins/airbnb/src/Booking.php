<?php
namespace Greenhill\Airbnb;

class Booking
{
    public static function icsFormat($booking)
    {
        $dateTo = (new \DateTimeImmutable($booking->date_to))->format('Ymd');
        $dateFrom = (new \DateTimeImmutable($booking->date_from))->format('Ymd');
        return sprintf(
            "BEGIN:VEVENT\nDTEND:%s\nDTSTART:%s\nSUMMARY:%s\nDESCRIPTION: Name: %s\nUID:%s\nEND:VEVENT\n",
            $dateTo,
            $dateFrom,
            $booking->name,
            $booking->name,
            sha1($booking->id . $dateFrom . $dateTo)
        );
    }
}
