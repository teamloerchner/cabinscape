<?php
namespace Greenhill\Airbnb;

class DisplayBookingsInIcsFormat {
    public function perform($cabinId)
    {
        global $wpdb;
        $table = $wpdb->prefix . 'nd_booking_booking';

        $bookings = $wpdb->get_results("
            SELECT
                date_from,
                date_to,
                id,
                title_post as cabin,
                paypal_email as email,
                concat(user_address, ', ', user_city, ', ', user_country) as address,
                user_phone as phone,
                concat(user_first_name, ' ', user_last_name) as name
            FROM $table
            WHERE id_post = $cabinId"
        );

        $results = "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:cabinscape\n";
        foreach($bookings as $booking) {
            $results .= Booking::icsFormat($booking);
        }
        $results .= "END:VCALENDAR\n";
        return $results;
    }
}
