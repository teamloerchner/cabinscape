<?php
namespace Greenhill\Airbnb;

class GetCabin {
    public static function ID() {
        $cabinId = (int) get_post_meta( get_the_ID(), 'cabin_id', true  );

        $cabins = self::all();

        if (!in_array($cabinId, $cabins)) {
            throw new \Exception(sprintf("cabin not found. please add cabin_id custom field with one of the possible cabin ids (%s)", implode(", ", $cabins)));
        }

        return $cabinId;
    }

    public static function all() {
        $the_query = new \WP_Query( ['post_type'=> 'nd_booking_cpt_1', 'post_status' => ['publish']] );
        $cabins = [];
        foreach ($the_query->posts as $post) {
            $cabins[] = (int) $post->ID;
        }
        return $cabins;
    }

    public static function titles() {
        $the_query = new \WP_Query( ['post_type'=> 'nd_booking_cpt_1', 'post_status' => ['publish']] );
        $cabins = [];
        foreach ($the_query->posts as $post) {
            $cabins[] = $post->post_title;
        }
        return $cabins;
    }
}
