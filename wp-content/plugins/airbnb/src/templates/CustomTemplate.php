<?php

use Greenhill\Airbnb\DisplayBookingsInIcsFormat;
use Greenhill\Airbnb\GetCabin;

header('Content-Type: text/plain; charset=utf-8');
echo (new DisplayBookingsInIcsFormat)->perform(GetCabin::ID());
