<?php
/**
 * Plugin Name:       Cabinscape Cleaner Schedule
 * Plugin URI:        https://briancurtis.de
 * Description:       Adds Cleaner User and Role and Schedule Admin Page.
 * Version:           0.0.1
 * Author:            Brian Greenhill
 * Author URI:        https://briancurtis.de
 */
const CS_CLEANERS_PLUGIN_NAME = 'cs_cleaners_plugin';

const ADMIN_PAGE_NAME = 'Cleaning Schedule';
const CLEAN_CABINS_CAPABILITY    = 'clean_cabins';
const MENU_SLUG = 'cleaning-staff';

add_action( 'init', CS_CLEANERS_PLUGIN_NAME . '_activate' );
register_deactivation_hook( __FILE__, CS_CLEANERS_PLUGIN_NAME . '_deactivate' );
add_action('admin_menu', CS_CLEANERS_PLUGIN_NAME . '_add_menu_page');
add_action('admin_bar_menu', CS_CLEANERS_PLUGIN_NAME . '_add_toolbar', 100);

function cs_cleaners_plugin_add_toolbar($admin_bar){
	$admin_bar->add_menu( array(
		'id'    => 'cleaning-schedule',
		'title' => 'Cleaning Schedule',
		'href'  => admin_url() . 'admin.php?page=' . MENU_SLUG,
		'meta'  => array(
			'title' => __('Cleaning Schedule'),
		),
	));
}

function cs_cleaners_plugin_activate()
{
    add_role('cabinscape_cleaner', 'Cleaning Staff', [ CLEAN_CABINS_CAPABILITY => true ]);
    cs_cleaners_plugin_register_custom_field();
}

function cs_cleaners_plugin_register_custom_field()
{
	if(function_exists("register_field_group"))
	{
		register_field_group(array (
			'id' => 'acf_responsible-cleaner',
			'title' => 'Responsible Cleaner',
			'fields' => array (
				array (
					'key' => 'field_5c139713a7efd',
					'label' => 'Cleaner',
					'name' => 'cleaner_user',
					'type' => 'user',
					'instructions' => 'The cleaner	responsible for this cabin',
					'required' => 1,
					'role' => array (
						0 => 'cabinscape_cleaner',
					),
					'field_type' => 'select',
					'allow_null' => 0,
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'nd_booking_cpt_1',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'side',
				'layout' => 'default',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}
}

function cs_cleaners_plugin_add_menu_page()
{
    add_menu_page( ADMIN_PAGE_NAME, ADMIN_PAGE_NAME, CLEAN_CABINS_CAPABILITY, MENU_SLUG, CS_CLEANERS_PLUGIN_NAME . '_page');
}

function cs_cleaners_plugin_page()
{
    global $wpdb;
    $results = $wpdb->get_results('
select 
	b.user_first_name,
	b.user_last_name,
	b.paypal_email,
	b.date_from,
	b.date_to,
	b.extra_services,
	p.post_title as cabin
from ' . $wpdb->prefix . 'nd_booking_booking b
join wp_posts p on b.id_post = p.ID
join wp_postmeta m on m.post_id = b.id_post
join wp_users u on u.ID = m.meta_value
where date_from BETWEEN NOW() - INTERVAL 1 DAY AND NOW() + INTERVAL 4 WEEK
and m.meta_key = \'cleaner_user\'
and m.meta_value = '.get_current_user_id().'
ORDER BY date_from ASC;
');

    include __DIR__ . '/templates/bookings.phtml';
}

function cs_cleaners_plugin_deactivate()
{
    remove_role('cabinscape_cleaner');
    remove_menu_page( MENU_SLUG );
}

function cs_cleaners_plugin_parse_services(\stdClass $result)
{
	global $wpdb;

	$services = [];
	if ($result->extra_services !== "") {
		$serviceIDList = rtrim($result->extra_services, ',');
		$parts = explode(',', $serviceIDList);
		$formattedIDs = array_map(function ($id) {
			return strpos($id, '[') ?
				substr($id, 0, strpos($id, '[')) :
				$id;
		}, $parts);

		$results = $wpdb
			->get_results('select post_title from ' . $wpdb->prefix . 'posts p where ID IN ('.implode(',', $formattedIDs).');');
		foreach($results as $res) {
			$services[] = $res->post_title;
		}
	}
	return implode(", ", $services);
}