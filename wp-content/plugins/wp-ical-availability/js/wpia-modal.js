(function ( $ ) {

    // Draggable & Sortable
    jQuery(document).on('wpia-modal-open', function () 
    {
    	jQuery('div[wpia-scroll]').perfectScrollbar();

        jQuery('.wpia-tabs-nav > li').on('click', function () {
        	var pageID 		= jQuery( this ).data('tab'),
        		$page 		= jQuery( '#' + pageID );

        	jQuery('.wpia-tabs-page, .wpia-tabs-nav > li').removeClass('active');

        	$page.addClass('active');
        	
        	// Set active tab item
        	jQuery( this ).addClass( 'active' );


        });
    });

    jQuery(document).on('wpia-modal-open', function () 
    {
    	jQuery('div[wpia-scroll]').perfectScrollbar();

        jQuery('.wpia-tabs-nav > li').on('click', function () {
        	var pageID 		= jQuery( this ).data('tab'),
        		$page 		= jQuery( '#' + pageID );

        	jQuery('.wpia-tabs-page, .wpia-tabs-nav > li').removeClass('active');

        	$page.addClass('active');
        	
        	// Set active tab item
        	jQuery( this ).addClass( 'active' );


        });
    });

	// jQuery(document).ready(function () {
	jQuery(window).on('load', function () {

		jQuery('.button.wpia_media_link').on('click', function (e) 
	    {
	        e.preventDefault();

	        wpiaOpenModal();
	    });

	    
	    function wpiaOpenModal()
	    {
	        jQuery('body').append('<div id="wpia-modal-overlay"></div><div id="wpia-modal"><div class="wpia-modal-header"><h3><span class="wpia_media_icon" style="margin-top: 4px;"></span> Add Calendar</h3><span class="wpia-close-modal"><img src="' + wpiaCloseIconUrl + '../images/window-close.png"><span></div><div class="wpia-modal-body"></div></div>');

	        // Insert content into the modal
	        jQuery.ajax({
	            url: wpiaModalAjaxUrl,
	            dataType: 'html',
	            success: function (html) 
	            {
	                jQuery('body #wpia-modal .wpia-modal-body').html( html );
			        // Trigger modal-open event
			        jQuery(document).trigger('wpia-modal-open');
	            }
	        });


	        jQuery('#wpia-modal .wpia-modal-header span.wpia-close-modal, #wpia-modal-overlay, #wpia-modal .wpia-close-button').on('click', function (e) 
	        {
	            e.preventDefault();

	            wpiaRemoveModal();
	        });

	    }

	    function wpiaRemoveModal()
	    {
	        jQuery('#wpia-modal-overlay, #wpia-modal').remove();
	        jQuery(document).trigger('wpia-modal-closed');
	    }
	});
})(jQuery, document, window);