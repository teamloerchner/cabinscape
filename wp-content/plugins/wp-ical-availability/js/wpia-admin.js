var wpia = jQuery.noConflict();
var $instance;

var $saveCalendarDisabled   = false;

function htmlEscape(str) {
    return String(str)
            .replace(/&/g, '--AMP--')
            .replace(/"/g, '--DOUBLEQUOTE--')
            .replace(/'/g, '--QUOTE--')
            .replace(/</g, '--LT--')
            .replace(/>/g, '--GT--');
}
function showLoader(){
    wpia('.wpia-loading').fadeTo(0,0).css('display','block').fadeTo(200,1);
    wpia('.wpia-calendar ul').animate({
        'opacity' : '0.7'
    },200);
}
function hideLoader(){
    wpia('.wpia-loading').css('display','none');
}
// function changeDay(direction, timestamp){

//     var data = {
// 		action: 'wpia_changeDayAdmin',
//         calendarDirection: direction,
// 		totalCalendars: $instance.find(".wpia-total-calendars").attr('data-total-calendars'),
//         currentTimestamp: timestamp,
//         // calendarData: $instance.find(".wpia-calendar-data").attr('data-calendar-data'),
//         calendarHistory: $instance.find(".wpia-calendar-history").attr('data-calendar-history'),
//         // calendarLegend: $instance.find(".wpia-calendar-legend").attr('data-calendar-legend'),
//         showLegend: $instance.find(".wpia-show-legend").attr('data-show-legend'),
//         showDropdown: $instance.find(".wpia-show-dropdown").attr('data-show-dropdown'),
//         showWeekNumbers: $instance.find(".wpia-calendar-weeknumbers").attr('data-calendar-weeknumbers'),
//         calendarLanguage: $instance.find(".wpia-calendar-language").attr('data-calendar-language'),
//         weekStart : $instance.find(".wpia-calendar-week-start").attr('data-calendar-week-start'),
//         calendarID : $instance.find(".wpia-calendar-ID").attr('data-calendar-ID')
        
// 	};
// 	wpia.post(ajaxurl, data, function(response) {
// 		$instance.find('.wpia-calendars').html(response);
//         hideLoader();  
//         wpia('.wpia-dropdown').customSelect();    
//         wpia('.wpia-chosen').chosen(); 
// 	});
// }
function wpia_changeDay(direction, timestamp){

    var data = {
        action: 'wpia_changeDayAdmin',
        calendarDirection: direction,
        totalCalendars: $instance.find(".wpia-total-calendars").attr('data-total-calendars'),
        currentTimestamp: timestamp,
        calendarData: $instance.find(".wpia-calendar-data").attr('data-calendar-data'),
        calendarHistory: $instance.find(".wpia-calendar-history").attr('data-calendar-history'),
        calendarLegend: $instance.find(".wpia-calendar-legend").attr('data-calendar-legend'),
        showLegend: $instance.find(".wpia-show-legend").attr('data-show-legend'),
        showDropdown: $instance.find(".wpia-show-dropdown").attr('data-show-dropdown'),
        showWeekNumbers: $instance.find(".wpia-calendar-weeknumbers").attr('data-calendar-weeknumbers'),
        calendarLanguage: $instance.find(".wpia-calendar-language").attr('data-calendar-language'),
        weekStart : $instance.find(".wpia-calendar-week-start").attr('data-calendar-week-start'),
        calendarID : $instance.find(".wpia-calendar-ID").attr('data-calendar-ID')
        
    };

    wpia.ajax({
        type: 'POST',
        url: wpia_ajaxurl,
        contentType: 'application/json; charset=utf-8',
        dataType: 'html',
        data: JSON.stringify(data),
        success: function ( response )
        {
            $instance.find('.wpia-calendars').html(response);
            hideLoader();
            wpia('.wpia-dropdown').customSelect();    
            wpia('.wpia-chosen').chosen(); 
        }
    });
}
var getInputs           = function (selector)
{
    var translations  = [];
    wpia(selector + ' input, ' + selector + ' select, ' + selector + ' textarea').each(function (i, e) 
    {
        field   = wpia( this );
        translations.push({input: field.attr('name'), value: field.val()})
    });
    return translations;
}
function wpia_saveCalendar(refresh)
{
    if ( $saveCalendarDisabled == true )
        return false;
    
    var data = {
        action:                 'wpia_saveCalendar',
        title:                  wpia('[name="calendarTitle"]').val(),
        icalendar_feed:         wpia('[name="icalendar_feed[]"]').map(function(){return wpia(this).val();}).get(),
        available_color:        wpia('#available_color').val(),
        booked_color:           wpia('#booked_color').val(),
        available_translations: getInputs('.available-translations'),
        booked_translations:    getInputs('.booked-translations'),
        id:                     wpia('[name="calendarID"]').val()
    }

    
    wpia.ajax({
        type: 'POST',
        url: wpia_ajaxurl,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(data),
        success: function ( response )
        {
            // disable "Save Changes" button till the form is created
            $saveCalendarDisabled = true;
            wpia('.wpia_saveCalendar').attr('disabled', true);

            wpia('body').scrollTop(0);

            wpia('#wpia-notification-wrapper').append('<div id="wpia-notice" class="notice notice-' + response.class + ' is-dismissible"><p>' + response.msg + '</p></div>');



            var removeTimeout = setTimeout(function () {
                wpia('#wpia-notice').remove();
            }, 3000);

            if ( response.result == false )
            {
                // alert('Something is wrong, NO results from request!');
                $saveCalendarDisabled = false;
                wpia('.wpia_saveCalendar').attr('disabled', false);
                return false;
            }

            if ( typeof response.url !== 'undefined' )
            {
                // redirect to the newly created form
                var timeout = setTimeout(function () {
                    window.location.href = ''+response.url+'';
                }, 3350);
            }
            else
            {
                var timeout = setTimeout(function () {
                    window.location.reload();
                }, 1500);
                
            }
        }
    });
}

wpia(document).ready(function(){
    
    wpia('.wpia-chosen').chosen();
    
    wpia('.wpia_saveCalendar').each(function () {
        wpia(this).on('click', function () {
            wpia_saveCalendar();
        });
    });

    wpia('.wpia-dropdown').customSelect();
    wpia('.wpia-container').each(function(){
    $instance = wpia(this);
    wpia($instance).on('change','.wpia-dropdown',function(e){
        showLoader();     
        e.preventDefault();        
        wpia_changeDay('jump',wpia(this).val())
    });
    
    wpia($instance).on('click','.wpia-prev',function(e){
        showLoader();
        e.preventDefault();
        if($instance.find(".wpia-current-timestamp a").length == 0)
            timestamp = $instance.find(".wpia-current-timestamp").attr('data-current-timestamp');
        else 
            timestamp = $instance.find(".wpia-current-timestamp a").attr('data-current-timestamp') 
        wpia_changeDay('prev',timestamp);
    });
    
    
    wpia($instance).on('click','.wpia-next',function(e){  
        showLoader();
        e.preventDefault();        
        if($instance.find(".wpia-current-timestamp a").length == 0)
            timestamp = $instance.find(".wpia-current-timestamp").attr('data-current-timestamp')
        else 
            timestamp = $instance.find(".wpia-current-timestamp a").attr('data-current-timestamp') 
        wpia_changeDay('next',timestamp);
    });
    
    })

    
    
    
   
    
    wpia(".saveCalendar").click(function(){
        if (!wpia.trim(wpia(".fullTitle").val()).length) {
            wpia(".fullTitle").addClass('error').focus();
            return false;
        }
        return true;
        
    })
    
    wpia(".open-wpia-legend-translations").click(function(e){
        e.preventDefault();
        if(wpia(this).find('span').text() == '+'){
            wpia(this).siblings('.wpia-legend-translations').slideDown();
            wpia(this).find('span').text('-');
        } else {
            wpia(this).siblings('.wpia-legend-translations').slideUp();
            wpia(this).find('span').text('+');
        }    
        
    })
    
    wpia("#wpia-add-feed").click(function(e){
        e.preventDefault();
        i= wpia('.feeds-table tr').length;
        wpia('<tr><th scope="row"><label for="icalendar_feed_'+i+'">iCalendar Feed #'+i+'</label></th><td><input name="icalendar_feed[]" type="text" id="icalendar_feed_'+i+'" value="" class="widefat" /></td></tr>').insertBefore('.add-more-feeds');
        
    })
    
    
})
function wpia_select_text(containerid) {
    if (document.selection) {
        var range = document.body.createTextRange();
        range.moveToElementText(containerid);
        range.select();
    } else if (window.getSelection) {
        var range = document.createRange();
        range.selectNode(containerid);
        window.getSelection().addRange(range);
    }
}