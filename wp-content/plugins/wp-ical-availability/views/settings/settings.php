<?php global $wpdb;?>
<div class="wrap wpia-wrap">
    <div id="icon-themes" class="icon32"></div>
    <h2><?php echo __('Settings','wpia');?></h2>
    <?php if(!empty($_GET['save']) && $_GET['save'] == 'ok'):?>
    <div id="message" class="updated">
        <p><?php echo __('The settings were saved.','wpia')?></p>
    </div>
    <?php endif;?>
    
        <div class="postbox-container">
            
            <form action="<?php echo admin_url( 'admin.php?page=wp-ical-availability-settings&do=save&noheader=true');?>" method="post">
            <div class="wpia-action-buttons"><input type="submit" class="button button-primary button-h2" value="<?php echo __('Save Changes','wpia');?>" /> </div>
            
            <div class="metabox-holder">
                <div class="postbox">
                    <div class="handlediv" title="Click to toggle"><br /></div>
                    <h3 class="hndle"><?php echo __('General Settings','wpia');?></h3>
                    <div class="inside">     
                        <?php $wpiaOptions = json_decode(get_site_option('wpia-options'),true);?>  
                        
                        <div class="wpia-settings-col">
                            <div class="wpia-settings-col-left">
                                <strong><?php echo __("Use Split Days",'wpia') ;?></strong>                                
                            </div>
                            <div class="wpia-settings-col-right">
                                <select name="date-type">
                                    <option <?php if(!empty($wpiaOptions['displayDays']) && $wpiaOptions['displayDays'] == 2):?>selected="selected"<?php endif;?> value="2"><?php _e('Yes','wpia');?></option>
                                    <option <?php if(!empty($wpiaOptions['displayDays']) && $wpiaOptions['displayDays'] == 1):?>selected="selected"<?php endif;?> value="1"><?php _e('No','wpia');?></option>                                    
                                    
                                </select>
                            </div> 
                            <div class="wpia-clear"></div>                            
                        </div>   
                        
                        <div class="wpia-settings-col">
                            <div class="wpia-settings-col-left">
                                <strong><?php echo __("Date Format",'wpia') ;?></strong>                                
                            </div>
                            <div class="wpia-settings-col-right wpia-date-format">
                                <label><input class="small" type="radio" id="" name="dateFormat" <?php if(!empty($wpiaOptions['dateFormat']) && $wpiaOptions['dateFormat'] == 'j F Y'): ?>checked="checked"<?php endif;?> value="j F Y" /> 25 <?php _e('July');?> 2013</label>
                                <label><input class="small" type="radio" id="" name="dateFormat"<?php if(!empty($wpiaOptions['dateFormat']) && $wpiaOptions['dateFormat'] == 'F j, Y'): ?>checked="checked"<?php endif;?> value="F j, Y" /> <?php _e('July');?> 25, 2013</label>
                                <label><input class="small" type="radio" id="" name="dateFormat"<?php if(!empty($wpiaOptions['dateFormat']) && $wpiaOptions['dateFormat'] == 'Y/m/d'): ?>checked="checked"<?php endif;?> value="Y/m/d" /> 2013/07/25</label>
                                <label><input class="small" type="radio" id="" name="dateFormat"<?php if(!empty($wpiaOptions['dateFormat']) && $wpiaOptions['dateFormat'] == 'm/d/Y'): ?>checked="checked"<?php endif;?> value="m/d/Y" /> 07/25/2013</label>
                                <label><input class="small" type="radio" id="" name="dateFormat"<?php if(!empty($wpiaOptions['dateFormat']) && $wpiaOptions['dateFormat'] == 'd/m/Y'): ?>checked="checked"<?php endif;?> value="d/m/Y" /> 25/07/2013</label>
                            </div>
                            <div class="wpia-clear"></div>                            
                        </div>   

                        <div class="wpia-settings-col">
                            <div class="wpia-settings-col-left">
                                <strong><?php echo __('Backend Start Day','wpia');?></strong>                                
                            </div>
                            <?php if(empty($wpiaOptions['backendStartDay'])) $wpiaOptions['backendStartDay'] = 1;?>
                            <div class="wpia-settings-col-right">
                                <select name="backend-start-day">
                                    <option <?php if($wpiaOptions['backendStartDay'] == 1):?>selected="selected"<?php endif;?> value="1"><?php _e('Monday');?></option>
                                    <option <?php if($wpiaOptions['backendStartDay'] == 2):?>selected="selected"<?php endif;?> value="2"><?php _e('Tuesday');?></option>
                                    <option <?php if($wpiaOptions['backendStartDay'] == 3):?>selected="selected"<?php endif;?> value="3"><?php _e('Wednesday');?></option>
                                    <option <?php if($wpiaOptions['backendStartDay'] == 4):?>selected="selected"<?php endif;?> value="4"><?php _e('Thursday');?></option>
                                    <option <?php if($wpiaOptions['backendStartDay'] == 5):?>selected="selected"<?php endif;?> value="5"><?php _e('Friday');?></option>
                                    <option <?php if($wpiaOptions['backendStartDay'] == 6):?>selected="selected"<?php endif;?> value="6"><?php _e('Saturday');?></option>
                                    <option <?php if($wpiaOptions['backendStartDay'] == 7):?>selected="selected"<?php endif;?> value="7"><?php _e('Sunday');?></option>
                                </select>
                            </div> 
                            <div class="wpia-clear"></div>                            
                        </div> 
                        <div class="wpia-settings-col">
                            <div class="wpia-settings-col-left">
                                <strong><?php echo __('Calendars to show in backend','wpia');?></strong>                                
                            </div>
                            <?php if(empty($wpiaOptions['backendCalendars'])) $wpiaOptions['backendCalendars'] = 3;?>
                            <div class="wpia-settings-col-right">
                                <select name="backend-calendars">
                                    <?php for($i=1; $i<=6;$i++):?>
                                    <option <?php if($wpiaOptions['backendCalendars'] == $i):?>selected="selected"<?php endif;?> value="<?php echo $i;?>"><?php echo $i;?></option>
                                    <?php endfor;?>
                                </select>
                            </div> 
                            <div class="wpia-clear"></div>                            
                        </div> 
                        <div class="wpia-settings-col">
                            <div class="wpia-settings-col-left">
                                <strong><?php echo __('Always enqueue scripts and styles','wpia');?></strong>                                
                            </div>
                            <div class="wpia-settings-col-right">
                                <select name="always-enqueue-scripts">
                                    <option <?php if(!empty($wpiaOptions['alwaysEnqueueScripts']) && $wpiaOptions['alwaysEnqueueScripts'] == 'no'):?>selected="selected"<?php endif;?> value="no"><?php _e('No');?></option>
                                    <option <?php if(!empty($wpiaOptions['alwaysEnqueueScripts']) && $wpiaOptions['alwaysEnqueueScripts'] == 'yes'):?>selected="selected"<?php endif;?> value="yes"><?php _e('Yes');?></option>
                                    
                                </select>
                                <br /><small><?php echo __("If you are loading the calendar with ajax then you should set this option to 'Yes'.",'wpia');?></small>
                            </div>
                            <div class="wpia-clear"></div>                            
                        </div> 
                        
                        <div class="wpia-settings-col">
                            <div class="wpia-settings-col-left">
                                <strong><?php echo __('Enable Caching','wpia');?></strong>                                
                            </div>
                            <div class="wpia-settings-col-right">
                                <select name="enable-caching">
                                    <option <?php if(!empty($wpiaOptions['enableCaching']) && $wpiaOptions['enableCaching'] == 'yes'):?>selected="selected"<?php endif;?> value="yes"><?php _e('Yes');?></option>
                                    <option <?php if(!empty($wpiaOptions['enableCaching']) && $wpiaOptions['enableCaching'] == 'no'):?>selected="selected"<?php endif;?> value="no"><?php _e('No');?></option>
                                    
                                    
                                </select>
                            </div>
                            <div class="wpia-clear"></div>                            
                        </div> 
                        
                        <div class="wpia-settings-col">
                            <div class="wpia-settings-col-left">
                                <strong><?php echo __('Cache Interval','wpia');?></strong>                                
                            </div>
                            <div class="wpia-settings-col-right">
                                <select name="cache-interval">
                                    <!-- <option <?php if(!empty($wpiaOptions['cacheInterval']) && $wpiaOptions['cacheInterval'] == 60):?>selected="selected"<?php endif;?> value="<?php echo 60;?>"><?php _e('1 minute');?></option> -->
                                    <option <?php if(!empty($wpiaOptions['cacheInterval']) && $wpiaOptions['cacheInterval'] == (60*5)):?>selected="selected"<?php endif;?> value="<?php echo (60*5);?>"><?php _e('5 minutes');?></option>
                                    <option <?php if(!empty($wpiaOptions['cacheInterval']) && $wpiaOptions['cacheInterval'] == (60*15)):?>selected="selected"<?php endif;?> value="<?php echo (60*15);?>"><?php _e('15 minutes');?></option>
                                    <option <?php if(!empty($wpiaOptions['cacheInterval']) && $wpiaOptions['cacheInterval'] == (60*30)):?>selected="selected"<?php endif;?> value="<?php echo (60*30);?>"><?php _e('30 minutes');?></option>
                                    <option <?php if(!empty($wpiaOptions['cacheInterval']) && $wpiaOptions['cacheInterval'] == (60*45)):?>selected="selected"<?php endif;?> value="<?php echo (60*45);?>"><?php _e('45 minutes');?></option>
                                    <option <?php if(!empty($wpiaOptions['cacheInterval']) && $wpiaOptions['cacheInterval'] == (60*60)):?>selected="selected"<?php endif;?> value="<?php echo (60*60);?>"><?php _e('1 hour');?></option>
                                    <option <?php if(!empty($wpiaOptions['cacheInterval']) && $wpiaOptions['cacheInterval'] == (60*120)):?>selected="selected"<?php endif;?> value="<?php echo (60*120);?>"><?php _e('2 hours');?></option>
                                    
                                    
                                    
                                </select>
                            </div>
                            <div class="wpia-clear"></div>                            
                        </div> 
                    </div>
                </div>
            </div> 
           
            
            <div class="metabox-holder">
                <div class="postbox">
                    
                    <h3 class="hndle"><?php echo __('Languages','wpia');?></h3>
                    <div class="inside">
                        <?php $activeLanguages = json_decode(get_site_option('wpia-languages'),true);?>
                        <?php $languages = array('en' => 'English', 'bg' => 'Bulgarian','ca' => 'Catalan','hr' => 'Croatian','cz' => 'Czech','da' => 'Danish','nl' => 'Dutch','et' => 'Estonian','fi' => 'Finnish','fr' => 'French','de' => 'German','el' => 'Greek','hu' => 'Hungarian','it' => 'Italian', 'jp' => 'Japanese', 'no' => 'Norwegian','pl' => 'Polish','pt' => 'Portugese','ro' => 'Romanian','ru' => 'Russian','sk' => 'Slovak','sl' => 'Slovenian','es' => 'Spanish','sv' => 'Swedish','tr' => 'Turkish','ua' => 'Ukrainian');?>    
                        <div class="wpia-settings-col">
                            <div class="wpia-settings-col-left">
                                <strong><?php echo __('Languages','wpia');?></strong><br />
                                <small><?php echo __('What languages do you <br />want to use?','wpia');?></small>
                            </div>
                            <div class="wpia-settings-col-right">
                                <?php foreach($languages as $code => $language):?>
                                    <label><input type="checkbox" name="<?php echo $code;?>" <?php if(in_array($language,$activeLanguages)):?>checked="checked"<?php endif;?> value="<?php echo $code;?>" /> <?php echo $language;?></label>
                                <?php endforeach;?>
                            </div>
                            <div class="wpia-clear"></div>
                            
                        </div> 
                        
                    </div>
                </div>
            </div> 
            <br /><input type="submit" class="button button-primary" value="<?php echo __('Save Changes','wpia');?>" /> 
            </form>
        </div>

</div>

