<script type="text/javascript">
    jQuery( "#wpia-sortable, #wpia-sortable2" ).sortable({
        connectWith: ".wpia-connected",
        placeholder: "ui-state-highlight",
        zIndex: 85462257
    }).disableSelection();
    
    jQuery('#wpia-modal').on('change', '#wpia_calendar_overview_display_calendars', function (e) 
    {
        jQuery('#wpia-modal #wpia-dd-calendars').toggleClass('wpia-hidden');
    });

    jQuery('#wpia-modal').on('change', '#wpia_calendar_theme', function (e) 
    {
        var value = jQuery('#wpia_calendar_theme').val();
        if ( value == 'classic' )
            jQuery('#wpia-modal #wpia-modern-hbc').addClass('wpia-hidden');
        else
            jQuery('#wpia-modal #wpia-modern-hbc').removeClass('wpia-hidden');

    });
    
</script>
<?php global $wpdb; ?>
<div class="wpia-content">
    
    <!-- start: Tabs -->
    <ul class="wpia-tabs-nav">
        <li class="active" data-tab="insertCalendar"><i class="mdi mdi-calendar-plus"></i> Insert Calendar</li>
        <li data-tab="insertOverview"><i class="mdi mdi-calendar-text"></i>Insert Multiple Overview Calendar</li>
        <!-- <li data-tab="editShortcode"><i class="mdi mdi-pencil"></i>Edit Shortcode</li> -->
    </ul>
    <div class="wpia-tabs-content" wpia-scroll>
        <div class="wpia-tabs-page active" id="insertCalendar">
            <div class="wpia-modal-container">
                <!-- start: Insert Calendar -->
                <h3><?php _e("Insert A Calendar", "wpia"); ?></h3>
                <p>
                    <?php _e("Select a calendar below to add it to your post or page.", "wpia"); ?>
                </p>

                <div class="wpia-row">
                    
                    <div class="wpia-col">
                        <strong><?php _e("Calendar", "wpia"); ?></strong><br />
                        <select id="wpia_calendar_id">                
                            <?php $sql = 'SELECT * FROM ' . $wpdb->base_prefix . 'wpia_calendars';?>
                            <?php $rows = $wpdb->get_results( $sql, ARRAY_A );?>                         
                            <?php foreach($rows as $calendar):?>
                                <option value="<?php echo absint($calendar['calendarID']) ?>"><?php echo esc_html($calendar['calendarTitle']) ?></option>
                            <?php endforeach; ?>
                        </select> <br/>
                        
                    </div>
                    
                    <div class="wpia-col">
                        <strong><?php _e("Display title?", "wpia"); ?></strong><br />
                        <select id="wpia_calendar_title">
                            <option value="yes"><?php _e("Yes", "wpia"); ?></option>
                            <option value="no"><?php _e("No", "wpia"); ?></option>                        
                        </select> <br/>                    
                    </div>
                    
                    <div class="wpia-col">
                        <strong><?php _e("Display legend?", "wpia"); ?></strong><br />
                        <select id="wpia_calendar_legend">
                            <option value="yes"><?php _e("Yes", "wpia"); ?></option>
                            <option value="no"><?php _e("No", "wpia"); ?></option>                        
                        </select> <br/>                    
                    </div>
                    
                    <div class="wpia-col">
                        <strong><?php _e("Display dropdown?", "wpia"); ?></strong><br />
                        <select id="wpia_calendar_dropdown">
                            <option value="yes"><?php _e("Yes", "wpia"); ?></option>
                            <option value="no"><?php _e("No", "wpia"); ?></option>                        
                        </select> <br/>                    
                    </div>

                </div>
                
                <div class="wpia-row">
                    <div class="wpia-col">
                        <strong><?php _e("Week starts on", "wpia"); ?></strong><br />
                        <select id="wpia_calendar_start">
                            <option value="1"><?php _e("Monday", "wpia"); ?></option>
                            <option value="2"><?php _e("Tuesday", "wpia"); ?></option>
                            <option value="3"><?php _e("Wednesday", "wpia"); ?></option>
                            <option value="4"><?php _e("Thursday", "wpia"); ?></option>
                            <option value="5"><?php _e("Friday", "wpia"); ?></option>
                            <option value="6"><?php _e("Saturday", "wpia"); ?></option>
                            <option value="7"><?php _e("Sunday", "wpia"); ?></option>
                            
                        </select> <br/>                    
                    </div>
                    
                    <div class="wpia-col">
                        <strong><?php _e("Months to display?", "wpia"); ?></strong><br />
                        <select id="wpia_calendar_display">
                            <?php for($i=1;$i<=12;$i++):?>
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php endfor;?>                        
                        </select> <br/>                    
                    </div>
                    
                    <div class="wpia-col">
                        <strong><?php _e("Language", "wpia"); ?></strong><br />
                        <select id="wpia_calendar_language">
                            <?php $activeLanguages = json_decode(get_option('wpia-languages'),true);?>
                                <option value="auto"><?php _e("Auto (let WP choose)", "wpia"); ?></option>
                            <?php foreach($activeLanguages as $code => $language):?>
                                <option value="<?php echo $code;?>"><?php echo $language;?></option>
                            <?php endforeach;?>                   
                        </select> <br/>                    
                    </div>
                    
                    <div class="wpia-col">
                        <strong><?php _e("Start Month", "wpia"); ?></strong><br />
                        <select id="wpia_calendar_start_month">
                            <option value="0"><?php _e("Current Month", "wpia"); ?></option>
                            <option value="1"><?php _e("January", "wpia"); ?></option>
                            <option value="2"><?php _e("February", "wpia"); ?></option>
                            <option value="3"><?php _e("March", "wpia"); ?></option>
                            <option value="4"><?php _e("April", "wpia"); ?></option>
                            <option value="5"><?php _e("May", "wpia"); ?></option>
                            <option value="6"><?php _e("June", "wpia"); ?></option>
                            <option value="7"><?php _e("July", "wpia"); ?></option>
                            <option value="8"><?php _e("August", "wpia"); ?></option>
                            <option value="9"><?php _e("September", "wpia"); ?></option>
                            <option value="10"><?php _e("October", "wpia"); ?></option>
                            <option value="11"><?php _e("November", "wpia"); ?></option>
                            <option value="12"><?php _e("December", "wpia"); ?></option>   
                        </select> <br/>                    
                    </div>

                </div>

                <div class="wpia-row">
                    
                    <div class="wpia-col">
                        <strong><?php _e("Start Year", "wpia"); ?></strong><br />
                        <select id="wpia_calendar_start_year">
                            <option value="0"><?php _e("Current Year", "wpia"); ?></option>
                            <?php for($i = date("Y"); $i<= date("Y") + 10; $i++):?>
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php endfor;?>                 
                        </select> <br/>                    
                    </div>

                    <div class="wpia-col">
                        <strong><?php echo __('Show history?','wpia');?></strong><br />
                        <select id="wpia_calendar_history">
                            <option value="1"><?php echo __('Display booking history','wpia');?></option>
                            <option value="2"><?php echo __('Replace booking history with the default legend item','wpia');?></option>
                            <option value="3"><?php echo __('Use the Booking History Color from the Settings','wpia');?></option>                        
                        </select> <br/>                    
                    </div>
                    
                    <!-- <div class="wpia-col">
                        <strong><?php echo __('Show tooltip?','wpia');?></strong><br />
                        <select id="wpia_calendar_tooltip">
                            <option selected="selected" value="1"><?php _e("No", "wpia"); ?></option>
                            <option value="2"><?php _e("Yes", "wpia"); ?></option>
                            <option value="3"><?php _e("Yes, with red indicator", "wpia"); ?></option>                      
                        </select> <br/>                    
                    </div> -->
                    
                    <div class="wpia-col">
                        <strong><?php echo __('Show week numbers?');?></strong><br />
                        <select id="wpia_calendar_week_numbers">
                            <option value="yes"><?php _e("Yes", "wpia"); ?></option>
                            <option selected="selected" value="no"><?php _e("No", "wpia"); ?></option>                        
                        </select> <br/>                    
                    </div>

                    <!-- @since     6.5.4 -->
                    <div class="wpia-col">
                        <strong><?php echo __("Jump Switch?",'wpia') ;?></strong><br />
                        <select id="wpia_calendar_jump">
                            <option value="no">No</option>
                            <option value="yes">Yes</option>
                        </select> <br/>
                    </div>
                </div>
                
                
                
               
                <input type="button" class="button-primary wpia-close-button" value="<?php _e("Insert Calendar", "wpia"); ?>" onclick="wpia_insert_shortcode();"/>&nbsp;&nbsp;&nbsp;
                <a class="button wpia-close-button"  href="#"><?php _e("Cancel", "wpia"); ?></a>
                <!-- end: Insert Calendar -->
            </div>
        </div>
        <div class="wpia-tabs-page" id="insertOverview">
            <div class="wpia-modal-container">
            <!-- start: Insert Overview Calendar -->
                <h3><?php _e("Insert multiple calendar overview", "wpia"); ?></h3>
                <p>
                    <?php _e("Create a calendar overview that displays all selected calendars and their availability", "wpia"); ?>
                </p>
                <div class="wpia-row">

                    
                    <div class="wpia-col">
                        <strong><?php _e("Display legend?", "wpia"); ?></strong><br />
                        <select id="wpia_calendar_overview_legend">
                            <option value="yes"><?php _e("Yes", "wpia"); ?></option>
                            <option value="no"><?php _e("No", "wpia"); ?></option>                        
                        </select> <br/>                    
                    </div>

                    <div class="wpia-col">
                        <strong><?php _e("Language", "wpia"); ?></strong><br />
                        <select id="wpia_calendar_overview_language">
                            <?php $activeLanguages = json_decode(get_option('wpia-languages'),true);?>
                                <option value="auto"><?php _e("Auto (let WP choose)", "wpia"); ?></option>
                            <?php foreach($activeLanguages as $code => $language):?>
                                <option value="<?php echo $code;?>"><?php echo $language;?></option>
                            <?php endforeach;?>                   
                        </select> <br/>                    
                    </div>

                    <div class="wpia-col">
                        <strong><?php _e("Start Date", "wpia"); ?></strong><br />
                        <select id="wpia_calendar_overview_start">
                            <option value="0"><?php _e("Current Month", "wpia"); ?></option>
                            <option value="1"><?php _e("January", "wpia"); ?></option>
                            <option value="2"><?php _e("February", "wpia"); ?></option>
                            <option value="3"><?php _e("March", "wpia"); ?></option>
                            <option value="4"><?php _e("April", "wpia"); ?></option>
                            <option value="5"><?php _e("May", "wpia"); ?></option>
                            <option value="6"><?php _e("June", "wpia"); ?></option>
                            <option value="7"><?php _e("July", "wpia"); ?></option>
                            <option value="8"><?php _e("August", "wpia"); ?></option>
                            <option value="9"><?php _e("September", "wpia"); ?></option>
                            <option value="10"><?php _e("October", "wpia"); ?></option>
                            <option value="11"><?php _e("November", "wpia"); ?></option>
                            <option value="12"><?php _e("December", "wpia"); ?></option>   
                        </select> <br/>                    
                    </div>

                    <div class="wpia-col">
                        <strong><?php echo __('Show history?','wpia');?></strong><br />
                        <select id="wpia_calendar_overview_history">
                            <option value="1"><?php echo __('Display booking history','wpia');?></option>
                            <option value="2"><?php echo __('Replace booking history with the default legend item','wpia');?></option>
                            <option value="3"><?php echo __('Use the Booking History Color from the Settings','wpia');?></option>                        
                        </select> <br/>                    
                    </div>

                </div>

                <div class="wpia-row">

                    <div class="wpia-col">
                        <strong><?php echo __('Show day abbreviations?');?></strong><br />
                        <select id="wpia_calendar_overview_week_numbers">
                            <option value="yes"><?php _e("Yes", "wpia"); ?></option>
                            <option selected="selected" value="no"><?php _e("No", "wpia"); ?></option>                        
                        </select> <br/>                    
                    </div>

                    <div class="wpia-col">
                        <strong><?php echo __('Display calendars?');?></strong><br />
                        <select id="wpia_calendar_overview_display_calendars">
                            <option selected="selected" value="1"><?php _e("All Calendars", "wpia"); ?></option>
                            <option value="2"><?php _e("Selected Calendars", "wpia"); ?></option>
                        </select> <br/>                    
                    </div>

                </div>


                <div id="wpia-dd-calendars" class="wpia-row wpia-hidden">
                    <div class="wpia-col-6">
                        <div class="bg-grey p-15 selectable-container">
                            <h3>Available Calendars</h3>
                            <p>Select and drag calendars to the other box</p>
                            <div id="wpia-calendars-set">
                                <ul id="wpia-sortable2" class="wpia-connected">
                                    <?php
                                    $calendars = $wpdb->get_results("SELECT calendarID, calendarTitle FROM " . $wpdb->prefix . "sbc_calendars;");
                                    $rows = $wpdb->get_results( $sql, ARRAY_A );
                                    
                                    foreach ( $rows as $calendar ):
                                    ?>
                                    <li id="<?php echo absint($calendar['calendarID']) ?>"><?php echo esc_html($calendar['calendarTitle']) ?></li>
                                    <?php 
                                    endforeach;
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="wpia-col-6">
                        <div class="p-15 selectable-container">
                            <h3>Selected Calendars</h3>
                            <p>The calendars that will be displayed</p>
                            <div id="wpia-calendars-set">
                                <ul id="wpia-sortable" class="wpia-connected">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <input type="button" class="button-primary" value="<?php _e("Insert Multiple Calendar Overview", "wpia"); ?>" onclick="wpia_insert_overview_shortcode();"/>&nbsp;&nbsp;&nbsp;
                <a class="button wpia-close-button"  href="#"><?php _e("Cancel", "wpia"); ?></a>
                <!-- end: Insert Overview Calendar -->
            </div>
        </div>
        <!-- <div class="wpia-tabs-page" id="editShortcode">
            <div class="wpia-modal-container">
            Third tab
            </div>
        </div> -->
    </div>
    <!-- end: Tabs -->

</div>

<?php die(); ?>