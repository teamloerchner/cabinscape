<?php global $wpdb; ?>
<script>
    wpia(document).ready(function(){
        wpia(".wpia-calendars").css('min-height',wpia('.wpia-responsive-calendars').height());
    })
    wpia(window).bind('load resize',function(){
        wpia(".wpia-calendars").css('min-height',wpia('.wpia-responsive-calendars').height());
    })
</script>
<div class="wrap wpia-wrap">
    <?php
    // Check PHP Version for compatibilty!
    // display nthe current PHP version for info

    $current_php_version = phpversion();
    $message    = null;

    if (!version_compare(phpversion(), WPIA_PHP_MIN_VERSION, ">="))
    {
        $message = array(
            'message'   => 'Your PHP version is not compatible with our plugin(s) requirement(s). Please upgrade PHP to the minimum required version (' . WPIA_PHP_MIN_VERSION . ') to ensure no errors will occur!',
            'class'     => 'warning'
        );
        ?>
        <div class="notice notice-<?php echo $message['class']; ?> is-dismissible">
            <p><?php echo $message['message']; ?></p>
            <button type="button" class="notice-dismiss">
                <span class="screen-reader-text">Dismiss this notice.</span>
            </button>
        </div>
        <?php
    }
    ?>
    <div id="icon-themes" class="icon32"></div>
    <h2><?php echo __('Edit Calendar','wpia');?></h2>
    <?php if(!empty($_GET['save']) && $_GET['save'] == 'ok'):?>
    <div id="message" class="updated">
        <p><?php echo __('The calendar was updated','wpia')?></p>
    </div>
    <?php endif;?>

    <div id="wpia-notification-wrapper">

    </div>


    <?php if(!(!empty($_GET['id']))) $_GET['id'] = 'wpia-new-calendar';?>
    <?php $sql = $wpdb->prepare('SELECT * FROM ' . $wpdb->base_prefix . 'wpia_calendars WHERE calendarID=%d',$_GET['id']); ?>
    <?php $calendar = $wpdb->get_row( $sql, ARRAY_A );?>
    <?php if(($wpdb->num_rows > 0 || $_GET['id'] == 'wpia-new-calendar') && (@in_array( get_current_user_id(), json_decode($calendar['calendarUsers'])) || current_user_can( WPIA_MIN_USER_CAPABILITY )) ):?>

        <?php if($_GET['id'] == 'wpia-new-calendar') { $calendar['calendarData'] = '{}';}?>
        <div class="postbox-container">
            <?php echo wpia_print_legend_css((!empty($calendar['calendarID'])) ? $calendar : false); ?>
            <form action="<?php echo admin_url( 'admin.php?page=wp-ical-availability&do=save-calendar&noheader=true');?>" method="post">
            <input type="text" name="calendarTitle" class="fullTitle" id="calendarTitle" placeholder="<?php echo __('Calendar title','wpia');?>" value="<?php echo (!empty($calendar['calendarTitle'])) ? stripslashes($calendar['calendarTitle']) : "" ;?>"/>
            <div class="wpia-action-buttons">
                <a class="button button-primary button-h2 wpia_saveCalendar"><?php echo __('Save Changes','wpia');?></a>

                <!-- <input type="submit" class="button button-primary button-h2 saveCalendar" value="<?php echo __('Save Changes','wpia');?>" />  -->
                <a class="button secondary-button button-h2 button-h2-back-margin" href="<?php echo admin_url( 'admin.php?page=wp-ical-availability&do=calendars');?>"><?php echo __('Back','wpia');?></a>
            </div>


            <div class="metabox-holder">
                <div class="postbox">

                    <h3 class="hndle"><?php echo __('Availability','wpia');?></h3>
                    <div class="inside">
                            <?php $wpiaOptions = json_decode(get_site_option('wpia-options'),true);?>
                            <?php $calendarOptions =(!empty($calendar['calendarOptions'])) ? json_decode($calendar['calendarOptions'],true) : false;?>
                            <?php if(empty($wpiaOptions['backendStartDay'])) $wpiaOptions['backendStartDay'] = 1;?>



                            <?php
                            echo wpia_calendar(
                                array(
                                    'calendarFeed'      => $calendarOptions['icalendar_feed'],
                                    'calendarID'        => (!empty($calendar['calendarID'])) ? $calendar['calendarID'] : "",
                                    'firstDayOfWeek'    => $wpiaOptions['backendStartDay'],
                                    'showDateEditor'    =>  true,
                                    'reCacheFeeds'      => ( isset($calendarOptions['last_feed_count']) && $calendarOptions['last_feed_count'] != $calendarOptions['feed_count'] ) ? true : false
                                )
                            );
                            ?>
                            <input type="hidden" value="<?php echo (!empty($calendar['calendarID'])) ? $calendar['calendarID'] : "" ;?>" name="calendarID" />


                    </div>
                </div>
            </div>

            <div class="metabox-holder">
                <div class="postbox">

                    <h3 class="hndle"><?php _e('Feeds','wpia');?></h3>
                    <div class="inside">
                        <table class="form-table feeds-table">
                            <tbody>
                                <?php if(!isset($calendarOptions['icalendar_feed'])) $calendarOptions['icalendar_feed'] = array(''); ?>
                                <?php $i=1; foreach($calendarOptions['icalendar_feed'] as $feed):?>
                                <tr>
                                    <th scope="row"><label for="icalendar_feed_<?php echo $i;?>"><?php _e('iCalendar Feed','wpia');?> #<?php echo $i;?></label></th>
                                    <td><input name="icalendar_feed[]" type="text" id="icalendar_feed_<?php echo $i;?>" value="<?php echo (!empty($feed)) ? urldecode($feed) : '';?>" class="widefat" />


                                    <?php
                                    if(!empty($feed))
                                    {
                                        global $ical;
                                        $ical = new wpia_ICal($feed);
                                        // var_dump($ical);
                                        if ( isset($ical) )
                                        {
                                            // if(isset($ical->cal) == false)
                                            // {
                                            $error_msg = (!empty($ical->curl_error) || is_null($ical->cal))
                                                ? '<strong>Error fetching feed:</strong> ' . $ical->curl_error
                                                : '';
                                            ?>
                                            <small style="color:red;"><?php _e( $error_msg,'wpia');?></small>
                                            <?php
                                            // }
                                        }
                                    }?>
                                    </td>
                                </tr>
                                <?php $i++; endforeach;?>
                                <tr class="add-more-feeds">
                                    <td colspan="2" style="padding-left: 0;"><input type="button" id="wpia-add-feed" class="button-secondary" value="<?php _e('Add another feed','wpia');?>" /></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="metabox-holder">
                <div class="postbox">
                    <?php $activeLanguages = json_decode(get_site_option('wpia-languages'),true);?>
                    <h3 class="hndle"><?php echo __('Legend','wpia');?></h3>
                    <div class="inside">
                          <table class="form-table">
                            <tbody>
                                <tr>


                                    <th scope="row"><label for="available_color"><?php echo (!empty($calendarOptions['available_translation_' . wpia_get_language()])) ? $calendarOptions['available_translation_' . wpia_get_language()] : __('Available','wpia');?></label></th>
                                    <td>
                                        <input name="available_color" type="text" id="available_color" value="<?php echo (!empty($calendarOptions['available_color'])) ? $calendarOptions['available_color'] : '#DDFFCC';?>" class="wpia-color-field" />
                                        <br /><a href="#" class="open-wpia-legend-translations"><small><span>+</span> <?php _e('translations','wpia');?></small></a>
                                        <div class="wpia-legend-translations available-translations">
                                            <table class="form-table">
                                                <tbody>

                                                    <?php foreach($activeLanguages as $code => $language):?>
                                                    <tr>
                                                        <th scope="row"><label for="available_translation_<?php echo $code;?>"><?php echo $language;?></label></th>
                                                        <td>
                                                            <input name="available_translation_<?php echo $code;?>" id="available_translation_<?php echo $code;?>" type="text" value="<?php echo (!empty($calendarOptions['available_translation_' . $code])) ? $calendarOptions['available_translation_' . $code] : '';?>" />

                                                        </td>
                                                    </tr>
                                                    <?php endforeach;?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <th scope="row"><label for="booked_color"><?php echo (!empty($calendarOptions['booked_translation_' . wpia_get_language()])) ? $calendarOptions['booked_translation_' . wpia_get_language()] : __('Booked','wpia');?></label></th>
                                    <td>
                                        <input name="booked_color" type="text" id="booked_color" value="<?php echo (!empty($calendarOptions['booked_color'])) ? $calendarOptions['booked_color'] : '#FFC0BD';?>" class="wpia-color-field" />
                                        <br /><a href="#" class="open-wpia-legend-translations"><small><span>+</span> <?php _e('translations','wpia');?></small></a>
                                        <div class="wpia-legend-translations booked-translations">
                                            <table class="form-table">
                                                <tbody>
                                                    <?php foreach($activeLanguages as $code => $language):?>
                                                    <tr>
                                                        <th scope="row"><label for="booked_translation_<?php echo $code;?>"><?php echo $language;?></label></th>
                                                        <td>
                                                            <input name="booked_translation_<?php echo $code;?>" id="booked_translation_<?php echo $code;?>" type="text" value="<?php echo (!empty($calendarOptions['booked_translation_' . $code])) ? $calendarOptions['booked_translation_' . $code] : '';?>" />

                                                        </td>
                                                    </tr>
                                                    <?php endforeach;?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>


            <br />
            <a class="button button-primary button-h2 wpia_saveCalendar"><?php echo __('Save Changes','wpia');?></a>

            <!-- <input type="submit" id="edit-form-save" class="button button-primary saveCalendar" value="<?php echo __('Save Changes','wpia');?>" /> -->
            </form>
        </div>
    <?php else:?>
        <?php echo __('Invalid calendar ID.','wpia')?>
    <?php endif;?>
</div>
