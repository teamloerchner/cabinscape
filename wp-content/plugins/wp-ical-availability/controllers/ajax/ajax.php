<?php
$data = file_get_contents("php://input");

if ( !$data )
	die;

$data = json_decode($data, true);


switch ($data['action'])
{
	case 'wpia_changeDayAdmin':
		echo wpia_changeDayAdmin_callback($data);
		break;
	case 'wpia_changeDay':
		echo wpia_changeDay_callback($data);
		break;
	case 'wpia_saveCalendar':
		global $wpdb;

		if( !empty($data['id']) )
		{
		    $wpdb->update(
		    	$wpdb->prefix.'wpia_calendars', 
		    	array(
		    		'calendarTitle' 	=> $data['title'], 
		    		'modifiedDate' 		=> time()
		    	), 
		    	array(
		    		'calendarID' 		=> $data['id']
		    	)
		    );

		    if ( isset( $data['icalendar_feed'] ) )
			{
				foreach ( $data['icalendar_feed'] as $feed )
				{
					if ( !empty( $feed ) )
						$calendarOptions['icalendar_feed'][] = esc_url( $feed );    
				}

			}

			if( isset( $data['available_color'] ) )
				$calendarOptions['available_color'] = $data['available_color'];

			if( isset($data['booked_color']) )
				$calendarOptions['booked_color'] = $data['booked_color'];

			$available_translations = array();
			foreach ( $data['available_translations'] as $translation )
			{
				$available_translations[$translation['input']] = $translation['value'];

			}
			
			$booked_translations = array();
			foreach ( $data['booked_translations'] as $translation )
			{
				$booked_translations[$translation['input']] = $translation['value'];

			}

			$activeLanguages = json_decode(get_site_option('wpia-languages'),true);
			foreach($activeLanguages as $code => $language)
			{
				if(isset($available_translations['available_translation_' . $code]))
					$calendarOptions['available_translation_' . $code] 	= $available_translations['available_translation_' . $code];
				
				if(isset($booked_translations['booked_translation_' . $code]))
					$calendarOptions['booked_translation_' . $code] 	= $booked_translations['booked_translation_' . $code];        
			}


			/*request calendarOptions */
			$checkOptions 	= $wpdb->get_row(
				'SELECT calendarOptions FROM ' . $wpdb->prefix  . 'wpia_calendars WHERE calendarID = ' . $data['id'] . ';', 
				ARRAY_A
			);
			/*decode calendarOptions*/
			$checkOptions 	= json_decode( $checkOptions['calendarOptions'], true);

			/*set new feed-count*/
			$calendarOptions['feed_count'] 		= count( $calendarOptions['icalendar_feed'] );
			/*set last_feed_count to the count that was stored in DB*/
			$calendarOptions['last_feed_count']	= $checkOptions['feed_count'];
			
		    
	        $wpdb->update(
	        	$wpdb->prefix.'wpia_calendars',
	        	array(
	        		'calendarOptions' 	=> json_encode($calendarOptions)
	        	),
	        	array(
	        		'calendarID' 	=> $data['id']
	        	)
	        );
	        
		    echo json_encode(array('result'=>true, 'class'=>'success', 'msg' => 'Calendar updated.'));
		}
		else
		{

			if ( empty( $data['title'] ) )
			{
				echo json_encode(
			    	array(
			    		'result'=>false, 
			    		'class'=>'error',
			    		'msg' => 'Please enter a title'
			    	)
			    );
			    die;
			}
		    
			if ( isset( $data['icalendar_feed'] ) )
			{
				foreach ( $data['icalendar_feed'] as $feed )
				{
					if ( !empty( $feed ) )
						$calendarOptions['icalendar_feed'][] = esc_url( $feed );    
				}

			}

			if( isset( $data['available_color'] ) )
				$calendarOptions['available_color'] = $data['available_color'];

			if( isset($data['booked_color']) )
				$calendarOptions['booked_color'] = $data['booked_color'];
		    

			$available_translations = array();
			foreach ( $data['available_translations'] as $translation )
			{
				$available_translations[$translation['input']] = $translation['value'];

			}
			
			$booked_translations = array();
			foreach ( $data['booked_translations'] as $translation )
			{
				$booked_translations[$translation['input']] = $translation['value'];

			}

			$activeLanguages = json_decode(get_site_option('wpia-languages'),true);
			foreach($activeLanguages as $code => $language)
			{
				if(isset($available_translations['available_translation_' . $code]))
					$calendarOptions['available_translation_' . $code] 	= $available_translations['available_translation_' . $code];
				
				if(isset($booked_translations['booked_translation_' . $code]))
					$calendarOptions['booked_translation_' . $code] 	= $booked_translations['booked_translation_' . $code];        
			}

			$calendarOptions['feed_count']	= count($calendarOptions['icalendar_feed']);

			$wpdb->show_errors();
		    
		    $result = $wpdb->insert(
		    	$wpdb->prefix.'wpia_calendars',
		    	array(
		    		'calendarTitle' 	=> sanitize_text_field($data['title']),
		    		'modifiedDate' 		=> time(), 
					'calendarOptions' 	=> json_encode($calendarOptions),
		    		'createdDate' 		=> time()
		    	)
		    );
		    
		    if ( !$result )
		    {
		    	$result = json_encode(
			    	array(
			    		'result'=>false, 
			    		'class'=>'error', 
			    		'msg' => $wpdb->print_error()
			    	)
			    );
		    }
		    else
		    {
		    	$result = json_encode(
			    	array(
			    		'result'=>true, 
			    		'class'=>'success', 
			    		'url'=> admin_url('admin.php?page=wp-ical-availability&do=edit-calendar&id='.$wpdb->insert_id.'&save=ok'),
			    		'msg' => 'Creating calendar...'
			    	)
			    );
		    }
		    echo $result;
		}
		die();
		break;

		// 
}
die();