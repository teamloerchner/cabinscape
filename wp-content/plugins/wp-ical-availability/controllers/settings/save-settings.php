<?php

/** Save Languages **/
$languages = array('en' => 'English', 'bg' => 'Bulgarian','ca' => 'Catalan','hr' => 'Croatian','cz' => 'Czech','da' => 'Danish','nl' => 'Dutch','et' => 'Estonian','fi' => 'Finnish','fr' => 'French','de' => 'German','el' => 'Greek','hu' => 'Hungarian','it' => 'Italian', 'jp' => 'Japanese', 'no' => 'Norwegian','pl' => 'Polish','pt' => 'Portugese','ro' => 'Romanian','ru' => 'Russian','sk' => 'Slovak','sl' => 'Slovenian','es' => 'Spanish','sv' => 'Swedish','tr' => 'Turkish','ua' => 'Ukrainian');

foreach($languages as $code => $language):
    if(!empty($_POST[$code]))
        $activeLanguages[$code] = $language;
endforeach;
if(empty($activeLanguages)) $activeLanguages['en'] = 'English';

update_site_option('wpia-languages',json_encode($activeLanguages));

$wpiaOptions['dateFormat'] = $_POST['dateFormat'];
$wpiaOptions['backendStartDay'] = $_POST['backend-start-day'];
$wpiaOptions['displayDays'] = $_POST['date-type'];
$wpiaOptions['alwaysEnqueueScripts'] = $_POST['always-enqueue-scripts'];
$wpiaOptions['backendCalendars'] = $_POST['backend-calendars'];

$wpiaOptions['enableCaching'] = $_POST['enable-caching'];
$wpiaOptions['cacheInterval'] = $_POST['cache-interval'];



update_site_option('wpia-options',json_encode($wpiaOptions));

wp_redirect(admin_url('admin.php?page=wp-ical-availability-settings&save=ok'));
die();