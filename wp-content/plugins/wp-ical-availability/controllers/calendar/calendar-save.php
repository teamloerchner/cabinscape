<?php
global $wpdb;

if(isset($_POST['icalendar_feed'])) {
    foreach($_POST['icalendar_feed'] as $feed){
        if(!empty($feed))
            $calendarOptions['icalendar_feed'][] = esc_url($feed);    
    }
    
};
if(isset($_POST['available_color'])) $calendarOptions['available_color'] = $_POST['available_color'];
if(isset($_POST['booked_color'])) $calendarOptions['booked_color'] = $_POST['booked_color'];



$activeLanguages = json_decode(get_site_option('wpia-languages'),true);
foreach($activeLanguages as $code => $language){
    if(isset($_POST['available_translation_' . $code])) $calendarOptions['available_translation_' . $code] = $_POST['available_translation_' . $code];
    if(isset($_POST['booked_translation_' . $code])) $calendarOptions['booked_translation_' . $code] = $_POST['booked_translation_' . $code];        
}

if(!empty($_POST['calendarID'])){
    $wpdb->update( $wpdb->base_prefix.'wpia_calendars', array('calendarTitle' => stripslashes($_POST['calendarTitle']), 'calendarOptions' => json_encode($calendarOptions), 'modifiedDate' => time()), array('calendarID' => $_POST['calendarID']) );  
    
    
    
    
    wp_redirect(admin_url('admin.php?page=wp-ical-availability&do=edit-calendar&id='.$_POST['calendarID'].'&save=ok'));
} else {
    $wpdb->insert( $wpdb->base_prefix.'wpia_calendars', array('calendarTitle' => stripslashes($_POST['calendarTitle']), 'calendarOptions' => json_encode($calendarOptions), 'modifiedDate' => time(), 'createdDate' => time() ));
    

    
    wp_redirect(admin_url('admin.php?page=wp-ical-availability&do=edit-calendar&id='.$wpdb->insert_id.'&save=ok'));     
}
die();


?>