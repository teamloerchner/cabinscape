<?php
/**
 * Plugin Name: WP iCal Availability
 * Plugin URI:  http://www.wpicalavailability.com
 * Description: WP iCal Availability
 * Version:     2.5.9
 * Author:      WP iCal Availability
 * Author URI:  http://www.wpicalavailability.com
 *
 * Copyright (c) 2017 WP iCal Availability
 */

// error_reporting(0);

include 'include/createTables.php';
register_activation_hook( __FILE__, 'wpia_install' );

define("WPIA_VERSION", "2.5.9" );
define("WPIA_MIN_USER_CAPABILITY", "manage_options");
define("WPIA_PATH",plugins_url('',__FILE__));
define("WPIA_DIR_PATH",dirname(__FILE__));
define("WPIA_PHP_MIN_VERSION", "5.5");

add_action( 'plugins_loaded', 'wpia_load_textdomain' );
function wpia_load_textdomain() 
{
    load_plugin_textdomain( 'wpia', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' ); 
}


include 'include/calendarLanguages.php';
include 'include/calendarFunctions.php';
include 'include/calendarAdmin.php';
include 'include/calendarCore.php';
include 'include/calendarOverviewCore.php';
include 'include/calendarAjax.php';
include 'include/class.iCalReader.php';

include 'include/pluginStructure.php';
include 'include/pluginShortcodeButton.php';
include 'include/pluginShortcode.php';
include 'include/pluginShortcodeOverview.php';
include 'include/pluginWidget.php';

if ( class_exists( 'SiteOrigin_Widget' ) )
{
    function add_my_awesome_widgets_collection($folders){
        $folders[] = dirname( __FILE__ );
        return $folders;
    }
    add_filter('siteorigin_widgets_widget_folders', 'add_my_awesome_widgets_collection');
}

if ( defined( 'WPB_VC_VERSION' ) ) 
{
    add_action( 'vc_before_init', 'vc_before_init_actions' );
 
    function vc_before_init_actions() {

        //.. Code from other Tutorials ..//

        // Require new custom Element
        require_once( dirname( __FILE__ ).'/include/vc-implementation.php' ); 

    }
}


if(get_option('timezone_string')){
    @date_default_timezone_set(get_option('timezone_string'));    
}
if(get_option('gmt_offset')){
    @date_default_timezone_set(wpia_tz_offset_to_name(get_option('gmt_offset')));

}

if (is_admin()) {
    add_action('admin_head','wpia_prevent_iphone_format');
    function wpia_prevent_iphone_format(){
        echo '<meta name="format-detection" content="telephone=no" />';
    }
    
	add_action('admin_menu', 'wpia_menu');   
    function wpia_admin_enqueue_files() {

        wp_enqueue_style( 'wpia-calendar', WPIA_PATH . '/css/wpia-calendar.css', array(), WPIA_VERSION );
        wp_enqueue_style( 'wpia-admin', WPIA_PATH . '/css/wpia-admin.css', array(), WPIA_VERSION );

        wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_style( 'chosen', WPIA_PATH . '/css/chosen.min.css', array(), WPIA_VERSION );

        wp_enqueue_style( 'wpia-scrollbar-css', WPIA_PATH . '/css/perfect-scrollbar.min.css', array(), WPIA_VERSION );

        wp_enqueue_script('wpia-scrollbar-js', WPIA_PATH . '/js/perfect-scrollbar.jquery.min.js', array('jquery'), WPIA_VERSION );
        
        // Register Admin Script
        wp_register_script( 'wpia-admin', WPIA_PATH . '/js/wpia-admin.js', array('jquery', 'jquery-ui-core', 'jquery-ui-draggable', 'jquery-ui-sortable'), WPIA_VERSION );
        // Localize Admin Script and overwrite wpia_ajaxurl
        wp_localize_script( 'wpia-admin', 'wpia_ajaxurl', admin_url( 'admin.php?page=wp-ical-availability&do=ajax-call&noheader=true' ) );

        // Enqueue registered Admin Script
        wp_enqueue_script('wpia-admin');

        wp_enqueue_script('wpia-colorpicker', WPIA_PATH . '/js/colorpicker.js', array( 'jquery', 'wp-color-picker' ), WPIA_VERSION );
        wp_enqueue_script('custom-select', WPIA_PATH . '/js/custom-select.js', array('jquery'), WPIA_VERSION );
        wp_enqueue_script('chosen', WPIA_PATH . '/js/chosen.jquery.min.js', array('jquery'), WPIA_VERSION );
        


    }
    add_action( 'admin_init', 'wpia_admin_enqueue_files' );       
} else {
    
    function wpia_enqueue_files() {
        wp_enqueue_style( 'wpia-calendar', WPIA_PATH . '/css/wpia-calendar.css', array(), WPIA_VERSION );
        wp_enqueue_style( 'wpia-overview-calendar', WPIA_PATH . '/css/wpia-overview-calendar.css', array(), WPIA_VERSION );
        wp_enqueue_script('wpia', WPIA_PATH . '/js/wpia.js', array('jquery'), WPIA_VERSION );
        wp_enqueue_script('custom-select', WPIA_PATH . '/js/custom-select.js', array('jquery'), WPIA_VERSION );    
    }
    $wpiaOptions = json_decode(get_site_option('wpia-options'),true);  
    if(!empty($wpiaOptions['alwaysEnqueueScripts']) && $wpiaOptions['alwaysEnqueueScripts'] == 'yes'){
        add_action( 'wp_enqueue_scripts', 'wpia_enqueue_files' );    
    }
    
    add_action('wp_head','wpia_ajaxurl');
}

//Admin Menu
function wpia_menu(){
    add_menu_page( 'WP iCal Availability', 'WP iCal <br />Availability', 'read', 'wp-ical-availability', 'wpia_calendars', WPIA_PATH . '/images/date-button.gif' , 108 );
    add_submenu_page( 'wp-ical-availability', __('Calendars','wpia'), __('Calendars','wpia'), 'read', 'wp-ical-availability', 'wpia_calendars' );  
    add_submenu_page( 'wp-ical-availability', __('Settings','wpia'), __('Settings','wpia'), 'read', 'wp-ical-availability-settings', 'wpia_settings' );

    add_submenu_page( null, __('Shortcode Options', 'wpia'), __('Shortcode Options', 'wpia'), 'read', 'wp-ical-availability-shortcode', 'wpia_shortcode_options');
}
add_action('wp_ajax_wpia_changeDayAdmin',           'wpia_changeDayAdmin_callback');
add_action('wp_ajax_wpia_changeOverview',           'wpia_changeOverview_callback');
add_action('wp_ajax_wpia_changeDay',                'wpia_changeDay_callback');
add_action('wp_ajax_nopriv_wpia_changeDay',         'wpia_changeDay_callback');
add_action('wp_ajax_nopriv_wpia_changeOverview',    'wpia_changeOverview_callback');

function wpia_ajaxurl() {
    ?>
    <script type="text/javascript">var wpia_ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';</script>
    <?php
}

