<?php
/*
Element Description: WP iCal Availability
*/
 
// Element Class 
class WPIA_VC_Shortcode extends WPBakeryShortCode {
     
    // Element Init
    function __construct() {
        add_action( 'init', array( $this, 'vc_wpia_mapping' ) );
        add_shortcode( 'vc-wpia', array( $this, 'vc_wpia_shortcode' ) );
        add_shortcode( 'vc-wpia-overview', array( $this, 'vc_wpia_shortcode_overview' ) );
    }
     
    // Element Mapping
    public function vc_wpia_mapping() 
    {
    	global $wpdb;

    	$calendarsSql 	= 'SELECT * FROM ' . $wpdb->base_prefix . 'wpia_calendars';
    	$rows 			= $wpdb->get_results( $calendarsSql, ARRAY_A );
    	$calendars 		= array();

    	foreach ( $rows as $calendar)
    	{
    		$calendars[trim(esc_html($calendar['calendarTitle']))] = $calendar['calendarID'];
    	}

    	$activeLanguages= json_decode(get_option('wpia-languages'),true);
    	$languages 		= array();
    	$languages['Auto Detect'] = 'auto';

    	foreach($activeLanguages as $code => $language)
    	{
    		$languages[$language] = $code;
    	}

    	$years 			= array();
    	$current_year 	= date('Y');

    	for ( $i = $current_year; $i <= $current_year+10; $i++ )
    	{
    		$years[] = $i;
    	}

    	$months 		= array(
    		__("Current Month", "wpia"),
            __("January", "wpia"),
            __("February", "wpia"),
            __("March", "wpia"),
            __("April", "wpia"),
            __("May", "wpia"),
            __("June", "wpia"),
            __("July", "wpia"),
            __("August", "wpia"),
            __("September", "wpia"),
            __("October", "wpia") ,
            __("November", "wpia") ,
            __("December", "wpia")
    	);
         
        vc_map( 
  
	        array(
	            'name' => __('WP iCal Availability', 'wpia'),
	            'base' => 'wpia',
	            'description' => __('Individual Calendar', 'wpia'), 
	            'category' => __('Calendars', 'wpia'),   
	            'icon' => plugins_url('../images/icon.png', __FILE__),
	            'params' => array(   
	                      
	                array(
	                    'type' => 'dropdown',
	                    'heading' => __( 'Calendar', 'wpia' ),
	                    'param_name' => 'id',
	                    'value' => $calendars,
	                    'group' => 'Individual Calendar',
	                ),  
	                  
	                array(
	                    'type' => 'dropdown',
	                    'heading' => __( 'Display title?', 'wpia' ),
	                    'param_name' => 'title',
	                    'value' => array( array( 'yes' => 'Yes' ), array( 'no' => 'No' ) ),
	                    'group' => 'Individual Calendar',
	                ),

	                array(
	                    'type' => 'dropdown',
	                    'heading' => __( 'Display legend?', 'wpia' ),
	                    'param_name' => 'legend',
	                    'value' => array( array( 'yes' => 'Yes' ), array( 'no' => 'No' ) ),
	                    'group' => 'Individual Calendar',
	                ),

	                array(
	                    'type' => 'dropdown',
	                    'heading' => __( 'Display dropdown?', 'wpia' ),
	                    'param_name' => 'dropdown',
	                    'value' => array( array( 'yes' => 'Yes' ), array( 'no' => 'No' ) ),
	                    'group' => 'Individual Calendar',
	                ),

	                array(
	                    'type' => 'dropdown',
	                    'heading' => __( 'Week starts on?', 'wpia' ),
	                    'param_name' => 'start',
	                    'value' => array(
	                    	array( '1' => __("Monday", "wpia") ), 
	                    	array( '2' => __("Tuesday", "wpia") ),
	                    	array( '3' => __("Wednesday", "wpia") ),
	                    	array( '4' => __("Thursday", "wpia") ),
	                    	array( '5' => __("Friday", "wpia") ),
	                    	array( '6' => __("Saturday", "wpia") ),
	                    	array( '7' => __("Sunday", "wpia") )
	                    ),
	                    'group' => 'Individual Calendar',
	                ),

	                array(
	                    'type' => 'dropdown',
	                    'heading' => __( 'Months to display?', 'wpia' ),
	                    'param_name' => 'display',
	                    'value' => array( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ),
	                    'group' => 'Individual Calendar',
	                ),

	                array(
	                    'type' => 'dropdown',
	                    'heading' => __( 'Language?', 'wpia' ),
	                    'param_name' => 'language',
	                    'value' => $languages,
	                    'group' => 'Individual Calendar',
	                ),


	                array(
	                    'type' => 'dropdown',
	                    'heading' => __( 'Start Month?', 'wpia' ),
	                    'param_name' => 'month',
	                    'value' => $months,
	                    'group' => 'Individual Calendar',
	                ),


	                array(
	                    'type' => 'dropdown',
	                    'heading' => __( 'Start Year?', 'wpia' ),
	                    'param_name' => 'year',
	                    'value' => $years,
	                    'group' => 'Individual Calendar',
	                ),


	                array(
	                    'type' => 'dropdown',
	                    'heading' => __( 'Show history?', 'wpia' ),
	                    'param_name' => 'history',
	                    'value' => array(
	                    	array( __('Display booking history','wpia') => 1 ),
	                    	array( __('Replace booking history with the default legend item','wpia') => 2 ),
	                    	array( __('Use the Booking History Color from the Settings','wpia') => 3 ),
	                    ),
	                    'group' => 'Individual Calendar',
	                ),


	                array(
	                    'type' => 'dropdown',
	                    'heading' => __( 'Show week numbers?', 'wpia' ),
	                    'param_name' => 'weeknumbers',
	                    'value' => array( array( 'yes' => 'Yes' ), array( 'no' => 'No' ) ),
	                    'group' => 'Individual Calendar',
	                ),


	                array(
	                    'type' => 'dropdown',
	                    'heading' => __( 'Jump Switch?', 'wpia' ),
	                    'param_name' => 'jump',
	                    'value' => array( array( 'yes' => 'Yes' ), array( 'no' => 'No' ) ),
	                    'group' => 'Individual Calendar',
	                )             
	                     
	            )
	        )			
	    );

		vc_map(

			array(
	            'name' => __('WP iCal Availability', 'wpia'),
	            'base' => 'wpia-overview',
	            'description' => __('Multiple Calendars', 'wpia'), 
	            'category' => __('Calendars', 'wpia'),   
	            'icon' => plugins_url('../images/icon.png', __FILE__),
	            'params' => array(
	                array(
	                    'type' => 'textfield',
	                    'holder' => 'h3',
	                    'class' => 'title-class',
	                    'heading' => __( 'Title', 'wpia' ),
	                    'param_name' => 'title',
	                    'value' => __( 'Default value', 'wpia' ),
	                    'description' => __( 'Box Title', 'wpia' ),
	                    'admin_label' => false,
	                    'weight' => 0,
	                    'group' => 'Multiple Calendar',
	                ),  
	                  
	                array(
	                    'type' => 'textarea',
	                    'holder' => 'div',
	                    'class' => 'text-class',
	                    'heading' => __( 'Text', 'wpia' ),
	                    'param_name' => 'text',
	                    'value' => __( 'Default value', 'wpia' ),
	                    'description' => __( 'Box Text', 'wpia' ),
	                    'admin_label' => false,
	                    'weight' => 0,
	                    'group' => 'Multiple Calendar',
	                )                           
	            	
	            )
	        )
		);
        
    } 
     
     
    // Element HTML
    public function vc_wpia_shortcode( $atts ) {
        require_once 'pluginShortcode.php';
        
        return wpia_shortcode( $atts );
        die();
    }

    public function vc_wpia_shortcode_overview( $atts ) {
        require_once 'pluginShortcodeOverview.php';
        return wpia_shortcode_overview( $atts );
        die();
    } 
     
} // End Element Class
 
// Element Class Init
new WPIA_VC_Shortcode();   