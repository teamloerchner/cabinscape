<?php
/**
 * This function prepares the calendar
 */
function wpia_calendar($options = array()){
    
    $default_options = array('ajaxCall' => false, 'calendarFeed' => null, 'monthToShow' => null, 'yearToShow' => null, 'currentCalendar' => 1, 'totalCalendars' => 1 , 'firstDayOfWeek' => 1, 'showDropdown' => 1, 'calendarLanguage' => 'en', 'calendarData' => null, 'currentTimestamp' => mktime(0, 0, 0, date('n') , 15, date('Y')),'calendarLegend' => false, 'calendarID' => false, 'showDateEditor' => false, 'jump' => false, 'showLegend' => false, 'calendarHistory' => 1, 'showWeekNumbers' => false, 'reCacheFeeds' => false);
   
    
    foreach($default_options as $key => $value){
        if(empty($$key))
            $$key = $value;
    }
    
    extract($options);
    
    $output = '';
    
    $wpiaOptions = json_decode(get_site_option('wpia-options'),true);


    if ( !$ajaxCall )
    {
        if ( $wpiaOptions['enableCaching'] == 'yes' )
        {
            if ( $reCacheFeeds == true )
            {
                if( $calendarFeed !== null )
                {
                    foreach( $calendarFeed as $feed )
                    {
                        global $ical;
                        $ical   = new wpia_ICal( $feed );
                        $events = array(); 
                        
                        if( is_array( $ical->events() ) )
                            $events = array_merge( $events, $ical->events() );

                        if( is_array( $ical->freeBusyEvents() ) )
                            $events = array_merge( $events, $ical->freeBusyEvents() );

                        $cacheData[ esc_url( $feed ) ]  = wpia_dataForCache( $events );
                    }

                    wpia_saveCache( $calendarID, $cacheData );

                    $cacheData = wpia_getCache( $calendarID );
                    $calendarData = wpia_displayData( $cacheData, $wpiaOptions['displayDays'] );

                    /* set the count so next load will not recache again */
                    wpia_updateFeedCount( $calendarID, count($calendarFeed) );
                }
            }

            /* If caching is enabled then get data from cache but first check if we got cached data */
            if ( wpia_checkCacheData( $calendarID, $wpiaOptions['cacheInterval'] ) )
            {
                $calendarData   = wpia_displayData( wpia_getCache($calendarID), $wpiaOptions['displayDays'] );
            }
            else
            {
                if( $calendarFeed !== null )
                {
                    foreach( $calendarFeed as $feed )
                    {
                        global $ical;
                        $ical   = new wpia_ICal( $feed );
                        $events = array(); 
                        
                        if( is_array( $ical->events() ) )
                            $events = array_merge( $events, $ical->events() );

                        if( is_array( $ical->freeBusyEvents() ) )
                            $events = array_merge( $events, $ical->freeBusyEvents() );

                        $cacheData[ esc_url( $feed ) ]  = wpia_dataForCache( $events );
                    }

                    wpia_saveCache( $calendarID, $cacheData );

                    $cacheData = wpia_getCache( $calendarID );
                    $calendarData = wpia_displayData( $cacheData, $wpiaOptions['displayDays'] );
                }
            }

        }
        else
        {
            /*data will need to come from feed*/
            if( $calendarFeed !== null )
            {
                foreach( $calendarFeed as $feed )
                {
                    global $ical;
                    $ical   = new wpia_ICal( $feed );
                    $events = array(); 

                    if( is_array( $ical->events() ) )
                        $events = array_merge( $events, $ical->events() );

                    if( is_array( $ical->freeBusyEvents() ) )
                        $events = array_merge( $events, $ical->freeBusyEvents() );

                    if( $events )
                        $calendarData[$feed]['events'] = wpia_getDataFromFeedData( $events );
                    // echo'<pre>';var_dump(wpia_displayData($calendarData, $wpiaOptions['displayDays'] ) );echo '</pre>';
                }
            }

            $calendarData   = json_encode(wpia_displayData( $calendarData, $wpiaOptions['displayDays'] ) );
        }
    }

    // echo'<pre>';var_dump($calendarData);echo'</pre>';

    if ($ajaxCall == false)
    {
        $output .= '<div class="wpia-container wpia-calendar-'.$calendarID.'">';
        $output .= '<div class="wpia-calendars">';
    }
    
    
    $output .= '<div class="wpia-responsive-calendars">';
        
    if($showDateEditor)    
        $totalCalendars = (!empty($wpiaOptions['backendCalendars'])) ? $wpiaOptions['backendCalendars'] : 3;
  
          
    for($i=0;$i<$totalCalendars;$i++):
        $calendarTimestamp = mktime(0, 0, 0, date('n',$currentTimestamp) + $i, 1, date('Y',$currentTimestamp));    
        $displayMonth = date('n', $calendarTimestamp);
        $displayYear = date('Y', $calendarTimestamp);
                
        $output .= wpia_showCalendar(
            array(
                'ajaxCall'          => $ajaxCall,
                'monthToShow'       => $displayMonth, 
                'yearToShow'        => $displayYear, 
                'currentCalendar'   => $i + 1, 
                'totalCalendars'    => $totalCalendars , 
                'firstDayOfWeek'    => ($firstDayOfWeek == 7) ? 0 : $firstDayOfWeek, 
                'calendarLanguage'  => ($showDateEditor) ? substr(get_locale(),0,2) : $calendarLanguage, 
                'showDropdown'      => $showDropdown, 
                'calendarData'      => $calendarData, 
                'calendarID'        => $calendarID, 
                'jump'              => $jump, 
                'calendarHistory'   => $calendarHistory,  
                'showWeekNumbers'   => $showWeekNumbers
            )
        );
    endfor;
    
    
    
    if($showLegend == 'yes'){
        $output .= '<div class="wpia-legend">';
            $output .= wpia_print_legend($calendarID, $calendarLanguage);  
        $output .= '<div class="wpia-clear"><!-- --></div></div>';
    }    
    
    
        $output .= '</div>';
        
     
    
    $output .= "<div class='wpia-clear'></div>";


    if ( is_array( $calendarData ) )
        $calendarData = json_encode( $calendarData );
    
    
    $output .= '<div class="wpia-calendar-options">';
    $output .= '<div class="wpia-show-legend" data-show-legend="'.$showLegend.'">';
    $output .= '</div><div class="wpia-current-timestamp" data-current-timestamp="'.$currentTimestamp.'">';
    $output .= '</div><div class="wpia-total-calendars" data-total-calendars="'.$totalCalendars.'">';
    $output .= "</div><div class='wpia-calendar-data' data-calendar-data='" . html_entity_decode(esc_html(($calendarData))) . "'>";
    $output .= "</div><div class='wpia-calendar-legend' data-calendar-legend='" . html_entity_decode(esc_html($calendarLegend)) . "'>";
    $output .= '</div><div class="wpia-calendar-language" data-calendar-language="'.$calendarLanguage.'">'; 
    $output .= '</div><div class="wpia-calendar-history" data-calendar-history="'.$calendarHistory.'">'; 
    $output .= '</div><div class="wpia-calendar-weeknumbers" data-calendar-weeknumbers="'.$showWeekNumbers.'">'; 
    $output .= '</div><div class="wpia-calendar-jump" data-calendar-jump="'.$jump.'">'; 
    $output .= '</div><div class="wpia-calendar-week-start" data-calendar-week-start="'.$firstDayOfWeek.'">';
    $output .= '</div><div class="wpia-show-dropdown" data-show-dropdown="'.$showDropdown.'">';
    $output .= '</div><div class="wpia-calendar-ID" data-calendar-ID="'.$calendarID.'">';
    $output .= '</div>';
    $output .= '</div>';
    
     
    
    
    if($ajaxCall == false): 
        
        $output .= '</div>';
        
        $output .= '</div><div class="wpia-clear"></div>';
        
    endif;
    
    return $output;
}

function startsWith($haystack, $needle)
{
    // search backwards starting from haystack length characters from the end
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

function fixImportAddress($importAddress)
{
    if(startsWith($importAddress,"webcal"))
    {
        $importAddress = "https" . substr($importAddress, 6); 
    }
    
    return $importAddress;
}
/**
 * This function is displays the calendar with the parameters given from the previous function
 */
function wpia_showCalendar($options = array())
{   
    foreach($options as $key => $value)
        $$key = $value;
 
    if (($monthToShow === null) or ($yearToShow === null)) {
        $today = getdate();
        $monthToShow = $today['mon'];
        $yearToShow = $today['year'];
    } else {
        $today = getdate(mktime(0, 0, 0, $monthToShow, 1, $yearToShow));
    }

    // get first and last days of the month
    $firstDay = getdate(mktime(0, 0, 0, $monthToShow, 1, $yearToShow));
    $lastDay = getdate(mktime(0, 0, 0, $monthToShow + 1, 0, $yearToShow)); //trick! day = 0

    // Create a table with the necessary header information
    $output = '<div class="wpia-calendar';
    if($showWeekNumbers == 'yes'){
        $output .= ' wpia-week-numbers';
    }  
    
    $output .= '">';
    $output .= '<div class="wpia-heading">';
    if($currentCalendar == 1){
        $output .= '<a href="#" class="wpia-prev"><img alt="'.__('Previous Month').'" src="'.WPIA_PATH.'/images/arrow-left.png" /></a>';
        if($showDropdown == true){
            $output .= '<div class="wpia-select-container"><select class="wpia-dropdown">';
                // $currentMonth   = date("m");
                // $currentYear    = date("Y");

                // $selected = '';

                // if ( mktime(0, 0, 0, $currentMonth, 15, $yearToShow) == mktime(0, 0, 0, $monthToShow, 15, $yearToShow) )
                //     $selected   = ' selected="selected"';
                
                // if ( $ajaxCall == true )
                // {
                //     if ( mktime(0, 0, 0, $currentMonth, 15, $currentYear) != mktime(0, 0, 0, $monthToShow, 15, $yearToShow) )
                //         $output .= '<option value="' . mktime(0, 0, 0, $currentMonth, 15, $currentYear) . '" ' . $selected . ' style="color: green;font-weight: bold">Current: ' . wpiaMonth(date('F',mktime(0, 0, 0, $currentMonth, 15, $currentYear)), $calendarLanguage) . " " . date('Y',mktime(0, 0, 0, $currentMonth, 15, $currentYear)) . '</option>';


                //     for( $d = $monthToShow; $d < ($monthToShow+12); $d++ )
                //     {
                //         $output .= '<option value="' . mktime(0, 0, 0, $d, 15, $yearToShow) . '"';

                //         if ( mktime(0, 0, 0, $d, 15, $yearToShow) == mktime(0, 0, 0, $monthToShow, 15, $yearToShow) )
                //             $output .= ' selected="selected"';

                //         $output .= '>';

                //         $output .= wpiaMonth(date('F',mktime(0, 0, 0, $d, 15, $yearToShow)), $calendarLanguage) . " " . date('Y',mktime(0, 0, 0, $d, 15, $yearToShow));

                //         $output .= '</option>';
                //     }
                // } 
                // else
                // {
                //     for( $d = $monthToShow; $d < ($monthToShow+12); $d++ )
                //     {
                //         $output .= '<option value="' . mktime(0, 0, 0, $d, 15, $yearToShow) . '"';

                //         if ( mktime(0, 0, 0, $d, 15, $yearToShow) == mktime(0, 0, 0, $monthToShow, 15, $yearToShow) )
                //             $output .= ' selected="selected"';

                //         $output .= '>';

                //         $output .= wpiaMonth(date('F',mktime(0, 0, 0, $d, 15, $yearToShow)), $calendarLanguage) . " " . date('Y',mktime(0, 0, 0, $d, 15, $yearToShow));

                //         $output .= '</option>';
                //     }
                // }
                


                for( $d = ($monthToShow-3); $d < ($monthToShow+12); $d++ )
                {
                    $output .= '<option value="' . mktime(0, 0, 0, $d, 15, $yearToShow) . '"';

                    if ( mktime(0, 0, 0, $d, 15, $yearToShow) == mktime(0, 0, 0, $monthToShow, 15, $yearToShow) )
                        $output .= ' selected="selected"';

                    $output .= '>';

                    $output .= wpiaMonth(date('F',mktime(0, 0, 0, $d, 15, $yearToShow)), $calendarLanguage) . " " . date('Y',mktime(0, 0, 0, $d, 15, $yearToShow));

                    $output .= '</option>';
                }
            $output .= '</select></div>';
        } else {
            $output .= "<span>" . wpiaMonth($today['month'],$calendarLanguage) . " " . $today['year'] . "</span>";    
        }
    } else {
        $output .= "<span>" . wpiaMonth($today['month'],$calendarLanguage) . " " . $today['year'] . "</span>";    
    }        
    
    if($currentCalendar == $totalCalendars)
        $output .= '<a href="#" class="wpia-next"><img alt="'.__('Next Month').'" src="'.WPIA_PATH.'/images/arrow-right.png" /></a>';
    $output .= "</div>";

    $output .= '<ul class="wpia-weekdays">';
    if($showWeekNumbers == 'yes'){
        $output .= '<li class="wpia-week-number"></li>';
    }
    
    $actday = 0; // used to count and represent each day
    
    $dayText = wpiaDoW($calendarLanguage);
    for ($i = 0; $i < 7; $i++) { // put 7 days in header, starting at appropriate day ($firstDayOfWeek)
        $output .= '<li>' . $dayText[$firstDayOfWeek + $i] . '</li>';
    }
    $output .= '</ul>';
    
    $output .= '<ul>';
    if($showWeekNumbers == 'yes'){
        $output .= '<li class="wpia-week-number" title="'.__('Week').' '.date('W',mktime(0,0,0,$monthToShow,$actday+1,$yearToShow)).'">'.date('W',mktime(0,0,0,$monthToShow,1,$yearToShow)).'</li>';
    }
    
    
    // Display the first calendar row with correct start of week
    if ($firstDayOfWeek <= $firstDay['wday']) {
        $blanks = $firstDay['wday'] - $firstDayOfWeek;
    } else {
        $blanks = $firstDay['wday'] - $firstDayOfWeek + 7;
    }
    for ($i = 1; $i <= $blanks; $i++) {
        $output .= '<li class="wpia-pad"><!-- --></li>';
    }
    
    // Note: loop below starts using the residual value of $i from loop above
    for ( /* use value of $i resulting from last loop*/; $i <= 7; $i++) {
        
        $output .= wpia_showDay($actday++,$yearToShow,$monthToShow,$calendarData,$calendarHistory); 

        
        
    }
    $output .= '</ul>';

    // Get how many complete weeks are in the actual month
    $fullWeeks = floor(($lastDay['mday'] - $actday) / 7);
    for ($i = 0; $i < $fullWeeks; $i++) {
        $output .= '<ul>';
        if($showWeekNumbers == 'yes'){
            $output .= '<li class="wpia-week-number" title="'.__('Week').' '.date('W',mktime(0,0,0,$monthToShow,$actday+1,$yearToShow)).'">'.date('W',mktime(0,0,0,$monthToShow,$actday+1,$yearToShow)).'</li>';
        }
        
        for ($j = 0; $j < 7; $j++) {
            $output .= wpia_showDay($actday++,$yearToShow,$monthToShow,$calendarData,$calendarHistory);
        }
        $output .= '</ul>';
    }

    //Now display the partial last week of the month (if there is one)
    if ($actday < $lastDay['mday']) {
        $output .= '<ul>';
        
        if($showWeekNumbers == 'yes'){
            $output .= '<li class="wpia-week-number" title="'.__('Week').' '.date('W',mktime(0,0,0,$monthToShow,$actday+1,$yearToShow)).'">'.date('W',mktime(0,0,0,$monthToShow,$actday+1,$yearToShow)).'</li>';
        }
        
        for ($i = 0; $i < 7; $i++) {
            if ($actday < $lastDay['mday']) {
                $output .= wpia_showDay($actday++,$yearToShow,$monthToShow,$calendarData,$calendarHistory);
            } else {
                $output .= '<li class="wpia-pad"><!-- --></li>';
            }
        }
        $output .= '</ul>';
    }
    $output .= '<div class="wpia-loading"><img alt="Loading" src="'.WPIA_PATH.'/images/ajax-loader.gif" /></div></div>';
    return $output;
}

function wpia_showDay($actday,$yearToShow,$monthToShow, $calendarData, $calendarHistory)
{
    // var_dump($calendarData);
    if ( !is_array( $calendarData ) )
        $calendarData = json_decode( $calendarData, true );

    if(!empty($calendarData[$yearToShow][$monthToShow][++$actday]))
        $status = $calendarData[$yearToShow][$monthToShow][$actday];
    else
        $status = 'available';

    if(mktime(0,0,0,$monthToShow,$actday+1,$yearToShow) < time() && $calendarHistory > 1){
        if($calendarHistory == 2)
            $status = 'available';
        elseif ($calendarHistory == 3)
            $status = 'grey';
    }
    $output = '<li class="wpia-day wpia-day-'.$actday.' status-' . $status .  ' ">';
        $output .= '<span class="wpia-day-split-top wpia-day-split-top-'.$status.'"></span>';
        $output .= '<span class="wpia-day-split-bottom wpia-day-split-bottom-'.$status.'"></span>';    
        $output .= '<span class="wpia-day-split-day">'.$actday.'</span>';    
    $output .= '</li>';
        
    return $output;
}





function wpia_printLegend( $calendarID, $legend, $language )
{
    $op = null;

    foreach($legend as $key => $value )
    {
        if(!empty($value['name'][$language])) 
            $legendName = $value['name'][$language]; 
        else 
            $legendName = $value['name']['default'];
        
        $op .= '<li class="wpia-legend-item wpia-calendar-' . $calendarID . '">';
            
            $op .= '<span class="wpia-legend-color status-' . $key . '">';
                $op .= '<span class="wpia-day-split-top wpia-day-split-top-'.$key.'"></span>';
                $op .= '<span class="wpia-day-split-bottom wpia-day-split-bottom-'.$key.'"></span>';
            $op .= '</span>';

            $op .= '<p>' . $legendName . '</p>';

        $op .= '</li>';

    }

    return $op;
}