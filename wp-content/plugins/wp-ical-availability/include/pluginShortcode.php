<?php
function wpia_shortcode( $atts ) {
    $wpiaOptions = json_decode(get_site_option('wpia-options'),true);  
    if(empty($wpiaOptions['alwaysEnqueueScripts']) || $wpiaOptions['alwaysEnqueueScripts'] == 'no'){
        wp_enqueue_style( 'wpia-calendar', WPIA_PATH . '/css/wpia-calendar.css' );
        wp_enqueue_style( 'wpia-calendar-overview',    WPIA_PATH . '/css/wpia-overview-calendar.css' );
        wp_enqueue_script('wpia', WPIA_PATH . '/js/wpia.js', array('jquery'));
        wp_enqueue_script('custom-select', WPIA_PATH . '/js/custom-select.js', array('jquery'));
    }
    
	extract( shortcode_atts( array(
		'id'            => null,
		'title'         => 'no',
        'legend'        => 'no',
        'dropdown'      => 'yes',
        'start'         => '1',
        'display'       => '1',
        'language'      => 'en',
        'history'       => '1',
        'jump'          => 'no',
        'weeknumbers'   => 'no',
        'month'         => 0,
        'year'          => 0
	), $atts, 'wpia' ) );
    
    
    
    if(!in_array($month,range(1,48))) {$month = date('m');}
    if(intval($year) < 1970 || intval($year) > 2100) { $year = date("Y");}
    
    if($id == null) return "WP Simple Booking Calendar: ID parameter missing.";
    if(!in_array($title,array('yes','no'))) $title = 'no';
    if(!in_array($weeknumbers,array('yes','no'))) $weeknumbers = 'no';
    if(!in_array($legend,array('yes','no'))) $legend = 'no';
    if(!in_array($dropdown,array('yes','no'))) $dropdown = 'yes';
    $dropdown = str_replace(array('yes','no'),array(true,false),$dropdown);
    
    if(!in_array(absint($start),array(1,2,3,4,5,6,7))) $start = 1;
    if(absint($display) < 1) $display = 1;    
    if(!in_array(absint($history),array(1,2,3))) $history = 1;

    if(!in_array($jump,array('yes','no'))) $jump = 'no';
        
    
    
    global $wpdb;
    
    
    if($language == 'auto'){
        $language = wpia_get_locale();
    } else {
        $activeLanguages = json_decode(get_site_option('wpia-languages'),true); if(!array_key_exists($language,$activeLanguages)) $language = 'en';    
    }

    $sql = $wpdb->prepare('SELECT * FROM ' . $wpdb->base_prefix . 'wpia_calendars WHERE calendarID=%d',$id);
    $calendar = $wpdb->get_row( $sql, ARRAY_A );
    if($wpdb->num_rows > 0):
        
        $calendarOutput = wpia_print_legend_css($calendar);
        if($title == 'yes') $calendarOutput .= '<h2>' . $calendar['calendarTitle'] . "</h2>";
        $calendarOptions =(!empty($calendar['calendarOptions'])) ? json_decode($calendar['calendarOptions'],true) : false;
        
        $calendarOutput .= wpia_calendar(
            array(
                'ajaxCall' => false, 
                'calendarHistory' => $history, 
                'calendarID' => $calendar['calendarID'], 
                'calendarFeed' => $calendarOptions['icalendar_feed'], 
                'totalCalendars' => $display, 
                'firstDayOfWeek' => $start, 
                'showDateEditor' => false, 
                'showDropdown' => $dropdown, 
                'jump' => $jump,  
                'showWeekNumbers' => $weeknumbers, 
                'showLegend' => $legend, 
                'calendarLanguage' => $language, 
                'currentTimestamp' => strtotime(date("F", mktime(0, 0, 0, $month, 15, date('Y'))) . " " . $year) )
            );
        

        return $calendarOutput;
    else:
        return __('WP iCal Availability: Invalid calendar ID.');
    endif;
	
}
add_shortcode( 'wpia', 'wpia_shortcode' );