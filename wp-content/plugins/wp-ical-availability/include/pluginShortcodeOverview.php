<?php
function wpia_shortcode_overview( $atts ) 
{
    $wpiaOptions = json_decode(get_site_option('wpia-options'),true);  
    if(empty($wpiaOptions['alwaysEnqueueScripts']) || $wpiaOptions['alwaysEnqueueScripts'] == 'no')
    {
        wp_enqueue_style( 'wpia-calendar-overview', WPIA_PATH . '/css/wpia-overview-calendar.css' );
        wp_enqueue_style( 'wpia-calendar', WPIA_PATH . '/css/wpia-calendar.css' );

    }
    
    
	extract( shortcode_atts( array(
		'calendars'           => null,
		'legend'              => 'no',
        'start'               => '0',
        'language'            => 'en',
        'history'             => '1',
        'tooltip'             => '1',
        'weeknumbers'         => 'no'
	), $atts, 'wpia' ) );
    
    
    if(!in_array($weeknumbers,array('yes','no'))) $weeknumbers = 'no';
    if(!in_array($tooltip,array(1,2,3))) $tooltip = 1;
    if(!in_array($legend,array('yes','no'))) $legend = 'no';
    if(!in_array(absint($start),array(0,1,2,3,4,5,6,7))) $start = 0;
    if(!in_array(absint($history),array(1,2,3))) $history = 1;
    
    global $wpdb;
    
    
    if($language == 'auto'){
        $language = wpia_get_locale();
    } else {
        $activeLanguages = json_decode(get_option('wpia-languages'),true);

        if(!array_key_exists($language,$activeLanguages)) 
            $language = 'en';    
    }

    if ( $calendars != "all")
    {
        $calendars          = explode(",", $calendars);

        $calendarResults    = array();

        foreach ( $calendars as $calendarId )
        {
            $sql = $wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'wpia_calendars WHERE calendarID = %d', $calendarId );
            $sql = str_replace("\\","", $sql) ;
            $calendarResults[] = $wpdb->get_row( $sql, ARRAY_A );
        }
    }
    else
    {
        $sql = 'SELECT * FROM ' . $wpdb->prefix . 'wpia_calendars ORDER by calendarID ASC';
        $sql = str_replace("\\","", $sql) ;
        $calendarResults = $wpdb->get_results( $sql, ARRAY_A );
    }
    
    if($wpdb->num_rows > 0)
    {
        $calendarOutput = '';
        foreach ( $calendarResults as $calendar )
        {
            $calendarOptions = json_decode($calendar['calendarOptions'], true);
            
            if ( is_array( $calendarOptions ) )
                $calendarOutput .= wpia_print_overview_legend_css($calendarOptions, $calendar['calendarID']);
        }

        if ( $start == 0 )
            $start = date('n');

        $calendarOutput .= wpia_displayOverview(
            array(
                'ajax'              => false,
                'calendars'         => $calendarResults,
                'currentTimestamp'  => strtotime( date( "F", mktime(0, 0, 0, $start, 15, date('Y'))) . " " . date("Y") ),
                'showTooltip'       => $tooltip,
                'showWeekNumbers'   => $weeknumbers,
                'showLegend'        => $legend,
                'showHistory'       => $history,
                'calendarLanguage'  => $language
            )
        );
        return $calendarOutput;
    }
    else
        return __('WP iCal Availability: Invalid calendar ID.');
	
}
add_shortcode( 'wpia-overview', 'wpia_shortcode_overview' );