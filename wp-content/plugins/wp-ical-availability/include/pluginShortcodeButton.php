<?php
add_action('media_buttons', 'wpia_add_form_button', 20);
function wpia_add_form_button(){
    $is_post_edit_page = in_array(basename($_SERVER['PHP_SELF']), array('post.php', 'page.php', 'page-new.php', 'post-new.php'));
    if(!$is_post_edit_page)
        return;

    // do a version check for the new 3.5 UI
    $version = get_bloginfo('version');

    if ($version < 3.5) {
        // show button for v 3.4 and below
        $image_btn =  WPIA_PATH.'/images/date-button.gif';
        echo '<a href="#TB_inline?width=480&inlineId=wpia_add_calendar" class="thickbox" id="add_wpia" title=""><img src="'.$image_btn.'" alt="' . __("Add Calendar", 'wpia') . '" /></a>';
    } else {
        // display button matching new UI
        echo '<style>.wpia_media_icon{
                background:url('.WPIA_PATH.'/images/date-button.gif) no-repeat top left;
                display: inline-block;
                height: 16px;
                margin: 0 2px 0 0;
                vertical-align: text-top;
                width: 16px;
                }
                .wp-core-ui a.wpia_media_link{
                 padding-left: 0.4em;
                }
                body #TB_window {width:800px !important; margin-left:-400px !important; max-width: 100% !important; max-height: 100% !important;}
                body #TB_ajaxContent {width: 940px !important; height: 580px !important; max-width: 100% !important; }
             </style>
              <a href="javascript:void(0)" class="button wpia_media_link" id="add_wpia" title="' . __("Add Calendar", 'wpia') . '"><span class="wpia_media_icon "></span> ' . __("Add Calendar", "wpia") . '</a>';
    }
}

add_action('admin_footer',  'wpia_add_mce_popup');    
function wpia_add_mce_popup(){
    global $wpdb;
    wp_enqueue_style( 'wpia-modal', WPIA_PATH . '/css/wpia-modal.css' );
    wp_enqueue_script('wpia-modal', WPIA_PATH . '/js/wpia-modal.js', array('jquery', 'jquery-ui-core', 'jquery-ui-draggable', 'jquery-ui-sortable'));
    ?>
    <script>
        var wpiaModalAjaxUrl   = '<?php echo admin_url( 'admin.php?page=wp-ical-availability-shortcode&do=options&noheader=true' ); ?>',
            wpiaCloseIconUrl   = '<?php echo plugin_dir_url(__FILE__); ?>';
        function wpia_insert_overview_shortcode()
        {
            var wpia_calendar_overview_display_calendars   = jQuery("#wpia_calendar_overview_display_calendars").val();
            
            
            if ( wpia_calendar_overview_display_calendars == "2" )
            {
                var selectedCalendars   = jQuery( "body #wpia-modal #wpia-sortable" ).sortable("toArray");
            }

            var wpia_calendar_overview_legend              = jQuery("#wpia_calendar_overview_legend").val();
            var wpia_calendar_overview_language            = jQuery("#wpia_calendar_overview_language").val();
            var wpia_calendar_overview_start_date          = jQuery("#wpia_calendar_overview_start").val();
            var wpia_calendar_overview_history             = jQuery("#wpia_calendar_overview_history").val();
            // var wpia_calendar_overview_tooltip             = jQuery("#wpia_calendar_overview_tooltip").val();
            var wpia_calendar_overview_week_numbers        = jQuery("#wpia_calendar_overview_week_numbers").val();
            var wpia_calendar_overview_featured_image      = jQuery("#wpia_calendar_overview_featured_image").val();


            var calendars = (wpia_calendar_overview_display_calendars == "1") ? 'all' : selectedCalendars;

            window.send_to_editor('[wpia-overview legend="' + wpia_calendar_overview_legend + '" start="' + wpia_calendar_overview_start_date + '" calendars="' + calendars + '" language="' + wpia_calendar_overview_language + '" history="' + wpia_calendar_overview_history + '" weeknumbers="' + wpia_calendar_overview_week_numbers + '"]');

            jQuery('#wpia-modal .wpia-modal-header span.wpia-close-modal').click();
        }
        function wpia_insert_shortcode(){
            var calendar_id = jQuery("#wpia_calendar_id").val();
            if(calendar_id == ""){
                alert("<?php _e("Please select a calendar", "wpia") ?>");
                return;
            }

            var wpia_calendar_title                     = jQuery("#wpia_calendar_title").val();
            var wpia_calendar_legend                    = jQuery("#wpia_calendar_legend").val();
            var wpia_calendar_dropdown                  = jQuery("#wpia_calendar_dropdown").val();
            var wpia_calendar_start                     = jQuery("#wpia_calendar_start").val();
            var wpia_calendar_display                   = jQuery("#wpia_calendar_display").val();
            var wpia_calendar_language                  = jQuery("#wpia_calendar_language").val();
            var wpia_calendar_start_month               = jQuery("#wpia_calendar_start_month").val();
            var wpia_calendar_start_year                = jQuery("#wpia_calendar_start_year").val();
            var wpia_calendar_history                   = jQuery("#wpia_calendar_history").val();
            
            var wpia_calendar_week_numbers              = jQuery("#wpia_calendar_week_numbers").val();

            var wpia_calendar_jump                      = jQuery("#wpia_calendar_jump").val();
            
            

            window.send_to_editor('[wpia id="' + calendar_id + '" title="' + wpia_calendar_title + '" legend="' + wpia_calendar_legend + '" dropdown="' + wpia_calendar_dropdown + '" start="' + wpia_calendar_start + '" display="' + wpia_calendar_display + '" language="' + wpia_calendar_language + '"  month="' + wpia_calendar_start_month + '" year="' + wpia_calendar_start_year + '" history="' + wpia_calendar_history + '" jump="' + wpia_calendar_jump + '" weeknumbers="' + wpia_calendar_week_numbers + '"]');

            jQuery('#wpia-modal .wpia-modal-header span.wpia-close-modal').click();
        }
    </script>

    <!-- <div id="wpia_add_calendar" style="display:none;">
        <div class="wrap wpia-shortcode-button-wrap">
            <div>
                <div style="padding:15px 15px 0 15px;">
                    <h3 style="color:#5A5A5A!important; font-family:Georgia,Times New Roman,Times,serif!important; font-size:1.8em!important; font-weight:normal!important;"><?php _e("Insert A Calendar", "wpia"); ?></h3>
                    <span>
                        <?php _e("Select a calendar below to add it to your post or page.", "wpia"); ?>
                    </span>
                </div>
                <div style="padding:15px 15px 0 15px; float:left; width:160px; ">
                    <strong><?php _e("Calendar", "wpia"); ?></strong><br />
                    <select id="wpia_calendar_id" style="width: 160px;">                
                        <?php $sql = 'SELECT * FROM ' . $wpdb->base_prefix . 'wpia_calendars';?>
                        <?php $rows = $wpdb->get_results( $sql, ARRAY_A );?>                         
                        <?php foreach($rows as $calendar):?>
                            <option value="<?php echo absint($calendar['calendarID']) ?>"><?php echo esc_html($calendar['calendarTitle']) ?></option>
                        <?php endforeach; ?>
                    </select> <br/>
                    
                </div>
                
                <div style="padding:15px 15px 0 15px; float:left; width:160px; ">
                    <strong><?php _e("Display title?", "wpia"); ?></strong><br />
                    <select id="wpia_calendar_title" style="width: 160px;">
                        <option value="yes"><?php _e("Yes", "wpia"); ?></option>
                        <option value="no"><?php _e("No", "wpia"); ?></option>                        
                    </select> <br/>                    
                </div>
                
                <div style="padding:15px 15px 0 15px; float:left; width:160px; ">
                    <strong><?php _e("Display legend?", "wpia"); ?></strong><br />
                    <select id="wpia_calendar_legend" style="width: 160px;">
                        <option value="yes"><?php _e("Yes", "wpia"); ?></option>
                        <option value="no"><?php _e("No", "wpia"); ?></option>                        
                    </select> <br/>                    
                </div>
                
                <div style="padding:15px 15px 0 15px; float:left; width:160px;  ">
                    <strong><?php _e("Display dropdown?", "wpia"); ?></strong><br />
                    <select id="wpia_calendar_dropdown" style="width: 160px;">
                        <option value="yes"><?php _e("Yes", "wpia"); ?></option>
                        <option value="no"><?php _e("No", "wpia"); ?></option>                        
                    </select> <br/>                    
                </div>
                
                
                <div style="padding:15px 15px 0 15px; float:left; width:160px;">
                    <strong><?php _e("Week starts on", "wpia"); ?></strong><br />
                    <select id="wpia_calendar_start" style="width: 160px;">
                        <option value="1"><?php _e("Monday", "wpia"); ?></option>
                        <option value="2"><?php _e("Tuesday", "wpia"); ?></option>
                        <option value="3"><?php _e("Wednesday", "wpia"); ?></option>
                        <option value="4"><?php _e("Thursday", "wpia"); ?></option>
                        <option value="5"><?php _e("Friday", "wpia"); ?></option>
                        <option value="6"><?php _e("Saturday", "wpia"); ?></option>
                        <option value="7"><?php _e("Sunday", "wpia"); ?></option>
                        
                    </select> <br/>                    
                </div>
                
                <div style="padding:15px 15px 0 15px; float:left; width:160px; ">
                    <strong><?php _e("Months to display?", "wpia"); ?></strong><br />
                    <select id="wpia_calendar_display" style="width: 160px;">
                        <?php for($i=1;$i<=12;$i++):?>
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php endfor;?>                        
                    </select> <br/>                    
                </div>
                
                <div style="padding:15px 15px 0 15px; float:left; width:160px;">
                    <strong><?php _e("Language", "wpia"); ?></strong><br />
                    <select id="wpia_calendar_language" style="width: 160px;">
                        <?php $activeLanguages = json_decode(get_site_option('wpia-languages'),true);?>
                            <option value="auto">Auto (let WP choose)</option>
                        <?php foreach($activeLanguages as $code => $language):?>
                            <option value="<?php echo $code;?>"><?php echo $language;?></option>
                        <?php endforeach;?>                   
                    </select> <br/>                    
                </div>
                
                <div style="padding:15px 15px 0 15px; float:left; width:160px; ">
                    <strong><?php _e("Start Month", "wpia"); ?></strong><br />
                    <select id="wpia_calendar_start_month" style="width: 160px;">
                        <option value="0">Current Month</option>
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5">May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>   
                    </select> <br/>                    
                </div>
                
                <div style="padding:15px 15px 0 15px; float:left; width:160px; ">
                    <strong><?php _e("Start Year", "wpia"); ?></strong><br />
                    <select id="wpia_calendar_start_year" style="width: 160px;">
                        <option value="0">Current Year</option>
                        <?php for($i = date("Y"); $i<= date("Y") + 10; $i++):?>
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php endfor;?>                 
                    </select> <br/>                    
                </div>
                
                <div style="padding:15px 15px 0 15px; float:left; width:160px; ">
                    <strong><?php echo __('Show history?','wpia');?></strong><br />
                    <select id="wpia_calendar_history" style="width: 160px;">
                        <option value="1"><?php echo __('Display booking history','wpia');?></option>
                        <option value="2"><?php echo __('Replace booking history with the default legend item','wpia');?></option>
                        <option value="3"><?php echo __('Grey out booking history','wpia');?></option>                        
                    </select> <br/>                    
                </div>
                
                <div style="padding:15px 15px 0 15px; float:left; width:160px; ">
                    <strong><?php echo __('Show week numbers?');?></strong><br />
                    <select id="wpia_calendar_week_numbers" style="width: 160px;">
                        <option value="yes"><?php _e("Yes", "wpia"); ?></option>
                        <option selected="selected" value="no"><?php _e("No", "wpia"); ?></option>                        
                    </select> <br/>                    
                </div>


                <div style="padding:15px 15px 0 15px; float:left; width:160px; ">
                    <strong><?php echo __('Jump Switch?');?></strong><br />
                    <select id="wpia_calendar_jump" style="width: 160px;">
                        <option value="yes"><?php _e("Yes", "wpia"); ?></option>
                        <option selected="selected" value="no"><?php _e("No", "wpia"); ?></option>                        
                    </select> <br/>                    
                </div>
                
               
                <div style="clear:left; padding:15px;">
                    <input type="button" class="button-primary" value="<?php _e("Insert Calendar", "wpia"); ?>" onclick="wpia_insert_shortcode();"/>&nbsp;&nbsp;&nbsp;
                <a class="button"  href="#" onclick="tb_remove(); return false;"><?php _e("Cancel", "wpia"); ?></a>
                </div>
            </div>
        </div>
    </div> -->

    <?php
}