<?php
class wpia_widget extends WP_Widget {
    public function __construct(){
        parent::__construct(false, $name = 'WP iCal Availability', array(
            'description' => 'WP iCal Availability'
        ));
    }
    function widget($args, $instance) {
        global $post;
        extract( $args );        
        
        echo $args['before_widget'];
        
        echo '<div class="wpia-widget">';       
        echo do_shortcode('[wpia id="'.$instance['wpia_select_calendar'].'" title="'.$instance['wpia_show_title'].'" legend="'.$instance['wpia_show_legend'].'" dropdown="'.$instance['wpia_show_dropdown'].'" start="'.$instance['wpia_calendar_start'].'" display="'.$instance['wpia_calendar_view'].'" language="'.$instance['wpia_calendar_language'].'" month="'.$instance['wpia_calendar_month'].'" year="'.$instance['wpia_calendar_year'].'" history="'.$instance['wpia_calendar_history'].'" weeknumbers="'.$instance['wpia_calendar_weeknumbers'].'"]'); 
        echo '</div>';
        
        
        
        echo $args['after_widget'];

    }
    function update($new_instance, $old_instance) {
        return $new_instance;
    }
    function form($instance) {
        global $wpdb;
        
        
        $calendarId = 0; if(!empty($instance['wpia_select_calendar'])) 
            $calendarId = $instance['wpia_select_calendar'];
        
        $showTitle = 'yes'; if(!empty($instance['wpia_show_title'])) 
            $showTitle = $instance['wpia_show_title'];
            
        $showLegend = 'yes'; if(!empty($instance['wpia_show_legend'])) 
            $showLegend = $instance['wpia_show_legend'];
            
        $showDropdown = 'yes'; if(!empty($instance['wpia_show_dropdown'])) 
            $showDropdown = $instance['wpia_show_dropdown'];

        $calendarView = '1'; if(!empty($instance['wpia_calendar_view'])) 
            $calendarView = $instance['wpia_calendar_view'];
            
        
        $calendarStart = '1'; if(!empty($instance['wpia_calendar_start'])) 
            $calendarStart = $instance['wpia_calendar_start'];
        
        $calendarLanguage = 'en'; if(!empty($instance['wpia_calendar_language'])) 
            $calendarLanguage = $instance['wpia_calendar_language'];
        
        $calendarMonth = '0'; if(!empty($instance['wpia_calendar_month'])) 
            $calendarMonth = $instance['wpia_calendar_month'];
        
        $calendarYear = '0'; if(!empty($instance['wpia_calendar_year'])) 
            $calendarYear = $instance['wpia_calendar_year'];
            
        $calendarHistory = '1'; if(!empty($instance['wpia_calendar_history'])) 
            $calendarHistory = $instance['wpia_calendar_history'];  
        
        $calendarTooltip = 'no'; if(!empty($instance['wpia_calendar_tooltip'])) 
            $calendarTooltip = $instance['wpia_calendar_tooltip'];
           
        $calendarWeeknumbers = 'no'; if(!empty($instance['wpia_calendar_weeknumbers'])) 
            $calendarWeeknumbers = $instance['wpia_calendar_weeknumbers'];    
            

            
        $sql = 'SELECT * FROM ' . $wpdb->base_prefix . 'wpia_calendars';
        $rows = $wpdb->get_results( $sql, ARRAY_A );
        
        
        ?>
        
        <p>
            <label for="<?php echo $this->get_field_id('wpia_select_calendar'); ?>"><?php _e('Calendar', "wpia");?></label>
            <select name="<?php echo $this->get_field_name('wpia_select_calendar'); ?>" id="<?php echo $this->get_field_id('wpia_select_calendar'); ?>" class="widefat">
            <?php foreach($rows as $calendar):?>
                <option<?php if($calendar['calendarID']==$calendarId) echo ' selected="selected"';?> value="<?php echo $calendar['calendarID'];?>"><?php echo $calendar['calendarTitle'];?></option>
            <?php endforeach;?>   
            </select>
         </p>   
         <p>
            <label for="<?php echo $this->get_field_id('wpia_show_title'); ?>"><?php _e('Display title?', "wpia");?></label>
            <select name="<?php echo $this->get_field_name('wpia_show_title'); ?>" id="<?php echo $this->get_field_id('wpia_show_title'); ?>" class="widefat">
                <option value="yes"><?php _e('Yes', "wpia");?></option>
                <option value="no"<?php if($showTitle=='no'):?> selected="selected"<?php endif;?>><?php _e('No', "wpia");?></option>
            </select>
         </p>   
         <p>   
            <label for="<?php echo $this->get_field_id('wpia_show_legend'); ?>"><?php _e('Display legend?', "wpia");?></label>
            <select name="<?php echo $this->get_field_name('wpia_show_legend'); ?>" id="<?php echo $this->get_field_id('wpia_show_legend'); ?>" class="widefat">
                <option value="yes"><?php _e('Yes', "wpia");?></option>
                <option value="no"<?php if($showLegend=='no'):?> selected="selected"<?php endif;?>><?php _e('No', "wpia");?></option>
            </select>
         </p>   
         <p>   
            <label for="<?php echo $this->get_field_id('wpia_show_dropdown'); ?>">D<?php _e('isplay dropdown?', "wpia");?></label>
            <select name="<?php echo $this->get_field_name('wpia_show_dropdown'); ?>" id="<?php echo $this->get_field_id('wpia_show_dropdown'); ?>" class="widefat">
                <option value="yes"><?php _e('Yes', "wpia");?></option>
                <option value="no"<?php if($showDropdown=='no'):?> selected="selected"<?php endif;?>><?php _e('No', "wpia");?></option>
            </select>
         </p>   
         <p>   
            <label for="<?php echo $this->get_field_id('wpia_calendar_start'); ?>"><?php _e('Week starts on', "wpia");?></label>
            <select name="<?php echo $this->get_field_name('wpia_calendar_start'); ?>" id="<?php echo $this->get_field_id('wpia_calendar_start'); ?>" class="widefat">
            
                <option value="1"<?php if($calendarStart==1):?> selected="selected"<?php endif;?>><?php _e("Monday", "wpia"); ?></option>
                <option value="2"<?php if($calendarStart==2):?> selected="selected"<?php endif;?>><?php _e("Tuesday", "wpia"); ?></option>
                <option value="3"<?php if($calendarStart==3):?> selected="selected"<?php endif;?>><?php _e("Wednesday", "wpia"); ?></option>
                <option value="4"<?php if($calendarStart==4):?> selected="selected"<?php endif;?>><?php _e("Thursday", "wpia"); ?></option>
                <option value="5"<?php if($calendarStart==5):?> selected="selected"<?php endif;?>><?php _e("Friday", "wpia"); ?></option>
                <option value="6"<?php if($calendarStart==6):?> selected="selected"<?php endif;?>><?php _e("Saturday", "wpia"); ?></option>
                <option value="7"<?php if($calendarStart==7):?> selected="selected"<?php endif;?>><?php _e("Sunday", "wpia"); ?></option>
            </select>
         </p>   
         <p>   
            <label for="<?php echo $this->get_field_id('wpia_calendar_view'); ?>"><?php _e('Months to display?', "wpia");?></label>
            <select name="<?php echo $this->get_field_name('wpia_calendar_view'); ?>" id="<?php echo $this->get_field_id('wpia_calendar_view'); ?>" class="widefat">
                <?php for($i=1;$i<=12; $i++):?>
                    <option value="<?php echo $i;?>"<?php if($calendarView==$i):?> selected="selected"<?php endif;?>><?php echo $i;?></option>
                <?php endfor;?>
            </select>
        </p>   
        <p>    
            <label for="<?php echo $this->get_field_id('wpia_calendar_language'); ?>"><?php _e('Language', "wpia");?></label>
            <select name="<?php echo $this->get_field_name('wpia_calendar_language'); ?>" id="<?php echo $this->get_field_id('wpia_calendar_language'); ?>" class="widefat">
                <?php $activeLanguages = json_decode(get_site_option('wpia-languages'),true);?>
                <option value="auto"><?php _e('Auto (let WP choose)', "wpia");?></option>
                <?php foreach($activeLanguages as $code => $language):?>
                    <option value="<?php echo $code;?>"<?php if($calendarLanguage == $code):?> selected="selected"<?php endif;?>><?php echo $language;?></option>
                <?php endforeach;?>   
            </select>
        </p>
        
        <p>    
            <label for="<?php echo $this->get_field_id('wpia_calendar_month'); ?>"><?php _e('Start Month', "wpia");?></label>
            <select name="<?php echo $this->get_field_name('wpia_calendar_month'); ?>" id="<?php echo $this->get_field_id('wpia_calendar_month'); ?>" class="widefat">
                <option<?php if($calendarMonth == 0):?> selected="selected"<?php endif;?> value="0"><?php _e('Current Month', "wpia");?></option>
                <option<?php if($calendarMonth == 1):?> selected="selected"<?php endif;?>  value="1"><?php _e('January', "wpia");?></option>
                <option<?php if($calendarMonth == 2):?> selected="selected"<?php endif;?>  value="2"><?php _e('February', "wpia");?></option>
                <option<?php if($calendarMonth == 3):?> selected="selected"<?php endif;?>  value="3"><?php _e('March', "wpia");?></option>
                <option<?php if($calendarMonth == 4):?> selected="selected"<?php endif;?>  value="4"><?php _e('April', "wpia");?></option>
                <option<?php if($calendarMonth == 5):?> selected="selected"<?php endif;?>  value="5"><?php _e('May', "wpia");?></option>
                <option<?php if($calendarMonth == 6):?> selected="selected"<?php endif;?>  value="6"><?php _e('June', "wpia");?></option>
                <option<?php if($calendarMonth == 7):?> selected="selected"<?php endif;?>  value="7"><?php _e('July', "wpia");?></option>
                <option<?php if($calendarMonth == 8):?> selected="selected"<?php endif;?>  value="8"><?php _e('August', "wpia");?></option>
                <option<?php if($calendarMonth == 9):?> selected="selected"<?php endif;?>  value="9"><?php _e('September', "wpia");?></option>
                <option<?php if($calendarMonth == 10):?> selected="selected"<?php endif;?>  value="10"><?php _e('October', "wpia");?></option>
                <option<?php if($calendarMonth == 11):?> selected="selected"<?php endif;?>  value="11"><?php _e('November', "wpia");?></option>
                <option<?php if($calendarMonth == 12):?> selected="selected"<?php endif;?>  value="12"><?php _e('December', "wpia");?></option>   
            </select>
        </p>
        
        <p>    
            <label for="<?php echo $this->get_field_id('wpia_calendar_year'); ?>"><?php _e('Start Year', "wpia");?></label>
            <select name="<?php echo $this->get_field_name('wpia_calendar_year'); ?>" id="<?php echo $this->get_field_id('wpia_calendar_year'); ?>" class="widefat">
                <option value="0">Current Year</option>
                <?php for($i = date("Y"); $i<= date("Y") + 10; $i++):?>
                    <option<?php if($calendarYear == $i):?> selected="selected"<?php endif;?> value="<?php echo $i;?>"><?php echo $i;?></option>
                <?php endfor;?>      
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('wpia_calendar_history'); ?>"><?php _e('Show history?', "wpia");?></label>
            <select name="<?php echo $this->get_field_name('wpia_calendar_history'); ?>" id="<?php echo $this->get_field_id('wpia_calendar_history'); ?>" class="widefat">
                <option <?php if($calendarHistory == 1):?> selected="selected"<?php endif;?> value="1"><?php _e('Display booking history', "wpia");?></option>
                <option <?php if($calendarHistory == 2):?> selected="selected"<?php endif;?> value="2"><?php _e('Replace booking history with the default legend item', "wpia");?></option>
                <option <?php if($calendarHistory == 3):?> selected="selected"<?php endif;?> value="3"><?php _e('Grey out booking history', "wpia");?></option>                        
            </select>              

        </p>
        
      
        
        <p>
            <label for="<?php echo $this->get_field_id('wpia_calendar_weeknumbers'); ?>"><?php _e('Show Week Numbers?', "wpia");?></label>
            <select name="<?php echo $this->get_field_name('wpia_calendar_weeknumbers'); ?>" id="<?php echo $this->get_field_id('wpia_calendar_weeknumbers'); ?>" class="widefat">
                <option <?php if($calendarWeeknumbers == 'no'):?> selected="selected"<?php endif;?> value="no"><?php _e('No', "wpia");?></option>
                <option <?php if($calendarWeeknumbers == 'yes'):?> selected="selected"<?php endif;?> value="yes"><?php _e('Yes', "wpia");?></option>
                            
            </select>              

        </p>

        <?php
    }
}
function wpia_register_widget() {
	register_widget( 'wpia_widget' );
}
add_action( 'widgets_init', 'wpia_register_widget' );