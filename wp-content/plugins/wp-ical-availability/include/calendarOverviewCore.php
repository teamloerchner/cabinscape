<?php
function wpia_display_month_select($options)
{
    extract($options);

    $op = null;

    if ( !isset( $class ) )
        $class = '';

    // start: Month Select
    $op .= '<div class="wpia-overview-header wpia-month-select '.$class.'">';
        $op .= '<select name="month">';
        for ( $d = ( $monthToShow - 3 ); $d <= ( $monthToShow + 12); $d++ )
        {
            $selected = ( mktime(0, 0, 0, $d, 15, $yearToShow) == mktime(0, 0, 0, $monthToShow, 15, $yearToShow) ) ? 'selected="selected"' : '';

            $op .= '<option value="' . mktime(0, 0, 0, $d, 15, $yearToShow) . '" ' . $selected . '>' . wpiaMonth(date('F',mktime(0, 0, 0, $d, 15, $yearToShow)), $calendarLanguage) . " " . date('Y',mktime(0, 0, 0, $d, 15, $yearToShow)) . '</option>';
        }
        $op .= '</select>';
    $op .= '</div>';
    // end: of Month Select
    return $op;
}

/**
 * Display Calendar Overview
 * @param type|array $options 
 * @return type
 */
function wpia_displayOverview( $options )
{
    $op = '';
    extract($options);

    // wpia_pr($options);

    if ( !$ajax )
        $op .= '<div class="wpia-overview-calendar-container">';

    /*=================================
    =            Variables            =
    =================================*/

    if ( !is_array( $calendars ) )
        $calendars      = explode(",", $calendars);
    
    $totalCalendars     = count( $calendars );

    $calendarTimestamp  = $currentTimestamp;
    $monthToShow        = date('n', $calendarTimestamp);
    $yearToShow         = date('Y', $calendarTimestamp);
    $calendarYear       = $yearToShow;

    $wpiaOptions        = json_decode(get_site_option('wpia-options'),true);



    if ( ($monthToShow === null) or ($yearToShow === null) ) 
    {
        $today          = getdate();
        $monthToShow    = $today['mon'];
        $yearToShow     = $today['year'];
    }
    else
    {
        $today = getdate( mktime( 0, 0, 0, $monthToShow, 1, $yearToShow ) );
    }

    $longTitle          = false;

    $monthsToShow       = 0;
    
    
    /*=====  End of Variables  ======*/
    
    foreach ( $calendars as $calendar )
    {
        if ( strlen( $calendar['calendarTitle'] ) > 17 )
            $longTitle = true;
    }

    // if NOT ajax call
    if ( !$ajax )
    {
        // $op .= '<h2>Calendar Overview</h2>';


        $fullWidth = ( $longTitle ) ? 'wpia-full-width': '';

        if ( $longTitle )
        {
            $op .= wpia_display_month_select(
                array(
                    'monthToShow'       => $monthToShow,
                    'monthsToShow'      => $monthsToShow,
                    'yearToShow'        => $yearToShow,
                    'calendarTimestamp' => $calendarTimestamp,
                    'calendarLanguage'  => $calendarLanguage,
                    'class'             => 'long-title'
                )
            );
        }

        $op .= '<div class="wpia-overview-calendar '. $fullWidth .'">';


            if ( !$longTitle )
            {
                $op .= '<div class="wpia-overview-headers">';

                    $op .= wpia_display_month_select(
                        array(
                            'monthToShow'       => $monthToShow,
                            'monthsToShow'      => $monthsToShow,
                            'yearToShow'        => $yearToShow,
                            'calendarTimestamp' => $calendarTimestamp,
                            'calendarLanguage'  => $calendarLanguage
                        )
                    );
                    

                    if ( $showWeekNumbers == "yes" )
                    {
                        $op .= '<div class="wpia-row-header"></div>';
                    }

                    foreach ( $calendars as $calendar )
                    {
                        $header_title = ( $longTitle ) ? '' : $calendar["calendarTitle"];

                        $op .= '<div class="wpia-row-header">' . $header_title . '</div>';

                        if ( $longTitle )
                            $op .= '<div class="wpia-row-header"></div>';
                    }

                    if ( count( $calendars ) >= 7 )
                    {
                        if ( $showWeekNumbers == "yes" )
                            $op .= '<div class="wpia-row-header"></div>';
                        $op .= '<div class="wpia-row-header"></div>';
                    }
                    


                $op .= '</div>';
            }



        
        $op .= '<div class="wpia-overview-content">';

    }

    // end: if NOT ajax call

        

    $op .= '<div class="wpia-month-display-container">';

    for ( $m = 0; $m <= $monthsToShow; $m++ )
    {
        $currentMonth   = $monthToShow + $m;
        
        $currentTimestamp = mktime(0, 0, 0, $currentMonth, 15, $yearToShow);

        $days = cal_days_in_month ( CAL_GREGORIAN , date( "n", $currentTimestamp ) , date( "Y", $currentTimestamp ) );

        $op .= '<div class="wpia-month-display">';
            $op .= '<a name="monthChange" data-value="' . mktime(0, 0, 0, $currentMonth, 15, $yearToShow) .'"></a>';

            $op .= '<ul data-month="' . mktime(0, 0, 0, $currentMonth, 15, $yearToShow) .'" data-month-name="'. date( "F", mktime(0, 0, 0, $currentMonth, 15, $yearToShow) ) .'">';

            $firstDayOfWeek     = 0;


            // First row
            $op .= '<li class="wpia-first-row">';
        
        
            for ( $i = 1; $i <= $days; $i++)
            {
                $op .= '<span class="wpia-row-day">' . $i . '</span>';
            }
            


            $op .= '</li>';

            // If Weekdays is set to Yes
            // Weekdays
            if ( $showWeekNumbers == "yes" )
            {
                
                $op .= '<li class="wpia-weekdays-row">';

                $dayText = wpiaDoW($calendarLanguage);         

                for ($i = 1; $i <= $days; $i++) 
                {
                    $xDay = date( "w", mktime( 0, 0, 0, $currentMonth, $i, $yearToShow ) );
                    $op .= '<span class="wpia-row-weekday">' . $dayText[$xDay] . '</span>';
                }
            
                $op .= '</li>';

            }



            // Calendars
            $activeDay          = 0;

            
            
            foreach( $calendars as $calendar )
            {
                
                $calendarOptions= json_decode( $calendar['calendarOptions'], true );

                $calendarData   = array();

                $calendarFeed   = $calendarOptions['icalendar_feed'];
                
                if ( $wpiaOptions['enableCaching'] == 'yes' )
                    $calendarData   = wpia_displayData( wpia_getCache($calendar['calendarID']), $wpiaOptions['displayDays'] );
                else
                {
                    /*data will need to come from feed*/
                    if( $calendarFeed !== null )
                    {
                        foreach( $calendarFeed as $feed )
                        {
                            global $ical;
                            $ical   = new wpia_ICal( $feed );
                            $events = array(); 

                            if( is_array( $ical->events() ) )
                                $events = array_merge( $events, $ical->events() );

                            if( is_array( $ical->freeBusyEvents() ) )
                                $events = array_merge( $events, $ical->freeBusyEvents() );

                            if( $events )
                                $calendarData[$feed]['events'] = wpia_getDataFromFeedData( $events );
                        }
                    }

                    $calendarData   = wpia_displayData( $calendarData, $wpiaOptions['displayDays'] );
                }

                // wpia_pr($calendarData);

                if ( $longTitle )
                {
                    // $link = wpia_get_calendar_link( $calendarOptions );
                    // $margin_class = ( $margin_class_cnt == 0 ) ? 'mt-2' : '';
                    // $margin_class_cnt++;

                    // if ( isset($link) && strlen( $link ) > 10 )
                    //     $op .= '<li class="wpia-long-title"><a href="' . $link . '">' . $calendar['calendarTitle'] . '</a></li>';
                    // else
                        $op .= '<li class="wpia-long-title">' . $calendar['calendarTitle'] . '</li>';
                }

                $op .= '<li class="wpia-calendar-row wpia-calendar-' . $calendar['calendarID'] . '">';


                // if( $wpiaOptions['enableCaching'] == 'yes' )
                //     $calendarData = wpia_multipleFeedData( $calendarData );
                
                for ( $i = 1; $i <= $days; $i++)
                {
                    $activeDay++; // Increment $activeDay
                    $dataTimestamp  = mktime(0,0,0,$currentMonth,$i,$yearToShow);

                    if(!empty($calendarData[$yearToShow][$currentMonth][$i]))
                        $status = $calendarData[$yearToShow][$currentMonth][$i];
                    else
                        $status = 'available';
                        
                    
                    if ( $dataTimestamp + (60*60*24) < time() && $showHistory > 1 )
                    {
                        if($showHistory == 3) 
                            $status = 'wpia-grey-out-history'; //grey-out
                    }

                    $op .= '<div class="wpia-row-day wpia-day-' . $i . ' status-' . $status . '">';

                        $op .= '<span class="wpia-day-split-top wpia-day-split-top-'.$status.'"></span>';
                        $op .= '<span class="wpia-day-split-bottom wpia-day-split-bottom-'.$status.'"></span>';


                    $op .= '</div>';
                }
                $op .= '</li>';

            }

            if ( $totalCalendars >= 7 )
            {
                // If Weekdays is set to Yes
                // Weekdays
                if ( $showWeekNumbers == "yes" )
                {
                    
                    $op .= '<li class="wpia-weekdays-row">';

                    $dayText = wpiaDoW($calendarLanguage);         

                    for ($i = 1; $i <= $days; $i++) 
                    {
                        $xDay = date( "w", mktime( 0, 0, 0, $currentMonth, $i, $yearToShow ) );
                        $op .= '<span class="wpia-row-weekday">' . $dayText[$xDay] . '</span>';
                    }
                
                    $op .= '</li>';

                }

                // Last row
                $op .= '<li class="wpia-last-row">';
                for ( $i = 1; $i <= $days; $i++)
                {
                    $op .= '<span class="wpia-row-day">' . $i . '</span>';
                }
                
                $op .= '</li>';

            }
            $op .= '</ul>';

        $op .= '</div>';
    }

    $op .= '</div>';



    // if NOT ajax call
    if ( !$ajax )
    {
        $op .= '</div>';


        // Append options
        $op .= wpia_displayOverviewOptions( $calendars, $options );

        $op .= '</div>';
        // if ( $showLegend == "yes" && $longTitle )
        // {
        //     $op .= '<ul class="wpia-overview-legend mt-2">';
            
        //     $op .= wpia_printLegend( $calendars[0]['calendarID'], json_decode($calendars[0]['calendarOptions'], true), $calendarLanguage );

        //     $op .= '</ul>';
        // }

        $op .= '</div>';
    }

    if ( $ajax )
    {
        $result = array();
        $months = '';
        $html   = $op;

        for ( $d = ( $monthToShow - 3 ); $d <= ( $monthToShow + 12); $d++ )
        {
            $selected = ( mktime(0, 0, 0, $d, 15, $calendarYear) === mktime(0, 0, 0, $monthToShow, 15, $calendarYear) ) ? 'selected="selected"' : '';
            $months .= '<option value="' . mktime(0, 0, 0, $d, 15, $yearToShow) . '" ' . $selected . '>' . wpiaMonth(date('F',mktime(0, 0, 0, $d, 15, $yearToShow)), $calendarLanguage) . " " . date('Y',mktime(0, 0, 0, $d, 15, $yearToShow)) . '</option>';
        }

        $result['months']   = $months;
        $result['html']     = $html;

        return json_encode( $result );
    }
    else
        return $op;


}

function wpia_displayOverviewOptions( $calendars, $options )
{
    extract($options);

    $calendarIds = array();
    foreach ( $calendars as $calendar )
    {
        $calendarIds[] = $calendar['calendarID'];
    }

    $op = '<div class="wpia-overview-calendar-options">';

        $op .= '<div class="wpia-show-overview-legend" data-show-legend="'.$showLegend.'"></div>';

        $op .= '<div class="wpia-calendar-overview-calendars" data-calendars="'.implode($calendarIds, ",").'"></div>';

        $op .= '<div class="wpia-calendar-overview-language" data-calendar-language="'.$calendarLanguage.'"></div>';

        $op .= '<div class="wpia-calendar-overview-tooltip" data-calendar-tooltip="'.$showTooltip.'"></div>'; 

        $op .= '<div class="wpia-calendar-overview-weeknumbers" data-calendar-weeknumbers="'.$showWeekNumbers.'"></div>';

    $op .= '</div>';

    return $op;
}