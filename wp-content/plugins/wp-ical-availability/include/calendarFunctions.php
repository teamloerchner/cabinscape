<?php
function wpia_print_legend_css($calendar = null){
    $output = "<style>";
        $calendarID = (!empty($calendar)) ? $calendar['calendarID'] : false;
        
        if($calendarID) $calendarOptions = json_decode($calendar['calendarOptions'],true);
        
        $availableColor = (!empty($calendarOptions['available_color'])) ? $calendarOptions['available_color'] : '#DDFFCC';
        $output .= ".wpia-calendar-".$calendarID." .status-available {background-color: ".$availableColor." ;}";
        $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-top-available {display:none ;} ";
        $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-bottom-available {display:none ;} ";
        
        $bookedColor = (!empty($calendarOptions['booked_color'])) ? $calendarOptions['booked_color'] : '#FFC0BD';
        $output .= ".wpia-calendar-".$calendarID." .status-booked {background-color: ".$bookedColor." ;}";
        $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-top-booked {display:none ;} ";
        $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-bottom-booked {display:none ;} ";
        
        $output .= ".wpia-calendar-".$calendarID." .status-grey {background-color: #ececec;}";
        $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-top-grey {display:none ;} ";
        $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-bottom-grey {display:none ;} ";
        
        
        $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-top-changeover-end {border-color: ".$bookedColor." transparent transparent transparent ; _border-color: ".$bookedColor." #000000 #000000 #000000 ;}";
        $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-bottom-changeover-end {border-color: transparent transparent " . $availableColor ." transparent ; _border-color:  #000000 #000000 " . $availableColor ." #000000 ;}";
        
        $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-top-changeover-start {border-color: ".$availableColor." transparent transparent transparent ; _border-color: ".$availableColor." #000000 #000000 #000000 ;}";
        $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-bottom-changeover-start {border-color: transparent transparent " . $bookedColor ." transparent ; _border-color:  #000000 #000000 " . $bookedColor ." #000000 ;}";
    
    
    $output .= "</style>";
    
    return $output;
}

function wpia_timeFormat($timestamp){
    $wpiaOptions = json_decode(get_site_option('wpia-options'),true);
    
    if(!isset($wpiaOptions['dateFormat'])) $wpiaOptions['dateFormat'] = 'd-m-Y';
    
    $date = date($wpiaOptions['dateFormat'], $timestamp);
    
    $month = date('F', $timestamp);
    $date = str_replace($month, __($month), $date);

    return $date;
    
}



function wpia_print_legend($calendarID, $calendarLanguage = 'en'){
    global $wpdb;
    $sql = $wpdb->prepare('SELECT calendarOptions FROM ' . $wpdb->base_prefix . 'wpia_calendars WHERE calendarID=%d',$calendarID);
    $calendar = $wpdb->get_row( $sql, ARRAY_A );
    $calendarOptions = json_decode($calendar['calendarOptions'],true);
    
    $language = ($calendarLanguage == 'auto') ? wpia_get_language() : $calendarLanguage;
    $available = (!empty($calendarOptions['available_translation_' . $language])) ? $calendarOptions['available_translation_' . $language] : __('Available','wpia');
    $booked = (!empty($calendarOptions['booked_translation_' . $language])) ? $calendarOptions['booked_translation_' . $language] : __('Booked','wpia');
    
    $output = '';
           
    $output .= '
        <div class="wpia-legend-item"><div class="wpia-legend-color status-available">
            <div class="wpia-day-split-top wpia-day-split-top-available"></div>
            <div class="wpia-day-split-bottom wpia-day-split-bottom-available"></div>    
        </div><p>'.$available.'</p></div>'; 
    $output .= '
        <div class="wpia-legend-item"><div class="wpia-legend-color status-booked">
            <div class="wpia-day-split-top wpia-day-split-top-booked"></div>
            <div class="wpia-day-split-bottom wpia-day-split-bottom-booked"></div>    
        </div><p>'.$booked.'</p></div>'; 
 
        

    return $output;
}

function wpia_get_language(){
    $activeLanguages = json_decode(get_site_option('wpia-languages'),true);
    if(array_key_exists(substr(get_bloginfo('language'),0,2),$activeLanguages)){
        return substr(get_bloginfo('language'),0,2);    
    }
    return 'en';
}

function wpia_get_locale(){
    return substr(get_locale(),0,2);
}
function wpia_tz_offset_to_name($offset){
    $offset *= 3600;
    $abbrarray = timezone_abbreviations_list();
    foreach ($abbrarray as $abbr){
        foreach ($abbr as $city){
            if ($city['offset'] == $offset){
                    return $city['timezone_id'];
            }
        }
    }
    
    return false;
}

function wpia_fixDate($date){
    $date = str_replace('TZ','T000000Z',$date);
    return $date;
}


function wpia_getCache($id){
    global $wpdb, $wpiaOptions;
    $sql = $wpdb->prepare('SELECT calendarCache FROM ' . $wpdb->base_prefix . 'wpia_calendars WHERE calendarID=%d',$id);
    $calendar = $wpdb->get_row( $sql, ARRAY_A );
    return json_decode($calendar['calendarCache'],true);
}

function wpia_checkCacheData( $calendarId, $cacheInterval )
{
    $cache = wpia_getCache($calendarId);

    if ( !is_array( $cache ) )
        return false;

    if( (int)$cache['last_update'] <= (time() - $cacheInterval ) )
        return false;

    return true;
}
function wpia_updateFeedCount( $calendarId, $count )
{
    if ( !$calendarId )
        return false;
    if ( !$count )
        return false;

    global $wpdb;

    $checkOptions   = $wpdb->get_row(
        'SELECT calendarOptions FROM ' . $wpdb->prefix  . 'wpia_calendars WHERE calendarID = ' . $calendarId . ';', 
        ARRAY_A
    );

    $checkOptions   = json_decode( $checkOptions['calendarOptions'], true);
    $checkOptions['last_feed_count'] = $count;

    $update     = $wpdb->update(
        $wpdb->prefix.'wpia_calendars', 
        array(
            'calendarOption'    => json_encode( $checkOptions ),
            'modifiedDate'      => time()
        ), 
        array(
            'calendarID'        => $calendarId
        )
    );

    return $update;

}
function wpia_checkCache( $calendarId, $feedUrl, $cacheInterval )
{
    $cache = wpia_getCache($calendarId);

    if( !isset( $cache[$feedUrl] ) )
        return false;
    
    if( (int)$cache['last_update'] <= (time() - $cacheInterval ) )
        return false;

    return true;
}

function wpia_getFromCache($id, $feed){
    $cache = wpia_getCache($id);
    // return $cache[$feed]['events'];
    return $cache;
}

/**
 * Generate calendarData to be saved in cache NOT the events from the feed
 * @return array $calendarData
 * 
 * @since   2.4.1
 */
function wpia_getDataFromFeedData( $feedData )
{
    if ( !$feedData )
        return false;

    $calendarData = array();

    foreach( $feedData as $event )
    {
        $start  = strtotime( wpia_fixDate( $event['DTSTART'] ) );
        $end    = strtotime( wpia_fixDate( $event['DTEND'] ) ) - 60;
        
        if ( $start > $end )
        {
            $calendarData[date('Y',$start)][date('n',$start)][date('j',$start)] = 'booked';
        }
        else
        {
            for($i = $start; $i<= $end; $i = $i + 60*60*24)
            {
                $calendarData[date('Y',$i)][date('n',$i)][date('j',$i)] = 'booked';
                if( isset( $event['SUMMARY'] ) )
                    $calendarData[date('Y',$i)][date('n',$i)]['description-' . date('j',$i)] = $event['SUMMARY'];
            }
            
        }
    }

    return $calendarData;
}

/**
 * Generates the JSON string/object to be saved/updated into the database
 * @return array $cacheData
 * 
 * @since   2.4.1
 */
function wpia_dataForCache( $events )
{
    if ( !is_array( $events ) )
        return false;

    $cacheData = array();
    
    if ( !$events )
        return new Exception('WP iCal Availability: No "events" found!');
    else
        $calendarData   = wpia_getDataFromFeedData( $events );
    
    $cacheData['events']      = $calendarData;
    $cacheData['last_update'] = time();

    return $cacheData;
}

/**
 * Saves/updates cacheData in the database for the given $calendarId
 * @return (int|false) $wpdb->update result
 * 
 * @since   2.4.1
 */
function wpia_saveCache( $calendarId, $cacheData )
{
    global $wpdb, $wpiaOptions;

    if ( !$cacheData )
        return false;

    $cacheData['last_update'] = time();

    $cacheData      = json_encode( $cacheData );

    return $wpdb->update(
        $wpdb->base_prefix.'wpia_calendars',
        array(
            'calendarCache' => $cacheData
        ),
        array(
            'calendarID'    => $calendarId
        )
    );
}

function wpia_multipleFeedData( $data )
{
    $calendarData = array();

    if ( !is_array( $data ) )
        $data = json_decode( $data, true );

    if ( $data )
    {
        foreach ( $data as $feed => $events )
        {
            if ( isset( $events['events'] ) )
            {
                foreach ( $events['events'] as $year => $months )
                {
                    foreach ( $months as $month => $days )
                    {
                        foreach ( $days as $day => $value)
                        {
                            $calendarData[$year][$month][$day] = $value;
                        }
                    }
                }
            }
        }
    }

    return $calendarData;
}

function wpia_displayData( $calendarData, $splitDays )
{
    // if ( !$ajaxCall)
    $calendarData = wpia_multipleFeedData( $calendarData );

    if ( !empty( $splitDays ) && $splitDays == 2 )
    {

        foreach($calendarData as $y => $months)
        {
            foreach($months as $m => $days)
            {
                foreach($days as $d => $status)
                {
                    if (strpos($d,'description') === false) 
                    {
                        
                        //previous day if first day of the month
                        if($d == 1){
                            $pY = date('Y',mktime(0,0,0,$m-1,15,$y));
                            $pM = date('n',mktime(0,0,0,$m-1,15,$y));
                            $pD = date('t',mktime(0,0,0,$m-1,15,$y));
                        } else {
                            $pY = $y;
                            $pM = $m;
                            $pD = $d-1;
                        }
                        
                        //next day if last day of the month
                        if($d == date('t',mktime(0,0,0,$m,15,$y))){
                            $nY = date('Y',mktime(0,0,0,$m+1,15,$y));
                            $nM = date('n',mktime(0,0,0,$m+1,15,$y));
                            $nD = 1;
                        } else {
                            $nY = $y;
                            $nM = $m;
                            $nD = $d+1;
                        }
                            
        
                        
                        if(
                            (
                                $calendarData[$y][$m][$d] == 'booked' 
                                && (!isset($calendarData[$pY][$pM][$pD]) || $calendarData[$pY][$pM][$pD] == "changeover-end")
                            )
                        )
                        {
                            $calendarData[$y][$m][$d] = 'changeover-start';    
                            if((!isset($calendarData[$nY][$nM][$nD])))
                            {
                                $calendarData[$nY][$nM][$nD] = 'changeover-end';
                            }
                        }
                        
                        if(
                            $calendarData[$y][$m][$d] == 'booked' 
                            && (!isset($calendarData[$nY][$nM][$nD]))
                        )
                        {
                            $calendarData[$nY][$nM][$nD] = 'changeover-end';
                        }
                        
                    }
                }
            }
        }
    }
    return $calendarData;
}

function wpia_addToCache($id, $feed,$events){
    global $wpdb, $wpiaOptions;
    if($wpiaOptions['enableCaching'] == 'no') return false;
    $cache = wpia_getCache($id);
    $cache[$feed]['events'] = $events;
    $cache[$feed]['last_updated'] = time();
    $wpdb->update( $wpdb->base_prefix.'wpia_calendars', array('calendarCache' => json_encode($cache)), array('calendarID' => $id) );
}

function wpia_print_overview_legend_css($calendarOptions, $calendarID)
{
    $output = "<style>";
    
    $availableColor = (!empty($calendarOptions['available_color'])) ? $calendarOptions['available_color'] : '#DDFFCC';
    $output .= ".wpia-calendar-".$calendarID." .status-available {background-color: ".$availableColor." !important ;}";
    $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-top-available {display:none ;} ";
    $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-bottom-available {display:none ;} ";
    
    $bookedColor = (!empty($calendarOptions['booked_color'])) ? $calendarOptions['booked_color'] : '#FFC0BD';
    $output .= ".wpia-calendar-".$calendarID." .status-booked {background-color: ".$bookedColor." !important;}";
    $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-top-booked {display:none ;} ";
    $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-bottom-booked {display:none ;} ";
    
    $output .= ".wpia-calendar-".$calendarID." .status-grey {background-color: #ececec;}";
    $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-top-grey {display:none ;} ";
    $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-bottom-grey {display:none ;} ";
    
    
    $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-top-changeover-end {border-color: ".$bookedColor." transparent transparent transparent ; _border-color: ".$bookedColor." #000000 #000000 #000000 ;}";
    $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-bottom-changeover-end {border-color: transparent transparent " . $availableColor ." transparent ; _border-color:  #000000 #000000 " . $availableColor ." #000000 ;}";
    
    $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-top-changeover-start {border-color: ".$availableColor." transparent transparent transparent ; _border-color: ".$availableColor." #000000 #000000 #000000 ;}";
    $output .= ".wpia-calendar-".$calendarID." .wpia-day-split-bottom-changeover-start {border-color: transparent transparent " . $bookedColor ." transparent ; _border-color:  #000000 #000000 " . $bookedColor ." #000000 ;}";
        
    $output .= ".wpia-overview-calendar-container .status-wpia-grey-out-history {background-color: #f3f3f3 !important;}";
    $output .= ".wpia-overview-calendar-container .status-wpia-grey-out-history .wpia-day-split-top, .status-wpia-grey-out-history .wpia-day-split-bottom {display:none;}";
    $output .= "</style>";
    
    return $output;
}

// Get link of calendar
function wpia_get_calendar_link( $options )
{
    if ( is_array( $options ) )
        $calendarLink       = $options['calendarHyperlinks'];
    else
    {
        $options            = json_decode( $options, true );
        $calendarLink       = $options['calendarHyperlinks'];
    }
    $link                   = $post = null;

    if ( isset($calendarLink['type']) )
    {
        if ( $calendarLink['type'] == 'internal' )
        {
            $post   = get_post( $calendarLink['link'] );
            $link   = get_permalink( $post->ID );
        }
        elseif ( $calendarLink['type'] == 'external' )
        {
            $link   = ( !empty( $calendarLink['link'] ) ) ? $calendarLink['link'] : '';
        }
        
    }

    return $link;
}


// Debug function
function wpia_pr( $what, $pre = true )
{
    echo ( $pre ) ? '<pre>' : PHP_EOL;
    var_dump( $what );
    echo ( $pre ) ? '</pre>' : PHP_EOL;
}