<?php
function wpia_changeDayAdmin_callback($data ) {
    global $showDateEditor;
    $showDateEditor = true;
    wpia_changeDay_callback($data );
    
    die();
}
function wpia_changeDay_callback($data ) {
    global $showDateEditor, $wpdb;
    
    $params = null;
    if ( !empty($data) )
        $params = $data;
    elseif ( !empty($_POST) )
        $params = $_POST;

    $calendarFeed = null;
    $calendarLegend = null;
    if(in_array($params['showLegend'],array(0,1))) $showLegend = $params['showLegend']; else $showLegend = 1;
    if(!empty($params['showWeekNumbers']) && in_array($params['showWeekNumbers'],array(0,1))) $showWeekNumbers = $params['showWeekNumbers']; else $showWeekNumbers = 1;
    if(in_array($params['showDropdown'],array(0,1))) $showDropdown = $params['showDropdown']; else $showDropdown = 1;
    if($params['totalCalendars'] > 1) $totalCalendars = $params['totalCalendars']; else $totalCalendars = 1;
    if(in_array($params['weekStart'],array(1,2,3,4,5,6,7))) $firstDayOfWeek = $params['weekStart']; else $firstDayOfWeek = 1;
    
    if(!empty($params['currentTimestamp'])) $currentTimestamp = $params['currentTimestamp'];
    $calendarData = (!empty($params['calendarData'])) ? $calendarData = $params['calendarData'] : false;
    if(!empty($params['calendarLegend'])) $calendarLegend = $params['calendarLegend'];    
    if(!empty($params['calendarID'])) $calendarID = $params['calendarID']; else $calendarID = 0;    
    if(!empty($params['calendarLanguage'])) $calendarLanguage = $params['calendarLanguage'];
    if(!empty($params['calendarHistory'])) $calendarHistory = $params['calendarHistory'];
    if(!empty($params['weekStart'])) $firstDayOfWeek = $params['weekStart'];

    if ( !is_array( $calendarData ) )
        $calendarData = json_decode( $calendarData, true );



    if(!empty($params['jump'])) $jump = $params['jump']; else $jump = 0;
    
    
    $currentTimestamp = intval($currentTimestamp);    
    //hack $currentTimestamp to be the middle of the month.
    $currentTimestamp = strtotime("15 " . date(' F Y',$currentTimestamp));
    
    if(!empty($params['calendarDirection']) && $params['calendarDirection'] == 'next')
    {
        /**
         * @since  2.1.9
         */
        if ( $jump == 'yes' )
            $currentTimestamp = strtotime(date('j F Y',$currentTimestamp) . " + " . $totalCalendars . " month");
        else
            $currentTimestamp = strtotime(date('j F Y',$currentTimestamp) . " + 1 month");
    } 
    elseif(!empty($params['calendarDirection']) && $params['calendarDirection'] == 'prev')
    {
        /**
         * @since  2.1.9
         */
        if ( $jump == 'yes' )
            $currentTimestamp = strtotime(date('j F Y',$currentTimestamp) . " - " . $totalCalendars . " month");
        else
            $currentTimestamp = strtotime(date('j F Y',$currentTimestamp) . " - 1 month");
    }

    if ( !isset($calendarData) && !isset($calendarLegend) )
    {
        $wpiaOptions = json_decode(get_site_option('wpia-options'),true);

        $sql = $wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'wpia_calendars WHERE calendarID = %s', $calendarID );
        $calendarResults = $wpdb->get_results( $sql, ARRAY_A );

        $calendarLegend = new stdClass;
        $calendarData   = new stdClass;

        if ( $wpdb->num_rows > 0 )
        {

            $calendarLegend     = stripslashes( $calendarResults[0]['calendarOptions'] );
            $calendarOptions    = json_decode( $calendarResults[0]['calendarOptions'], true );
            $calendarFeed       = $calendarOptions['icalendar_feed'];

            if( $wpiaOptions['enableCaching'] == 'yes' ) {
                $calendarData = wpia_multipleFeedData( stripslashes( $calendarResults[0]['calendarCache'] ) );
            }
                

            else 
            {
                $calendarOptions= json_decode( $calendarResults[0]['calendarOptions'], true );
                $calendarFeed   = $calendarOptions['icalendar_feed'];

                if($calendarFeed !== null)
                {
                    foreach($calendarFeed as $feed)
                    {
                        $ical   = new wpia_ICal( $feed );
                        $events = array(); 
                        
                        if( is_array( $ical->events() ) )
                            $events = array_merge( $events, $ical->events() );

                        if( is_array( $ical->freeBusyEvents() ) )
                            $events = array_merge( $events, $ical->freeBusyEvents() );

                        if( $events )
                            $cacheData[ esc_url( $feed ) ]  = wpia_dataForCache( $events );
                    }

                    $calendarData = wpia_multipleFeedData( $cacheData );
                }
            }
        }
    }

        // echo'<pre>';var_dump($calendarData);echo'</pre>';
    echo wpia_calendar(array('ajaxCall' => true, 'calendarLanguage' => $calendarLanguage, 'calendarHistory' => $calendarHistory, 'showDateEditor' => $showDateEditor, 'calendarID' => $calendarID, 'calendarData' => $calendarData, 'currentTimestamp' => $currentTimestamp, 'calendarFeed' => $calendarFeed, 'showWeekNumbers' => $showWeekNumbers, 'showLegend' => $showLegend, 'showDropdown' => $showDropdown, 'totalCalendars' => $totalCalendars, 'jump' => $jump, 'firstDayOfWeek' => $firstDayOfWeek));
    
    die();
}

function wpia_changeOverview_callback() 
{
    global $showDateEditor, $wpdb;
    
    if(in_array($_POST['showLegend'],array(0,1))) $showLegend = $_POST['showLegend']; else $showLegend = 1;
    
    if(!empty($_POST['currentTimestamp'])) $currentTimestamp = $_POST['currentTimestamp'];
    // if(!empty($_POST['calendarData'])) $calendarData = $_POST['calendarData'];
    // if(!empty($_POST['calendarLegend'])) $calendarLegend = $_POST['calendarLegend'];    
    if(!empty($_POST['calendarLanguage'])) $calendarLanguage = $_POST['calendarLanguage'];
    // if(!empty($_POST['calendarHistory'])) $calendarHistory = $_POST['calendarHistory'];
    if(!empty($_POST['showLegend'])) $showLegend = $_POST['showLegend'];
    if(!empty($_POST['showWeekNumbers'])) $showWeekNumbers = $_POST['showWeekNumbers'];

    if(!empty($_POST['calendars'])) $calendars = $_POST['calendars'];
        
    $currentTimestamp = intval($currentTimestamp);    
    // //hack $currentTimestamp to be the middle of the month.
    $currentTimestamp = strtotime("15 " . date(' F Y',$currentTimestamp));

    if ( $calendars != "all")
    {
        $calendars          = explode(",", $calendars);

        $calendarResults    = array();

        foreach ( $calendars as $calendarId )
        {
            $sql = $wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'wpia_calendars WHERE calendarID = %d', $calendarId );
            $sql = str_replace("\\","", $sql) ;
            $calendarResults[] = $wpdb->get_row( $sql, ARRAY_A );
        }
    }
    else
    {
        $sql = 'SELECT * FROM ' . $wpdb->prefix . 'wpia_calendars ORDER by calendarID ASC';
        $sql = str_replace("\\","", $sql) ;
        $calendarResults = $wpdb->get_results( $sql, ARRAY_A );
    }

    $options =
        array(
            'ajax'              => true,
            'calendars'         => $calendarResults,
            'currentTimestamp'  => $currentTimestamp,
            'calendarLanguage'  => $calendarLanguage,
            'showWeekNumbers'   => $showWeekNumbers, 
            'showLegend'        => $showLegend
        )
    ;

    echo wpia_displayOverview( $options );
    
    die(); 
}