#!/usr/bin/python
import mysql.connector
import mandrill
import pandas as pd
import os
import logging
import configparser

logging.basicConfig(filename='/var/log/rescheduler.log', format='%(asctime)s %(message)s', level=logging.INFO)
config = configparser.ConfigParser()
config.read('/cabinscape/rescheduler/config.ini')

def checkFailed():
    mydb = mysql.connector.connect(
      host=config.get('DATABASE', 'host'),
      user=config.get('DATABASE', 'user'),
      passwd=config.get('DATABASE', 'passwd'),
      database=config.get('DATABASE', 'database')
    )

    mycursor = mydb.cursor()

    mycursor.execute("""
    select t1.*, t2.post_title as cabin from
    (
	select STR_TO_DATE(date, '%H:%i:%s %M %e %Y') as booking_date,
	    id as booking_id, 
	    id_post,
	    paypal_email,
	    date_from,
	    date_to,
	    paypal_payment_status
	from wp_nd_booking_booking
	where id NOT IN (
	    select booking_id from wp_cs_scheduled_emails
	)
	and paypal_payment_status NOT IN ('failed')
    ) as t1
    left join wp_posts as t2 on t1.id_post = t2.ID
    where booking_date between CAST('2019-02-01' as DATE) and now()
    and date_from >= '2020/02/01'
    ORDER BY booking_date DESC;
    """)

    myresult = mycursor.fetchall()
    return myresult
    

def reschedule(failed):
    apikey = config.get('MAIL', 'mandrill_api_key')
    for guest in failed:
	booking_id = guest[1]
	cabin_id = guest[2]
	cmd = "cd /var/www/html/wp-content/plugins/customer-mails; php bin/console.php emails:reschedule {} {} {}".format(apikey, cabin_id, booking_id)
	os.system(cmd)   

def sendEmail(myresult):
    myresult = pd.DataFrame(myresult, columns=['index', 'booking_date', 'booking_id', 'cabin_id','email','check in', 'checkout'])
    apikey = config.get('MAIL', 'mandrill_api_key')
    mandrill_client = mandrill.Mandrill(apikey)
    mandrill_email_from = config.get('MAIL', 'mandrill_email_from')
    mandrill_email_to = config.get('MAIL', 'mandrill_email_to')

    message = { 'from_email': mandrill_email_from,
      'from_name': 'Cabinscape',
      'to': [{
	'email': mandrill_email_to,
	'name': 'Cabinscape Admin',
	'type': 'to'
      }],
     'subject': "Failed Scheduled Email Report",
     'text': myresult.to_string()
    }
    result = mandrill_client.messages.send(message = message)
    logging.info("Email status reported from mandrill: {}".format(result[0]['status']))

logging.info('Checking for emails that weren\'t scheduled')
failed = checkFailed()
if len(failed) > 0:
    logging.info("found \n {}".format(failed))
    reschedule(failed)
    logging.info('done rescheduling attempt')
    rescheduleFailed = checkFailed()
    if len(rescheduleFailed) > 0:
	logging.error("after rescheduling attempt found \n {}".format(rescheduleFailed))
	sendEmail(rescheduleFailed)
	logging.info("sent email after failing to reschedule")
    else:
	logging.info("emails were rescheduled successfully")
else:
    logging.info("no emails needed to be rescheduled")
logging.info("script completed")
